<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuctionRowModel extends Model
{
	public $primaryKey = 'auctionrow_id';
	const CREATED_AT = 'auctionrow_created';
	const UPDATED_AT = 'auctionrow_updated';
	public $table = 'turi_auctionrows';
	protected $fillable = array(
		'auctionrow_auction',
		'auctionrow_number',
		'auctionrow_price',
		'auctionrow_hotel',
		'auctionrow_user',
		"auctionrow_room",
		'auctionrow_time',
		'auctionrow_active'
	);
}
