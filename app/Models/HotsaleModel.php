<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotsaleModel extends Model
{


	public $primaryKey = 'hotsale_id';
	public $table = 'turi_hotsales';
	const CREATED_AT = 'hotsale_created';
	const UPDATED_AT = 'hotsale_updated';
	protected $casts = [
		'hotsale_services' => 'array',
		'hotsale_images' => 'array',
	];

}
