<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelModel extends Model
{
    public $primaryKey = 'hotel_id';
    public $table = 'turi_hotels';
    const CREATED_AT = 'hotel_created';
    const UPDATED_AT = 'hotel_updated';

    protected $fillable = array(
        'hotel_user',
        'hotel_name',
        'hotel_city',
        'hotel_address',
        'hotel_phone',
        'hotel_active',
        'hotel_rooms',
        'hotel_images',
        'hotel_description',
    );

    protected $casts = array(
        'hotel_images' => 'array'
    );
}
