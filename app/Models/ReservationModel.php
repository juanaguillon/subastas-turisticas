<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationModel extends Model
{
    public $table = 'turi_reservations';
    protected $primaryKey = "reservation_id";
    const CREATED_AT = 'reservation_created';
    const UPDATED_AT = 'reservation_updated';
}
