<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;

class UserModel extends Model implements JWTSubject
{
    public $primaryKey = 'user_id';
    public $table = 'turi_users';
    const CREATED_AT = 'user_created';
    const UPDATED_AT = 'user_updated';
    protected $fillable = ['user_name', 'user_password', 'user_rol', 'user_email', 'user_valid'];
    protected $hidden = ['user_password'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
