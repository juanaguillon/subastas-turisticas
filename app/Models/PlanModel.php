<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanModel extends Model
{
	public $primaryKey = 'plan_id';
	public $table = 'turi_planes';
	const CREATED_AT = 'plan_created';
	const UPDATED_AT = 'plan_updated';
	protected $casts = [
		'plan_special_service' => 'array',
		'plan_services' => 'array',
		'plan_images' => 'array',
	];
}
