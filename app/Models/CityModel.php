<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    public $incrementing = false;
    public $table = 'turi_cities';
    public $primaryKey = 'city_id';
}
