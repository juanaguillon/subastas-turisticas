<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomModel extends Model
{
	public $primaryKey = 'room_id';
	public $table = 'turi_rooms';
	const CREATED_AT = 'room_created';
	const UPDATED_AT = 'room_updated';
	protected $casts = [
		'room_services' 	=> 'array',
		'room_equipament' => 'array',
		'room_gifts' 			=> 'array',
		'room_images' 		=> 'array',
	];
}
