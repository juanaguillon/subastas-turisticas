<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuctionModel extends Model
{
	public $primaryKey = 'auction_id';
	public $table = 'turi_auctions';
	const CREATED_AT = 'auction_created';
	const UPDATED_AT = 'auction_updated';
	protected $fillable = array(
		"auction_hotels",
		"auction_city",
		"auction_days",
		"auction_minval",
		"auction_maxval",
		"auction_adults",
		"auction_useremail",
		"auction_username",
		"auction_childrens",
		"auction_infants",
		"auction_status",
		"auction_userid",
		"auction_init",
		"auction_end",
	);

	protected $casts = array(
		'auction_hotels' => 'array'
	);
}
