<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class Authenticate extends Middleware
{
	/**
	 * Get the path the user should be redirected to when they are not authenticated.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return string
	 */
	protected function redirectTo($request)
	{
		if (!$request->expectsJson()) {
			return route('login');
		}
	}

	public function handle($request, Closure $next, ...$guards)
	{
		$token = JWTAuth::getToken();
		$apy = JWTAuth::getPayload($token)->toArray();
		return response()->json(array(
			'token' => $apy
		));
		// return $next($request);
	}
}
