<?php

namespace App\Http\Middleware;

use Closure;

class CheckRol
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $rol)
	{

		if ($rol == 'any') {
			if (isAuth()) {
				return $next($request);
			} else {
				return redirect('/');
			}
		}

		if (isAuth()) {
			$currentSession = $request->session()->get('user_rol');
		} else {
			$currentSession = "noauth";
		}



		if ($currentSession !== $rol && $currentSession !== "admin") {
			switch ($currentSession) {
				case 'client':
				default:
					return redirect('/');
					break;
				case 'hotel':
					return redirect('/room/create');
					break;
			}
		}
		return $next($request);
	}
}
