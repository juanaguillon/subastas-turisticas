<?php

namespace App\Http\Controllers;

use App\Models\RoomModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use stdClass;

class RoomController extends Controller
{
	/**
	 * POST /room/create
	 * Crear una nueva habitacion
	 *
	 * @param Request $request
	 * @return void
	 */
	public function createRoom(Request $request)
	{
		$validator = Validator::make($request->all(), array(
			'room_active' => 'required|boolean',
			'room_name' => 'required',
			'room_adults' => 'required|numeric|min:1',
			'room_minprice' => 'required|numeric|min:1000',
			'room_maxprice' => 'required|numeric|min:1000',
			'room_rooms' => 'numeric|min:1',
			'room_meters' => 'required|numeric|min:1',
		));

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}


		// CODIGO GUARDAR IMAGENES
		// Este trozo de codigo ayudara a guardar las imagenes segun el orden deseado por el cliente.
		$imagesToSave = array();
		$imagesOrder = $request->input('room_indexes_imgs');
		if ($imagesOrder !== "" && $imagesOrder !== null) {

			$imagesOrder = explode(',', trim($imagesOrder));

			$filesRequest = $request->file('room_images');

			foreach ($imagesOrder as $value) {
				$imagesToSave[] = saveImage($filesRequest[$value], 'habitacion/');
			}
		}
		// FINAL GUARDAR IMAGENES

		$keysToSave = array(
			'room_name',
			'room_description',
			'room_terms',
			'room_services',
			'room_equipament',
			'room_gifts',
			'room_meters',
			'room_rooms',
			'room_adults',
			'room_minprice',
			'room_maxprice',
			'room_childs',
			'room_babys',
			'room_active',
		);

		$dataTosave = $request->all($keysToSave);

		$dataTosave['room_services'] = json_encode($dataTosave['room_services']);
		$dataTosave['room_equipament'] = json_encode($dataTosave['room_equipament']);
		$dataTosave['room_gifts'] = json_encode($dataTosave['room_gifts']);
		$dataTosave['room_images'] = json_encode($imagesToSave);
		$dataTosave['room_hotel'] = $request->session()->get("user_hotel");
		$dataTosave['room_description'] = sanitizeHtml($dataTosave['room_description']);
		$dataTosave['room_terms'] = sanitizeHtml($dataTosave['room_terms']);
		$dataTosave['room_created'] = Carbon::now()->toDateTimeLocalString();
		$dataTosave['room_updated'] = Carbon::now()->toDateTimeLocalString();

		try {
			$roomSaved = RoomModel::insert($dataTosave);
			if ($roomSaved) {
				return redirect('/room/create?new_reg=true');
			} else {
				return redirect()->back()->withErrors('Error al crear la habitación');
			}
		} catch (\Throwable $th) {
			echo $th->getMessage();
			echo $th->getFile();
			echo $th->getLine();
		}
	}


	/**
	 * (Hotel) POST /room/editar-habitacion/{roomid}
	 *
	 * @return void
	 */
	function updateRoom(Request $request, $roomid)
	{

		$roomModel = RoomModel::find($roomid);
		if (!$roomModel) {
			return response()->json(array(
				"error" => true,
				"message" => "ID not found"
			));
		}

		$validator = Validator::make($request->all(), array(
			'room_active' => 'required|boolean',
			'room_name' => 'required',
			'room_adults' => 'required|numeric|min:1',
			'room_minprice' => 'required|numeric|min:1000',
			'room_maxprice' => 'required|numeric|min:1000',
			'room_rooms' => 'numeric|min:1',
			'room_meters' => 'required|numeric|min:1',
		));

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}

		$roomModel->room_active 			= $request->input("room_active");
		$roomModel->room_name					= $request->input("room_name");
		$roomModel->room_adults 			= $request->input("room_adults");
		$roomModel->room_childs 			= $request->input("room_childs");
		$roomModel->room_babys 				= $request->input("room_babys");
		$roomModel->room_minprice 		= $request->input("room_minprice");
		$roomModel->room_maxprice 		= $request->input("room_maxprice");
		$roomModel->room_rooms 				= $request->input("room_rooms");
		$roomModel->room_meters 			= $request->input("room_meters");
		$roomModel->room_description 	= sanitizeHtml($request->input("room_description"));
		$roomModel->room_terms 				= sanitizeHtml($request->input("room_terms"));
		$roomModel->room_equipament 	= $request->input("room_equipament");
		$roomModel->room_services 		= $request->input("room_services");
		$roomModel->room_gifts 				= $request->input("room_gifts");

		try {
			$roomSaved = $roomModel->save();
			if ($roomSaved) {
				return redirect('/room/create?new_reg=true');
			} else {
				return redirect()->back()->withErrors('Error al crear la habitación');
			}
		} catch (\Throwable $th) {
			echo $th->getMessage();
			echo $th->getFile();
			echo $th->getLine();
		}
	}

	/**
	 * Actualizar una habitacion por ID
	 * @param int $roomid
	 */
	public function view__updateRoom($roomid)
	{
		$preRoom = RoomModel::find($roomid);
		return view('room.create-room', array(
			'room' => $preRoom,
			'updating' => true
		));
	}

	/**
	 * Obtener todas las habitaciones segun Hotel ID
	 */
	public function getAllRooms($hotelID)
	{
		return RoomModel::where('room_hotel',  $hotelID)->get();
	}

	/**
	 * Obtener una habitacion segun IDs de multiples hoteles
	 * Esta funcion es util al momento de crear un nuevo AuctionRow ( Fila de Subasta ) ya que el cada Auction guarda 5 hoteles aleatoriamente.
	 * 
	 * @param array $hotelId ID de hotel a buscar
	 * @param array $conditions Condiciones a buscar en las habitaciones
	 */
	public function getRandomRoomByHotelId($hotelId, $conditions = array())
	{
		if (empty($hotelId) || $hotelId == 0) return false;

		$dbInst = DB::table("turi_rooms")
			->join("turi_hotels", "turi_rooms.room_hotel", "=", "turi_hotels.hotel_id")
			->where('room_hotel', $hotelId);

		if (!empty($conditions)) {
			$dbInst->where($conditions);
		}

		return $dbInst->inRandomOrder()->first();
	}

	/**
	 * Obtener una habitación por ID
	 *
	 * @param int $roomid RoomID a buscar
	 * @return void
	 */
	public function getRoomById($roomid)
	{
		return RoomModel::find($roomid);
	}

	/**
	 * VIEW /room/mis-habitaciones
	 * Mostar habitaciones de el hotel actual
	 */
	public function view__showMyRooms()
	{
		if (!currentUserHasRol('hotel') && !currentUserHasRol('admin')) {
			return response()->json(array(
				'error' => true,
				'message' => 'No allowed to read this content.'
			));
		}

		$rooms = RoomModel::where('room_hotel', session()->get('user_hotel'))->get();
		$activeCount = getCountOfAttributes($rooms, 'room_active');

		return view('room.ver-habitaciones', array(
			'roomcount' => $rooms->count(),
			'actives' => $activeCount
		));
	}


	/**
	 * GET  room/get-by-hotel-id
	 * Obtener todas las habitaciones de un hotel por ID
	 */
	public function ajax__getRoomsByHotelId($hotelID)
	{
		$results = DB::table('turi_rooms')
			->join('turi_hotels', 'turi_rooms.room_hotel', '=', 'turi_hotels.hotel_id')
			->where("turi_hotels.hotel_id", $hotelID)
			->get();

		return response()->json($results);
	}

	/**
	 * /room/paginador
	 * Paginador de habitaciones de el actual hotel.
	 *
	 * @param Request $request
	 */
	public function ajax__getPaginator(Request $request)
	{

		$count = $request->get("count", 5);
		$start = intval($request->get("start", 0) - 1) * $count;

		$rooms = RoomModel::where('room_hotel', session()->get('user_hotel'))
			->select(array(
				'room_id',
				'room_name',
				'room_adults',
				'room_minprice',
				'room_maxprice',
				'room_active',
				'room_created'
			));

		if ($request->exists("orderby")) {
			$orderType = 'desc';
			if ($request->exists("order")) {
				if (in_array($request->get("order"), ["ASC", "DESC"])) {
					$orderType = $request->get("order");
				}
			}

			$orderBy = $request->get("orderby");
			if (in_array($orderBy, ["room_created", 'room_minprice', 'room_maxprice', 'room_adults', 'room_name'])) {
				$rooms->orderBy($orderBy, $orderType);
			}
		}

		if ($request->exists('room_active')) {
			$rooms->where('room_active', $request->get('room_active'));
		}

		$rooms->skip($start)->take($count);
		$roomJson = array();
		foreach ($rooms->get() as $value) {
			$roomChanged = new stdClass();
			$roomChanged->room_id = $value->room_id;

			$roomChanged->room_minprice = formatPrice($value->room_minprice);
			$roomChanged->room_maxprice = formatPrice($value->room_maxprice);
			$roomChanged->room_created = formatDate($value->room_created);
			$roomChanged->room_active = $value->room_active === 1 ? 'Activo' : 'Inactivo';
			$roomChanged->room_name = $value->room_name;
			$roomChanged->room_adults = $value->room_adults;
			$roomJson[] = $roomChanged;
		}

		return response()->json($roomJson);
	}
}
