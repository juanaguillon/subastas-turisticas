<?php

namespace App\Http\Controllers;

use App\Models\AuctionModel;
use App\Models\AuctionRowModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use stdClass;

class AuctionRowController extends Controller
{
	/**
	 * POST /auctionrow/create
	 * Crear una nueva nueva fila de subasta.
	 * @param Request $request
	 * @return void
	 */
	public function createAuctionRow(Request $request)
	{
		/**
		 * El proceso es el siguiente:
		 * Se obtendrán una cantidad de hoteles habilitados para la subasta (AuctionController::createAuction para ver el proceso
		 * de obtención de hoteles), segun esos hoteles se obtendrá una habitación aleatoria, estos datos se guardaran en la tabla MySql auctions. 
		 * Si se ha guardado correctamente el nuevo auction, se renderizará inputs:hidden (Puede ver views/components/auction-details)
		 * con el AuctionID (auctionrow_init), canidad de hoteles a procesar, donde puede ser 1 o 5 ( auction_maxhotels ) y el array key de  
		 * el actual hotel (auction_lastnum)
		 * 
		 * Si se renderiza correctmente, creará una petición via Ajax para entrar el actual método ( self::createAuctionRow ), pasando
		 * estos tres datos renderizados en los input:hidden (auctionrow_id,auctionrow_hotelcount,auction_lastnum) 
		 */
		$autctionID = $request->input('auctionrow_init');
		$hotelCounts = $request->input('auctionrow_maxhotels');
		$lastNum = $request->input('auctionrow_lastnum');
		$auctionTime = $request->input('auctionrow_time');

		// Reinicializar el array de hoteles que se están procesando.
		if ($lastNum == $hotelCounts) {
			$lastNum = 0;
		}

		// dd($hotelCounts)
		$auctionController = new AuctionController();
		$auctionResult = $auctionController->getAuctionById($autctionID);

		if (!$auctionResult) return response()->json(array(
			'error' => true,
			'message' => 'Subasta no encontrada'
		));

		$minPrice = $auctionResult->auction_minval;
		$maxPrice = $auctionResult->auction_maxval;
		$adults = $auctionResult->auction_adults;
		$childs = $auctionResult->auction_childrens;
		$infants = $auctionResult->auction_infants;
		$hotelIds = $auctionResult->auction_hotels;

		$hotelToProcess = $hotelIds[$lastNum];
		// dd($hotelToProcess);
		$roomController = new RoomController();

		$condicionRooms = array(
			["turi_rooms.room_minprice", ">=", $minPrice],
			["turi_rooms.room_maxprice", "<=", $maxPrice],
			["turi_rooms.room_adults", ">=", $adults]
		);
		if ($childs) {
			$condicionRooms[] =	array("turi_rooms.room_childs", ">=", $childs);
		}
		if ($infants) {
			$condicionRooms[] =	array("turi_rooms.room_babys", ">=", $infants);
		}

		$roomResponse = $roomController->getRandomRoomByHotelId($hotelToProcess, $condicionRooms);
		if (!$roomResponse) {
			return response()->json(array(
				"error" => true,
				"message" => "No se puede crear subastas parámetros proporcionados."
			));
		}


		$paramToCreate = array(
			"auctionrow_auction" => $autctionID,
			"auctionrow_number" => 1,
			"auctionrow_price" => $maxPrice,
			"auctionrow_hotel" => $hotelToProcess,
			"auctionrow_room" => $roomResponse->room_id,
			"auctionrow_time" =>  $auctionTime,
			"auctionrow_user" => session()->get("user_id"),
			"auctionrow_active" => true
		);

		$preAuction = AuctionRowModel::where([["auctionrow_auction", "=", $autctionID]])
			->orderBy("auctionrow_updated", "DESC")
			->get();

		if ($preAuction->count() > 0) {
			$foundPreRow = false;
			/**
			 * Verificar si ya se ha creado un auctionRow con el actual hotelID
			 */
			foreach ($preAuction as $prerow) {
				if ($prerow->auctionrow_hotel == $hotelToProcess) {
					$foundPreRow = $prerow;
					break;
				}
			}

			if ($foundPreRow) {
				$foundPreRow->auctionrow_price = $preAuction[0]->auctionrow_price - 10000;
				$foundPreRow->auctionrow_time = $auctionTime;
				$foundPreRow->auctionrow_room = $roomResponse->room_id;
				$foundPreRow->save();
				$auctionRowModel = $foundPreRow;
			} else {
				$paramToCreate["auctionrow_price"] = $preAuction[0]->auctionrow_price - 10000;
				$auctionRowModel = AuctionRowModel::create($paramToCreate);
			}
		} else {
			$auctionRowModel = AuctionRowModel::create($paramToCreate);
		}


		if (!$auctionRowModel) {
			return response()->json(array(
				"error" => true,
				"message" => "Error al obtener una nueva subasta, intente nuevamente"
			));
		}
		$roomResponse->auctionrow_id = $auctionRowModel->auctionrow_id;
		$roomResponse->auctionrow_time = $auctionRowModel->auctionrow_time;
		$roomResponse->auctionrow_price = $auctionRowModel->auctionrow_price;
		$jsonResponse = array(
			"error" => false,
			"newauction" => $auctionRowModel,
			"roomrender" => view("components.auctionrow-row", ["data" => $roomResponse])->render(),
			"hotelkey" => $lastNum + 1,
			'roomres' => $roomResponse
		);

		/**
		 * Si solo se está subastando con 1 hotel, no se obtendran nuevas AuctionRows, ya que solo estará subastando un hotel con el criterio establecido
		 */
		if ($hotelCounts  > 1) {
			$jsonResponse["iterate"] = true;
		} else {
			$jsonResponse["iterate"] = false;
		}

		return response()->json($jsonResponse);
	}

	/**
	 * HotelRol /auction/gestionar-fila/
	 * Actualizar un auctionRow
	 *
	 * @param int $aurowid
	 */
	public function manageAuctionRow($aurowid)
	{
		$auModel = AuctionRowModel::find($aurowid);
		if (!$auModel) return response()->json(array(
			"error" => true,
			"message" => "Oferta de subasta no encontrada"
		));

		$hotelControll = new HotelController();
		$auctionController = new AuctionController();
		$auctionData = $auctionController->getAuctionById($auModel->auctionrow_auction);
		$hotelData = $hotelControll->getHotelById($auModel->auctionrow_hotel);
		$allPersons = intval($auctionData->auction_adults) + intval($auctionData->auction_childrens);
		$allPersons += intval($auctionData->auction_infants);

		$roomController = new RoomController();
		$allRooms = $roomController->getAllRooms(session()->get('user_hotel'));

		$auctionData->auction_hotelscount = count($auctionData->auction_hotels);

		return view("auctionrow.update-auctionrow", array(
			"auctionrow" 	=> $auModel,
			"auction" 		=> $auctionData,
			"hotel"				=> $hotelData,
			"persons"			=> $allPersons,
			'rooms' 			=> $allRooms
		));
	}

	/**
	 * Obtener todas las ofertas de subastas por auctionID
	 * GET /auctionrow/get-lastests/
	 * @param int $auctionid AuctionID a buscar.
	 * @param boolean $get Obtener resultados u obtener instancia de Modelo.
	 * @return mixed Resultados o instancia de DB
	 */
	public function getAllAuctionsRowByAuctionID($auctionid, $get = true)
	{
		$resultDB = DB::table('turi_auctionrows')
			->join('turi_rooms', 'turi_auctionrows.auctionrow_room', '=', 'turi_rooms.room_id')
			->join('turi_hotels', 'turi_auctionrows.auctionrow_hotel', '=', 'turi_hotels.hotel_id')
			->where("auctionrow_auction", $auctionid)
			->orderBy("auctionrow_updated", "DESC");
		if ($get) {
			$dataToRes = $resultDB->get();
			return response()->json(array(
				'auctions' => $dataToRes,
				'render' => view('components.auctionrow-row', array(
					'loopData' => $dataToRes
				))->render()
			));
		} else {
			return $resultDB;
		}
	}

	/**
	 * HotelRol /auctiorow/agregar-fila
	 * Agregar una oferta ( AuctionRow ) de una subasta. 
	 * Esto sera util para enviar una subasta manualmente desde el hotel
	 *
	 * @param Request $request
	 */
	public function agregarAuctionRow(Request $request)
	{
		$validatorKeys = array(
			'auctionrow_room' => 'required|numeric|exists:turi_rooms,room_id',
			'auctionrow_price' => 'required|numeric',
			'auctionrow_auction' => 'required|numeric',
			'auctionrow_time' => 'required|numeric',
		);
		$validator = Validator::make($request->all(), $validatorKeys);

		if ($validator->fails()) {
			return response()->json(
				array(
					'error' => true,
					'message' => $validator->errors()->first()
				)
			);
		}
		$auRowSave = $request->all(array_keys($validatorKeys));
		$currentHotel =  session()->get('user_hotel');
		$auRowSave['auctionrow_user'] = null;
		$auRowSave['auctionrow_active'] = 1;
		$auRowSave['auctionrow_number'] = 1;
		$auRowSave['auctionrow_number'] = $currentHotel;

		$preAuRow = AuctionRowModel::where('auctionrow_hotel', $currentHotel)
			->where('auctionrow_auction', $auRowSave['auctionrow_auction'])->first();
		if ($preAuRow) {
			$preAuRow->auctionrow_room = $auRowSave['auctionrow_room'];
			$preAuRow->auctionrow_time = $auRowSave['auctionrow_time'];
			$preAuRow->auctionrow_price = $auRowSave['auctionrow_price'];
			$savedUpRow = $preAuRow->save();
		} else {
			$savedUpRow  = AuctionRowModel::create($auRowSave);
		}

		if ($savedUpRow) {
			return response()->json(array(
				'error' => false,
				'message' => 'Oferta de subasta actualizada correctamente'
			));
		} else {
			return response()->json(array(
				'error' => true,
				'message' => 'Error al actalizar la oferta, intente nuevamente'
			));
		}
	}

	/**
	 * Obtener los datos de una Fila de Subasta por ID
	 *
	 * @param int $id ID De el auctionRow ( Fila de subasta ID)
	 * @param boolean $getAuctionData Obtener los datos de el Auction
	 * @param boolean $getHotelData Obtener los datos de el hotel
	 * @param boolean $getRoomData Obtener los datos de la habitación actual
	 * @return array Se rotarnará la primer fila que se encuentra con los datos que se intentan obtener.
	 */
	public function getAuctionRowData($id, $getAuctionData = false, $getHotelData = false, $getRoomData = false)
	{
		$dbInstance = DB::table("turi_auctionrows")
			->where("auctionrow_id", $id);

		if ($getAuctionData) {
			$dbInstance->join("turi_auctions", "auctionrow_auction", "=", "auction_id");
		}
		if ($getHotelData) {
			$dbInstance->join("turi_hotels", "auctionrow_hotel", "=", "hotel_id")
				->join("turi_cities", "hotel_city", "=", "city_id");
		}
		if ($getRoomData) {
			$dbInstance->join("turi_rooms", "auctionrow_room", "=", "room_id");
		}
		return $dbInstance->first();
	}

	/**
	 * VIEW Ver los datos de un AuctionRow por ID
	 *
	 * @param [type] $id
	 * @return void
	 */
	public function view__showAuctionRowByID($id)
	{
		$data = $this->getAuctionRowData($id, true, true, true, true);
		return view("auctionrow.ver-auctionrow", array(
			"auctionrow" => $data
		));
	}

	/**
	 * GET /auctionrow/get-last-by-auction-id
	 * Obtener la última fila de subasta según una subasta ID.
	 * @param int $auctionid Debe ser relación con la tabla auction y no auctionRow
	 */
	public function getLastAuctionRowByAuctionID($auctionid)
	{
		$auctionRow = $this->getAllAuctionsRowByAuctionID($auctionid, false)->first();
		return response()->json(array(
			"data" => $auctionRow,
			"roomrender" => view("components.auctionrow-row", array(
				"data" => $auctionRow,
			))->render()
		));
	}
}
