<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function makeLogin(Request $request)
    {
        $userController = new UserController();
        return $userController->loginUser($request, true);
    }
}
