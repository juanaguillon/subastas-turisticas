<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	 * Validar el guardado de imagenes. Creara un orden con las imagenes ingresadas, en caso que todo funcione, retornara un array de las imagenes guardadas. Cada imagen tambien sera un array con tamanos original, XL, large, 765, 365 y el nombre original de la imagen. 
	 *
	 * @param Request $request RequestObject
	 * @param string $fieldImages Input de la imagenes
	 * @param string $fieldImageIndex Input de orden de las imagenes
	 * @param string $folder Folder donde se debe guardar las imagenes
	 * @return array Se retorana array vacío si no existen imagenes ingresadas en el request.
	 */
	public function validateImages(Request $request, String $fieldImages, String $fieldImageIndex, $folder = "")
	{
		$imagesToSave = array();
		$imagesOrder = $request->input($fieldImageIndex);
		if ($imagesOrder !== "" && $imagesOrder !== null) {

			$imagesOrder = explode(',', trim($imagesOrder));

			$filesRequest = $request->file($fieldImages);

			foreach ($imagesOrder as $value) {
				$imagesToSave[] = saveImage($filesRequest[$value], $folder);
			}
		}
		return $imagesToSave;
	}
}
