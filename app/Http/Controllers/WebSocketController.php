<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class WebSocketController extends Controller implements MessageComponentInterface
{
	protected $clients;
	protected $clientsAuctions = array();
	protected $connections = array();

	public function __construct()
	{
		$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn)
	{
		// Store the new connection to send messages to later
		$this->clients->attach($conn);
		$this->users[$conn->resourceId] = $conn;
		echo "New connection! ({$conn->resourceId})\n";
	}

	public function onMessage(ConnectionInterface $from, $msg)
	{
		$dataMsg = json_decode($msg);
		switch ($dataMsg->caseSub) {
			case 'auctionSub':
				$this->clientsAuctions[] = array(
					'user' => $from->resourceId,
					'auction' => $dataMsg->auctionID
				);
				break;
			case 'auctionSet':
				foreach ($this->clientsAuctions as $client) {
					if ($client['auction'] == $dataMsg->auctionID) {
						$this->users[$client['user']]->send(1);
					}
				}
				break;
		}
	}

	public function onClose(ConnectionInterface $conn)
	{
		// The connection is closed, remove it, as we can no longer send it messages
		$this->clients->detach($conn);

		echo "Connection {$conn->resourceId} has disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e)
	{
		echo "An error has occurred: {$e->getMessage()}\n";

		$conn->close();
	}
}
