<?php

namespace App\Http\Controllers;

use App\Models\PlanModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PlanController extends Controller
{

	private $table = "turi_planes";

	/**
	 * Obtener datos de un plan por iD
	 *
	 * @param int $id ID de plan a obtener
	 * @param int $getCityData Obtener los datos de ciudad de el Plan
	 * @param int $getHotelData Obtener los datos de hotel de el Plan
	 * @param int $getRoomData Obtener los datos de habitacion de el Plan
	 * @return array|null
	 */
	public function getPlanById($id, $getCityData = false, $getHotelData = false, $getRoomData = false)
	{
		$dbInstance = DB::table("{$this->table}")
			->where("{$this->table}.plan_id", $id);
		// ->where("{$this->table}.plan_active", true);

		if ($getCityData) {
			$dbInstance->join("turi_cities", "{$this->table}.plan_city", "=", "turi_cities.city_id");
		}
		if ($getHotelData) {
			$dbInstance->join("turi_hotels", "{$this->table}.plan_hotel", "=", "turi_hotels.hotel_id");
		}
		if ($getRoomData) {
			$dbInstance->join("turi_rooms", "{$this->table}.plan_room", "=", "turi_rooms.room_id");
		}

		return $dbInstance->groupBy("plan_id")->first();
	}

	public function createPlan(Request $request)
	{
		$keyProccess = array(
			'plan_name' 						=> 'required',
			'plan_city' 						=> 'required|numeric',
			'plan_room' 						=> 'required|numeric',
			'plan_hotel' 						=> 'required|numeric',
			'plan_rooms_quantity' 	=> 'required|numeric',
			'plan_price' 						=> 'required|numeric',
			'plan_currency' 				=> array(
				'required',
				Rule::in(['COP', 'USD'])
			),
			'plan_mindate' 					=> 'required|date_format:d-m-Y',
			'plan_maxdate' 					=> 'required|date_format:d-m-Y',
			'plan_active' 					=> 'required|boolean',
			'plan_banner_active' 		=> 'required|boolean',
			'plan_special_service' 	=> 'max:3',
			'plan_services' 				=> 'array',
			'plan_promotion' 				=> '',
			'plan_description' 			=> '',
			'plan_terms' 						=> '',
		);

		$validator = Validator::make($request->all(), $keyProccess);

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors()->first());
		}

		$dataGetting = $request->all(array_keys($keyProccess));
		foreach ($dataGetting['plan_special_service'] as $key => $service) {
			if (empty($service['title'])) {
				unset($dataGetting['plan_special_service'][$key]);
			}
		}

		$imagesValidator = $this->validateImages($request, "plan_images", "plan_indexes_imgs", "plan/");

		$dataGetting['plan_special_service'] = json_encode($dataGetting['plan_special_service']);
		$dataGetting['plan_images'] = json_encode($imagesValidator);

		$dataGetting['plan_services'] = json_encode($dataGetting['plan_services']);
		$dataGetting['plan_mindate'] = date('Y-m-d', strtotime($dataGetting['plan_mindate']));
		$dataGetting['plan_maxdate'] = date('Y-m-d', strtotime($dataGetting['plan_maxdate']));

		$dataGetting['plan_created'] = Carbon::now()->toDateTimeLocalString();
		$dataGetting['plan_updated'] = Carbon::now()->toDateTimeLocalString();
		$insertedPlan = PlanModel::insert($dataGetting);

		if ($insertedPlan) {
			return redirect('plan/create?new_reg=true');
		} else {
			return redirect()->back()->withErrors('Error al crear el nuevo plan. Intenten nuevamente');
		}
	}


	/**
	 * (ADMIN) POST /plan/editar-plan/{planid} Editar el plan
	 *
	 * @param Request $request
	 * @param int $planid
	 * @return void
	 */
	public function editPlan(Request $request, $planid)
	{
		$savedPlan = PlanModel::find($planid);

		if (!$savedPlan) {
			return;
		}

		$keyProccess = array(
			'plan_name' 						=> 'required',
			'plan_city' 						=> 'required|numeric',
			'plan_room' 						=> 'required|numeric',
			'plan_hotel' 						=> 'required|numeric',
			'plan_rooms_quantity' 	=> 'required|numeric',
			'plan_price' 						=> 'required|numeric',
			'plan_currency' 				=> array(
				'required',
				Rule::in(['COP', 'USD'])
			),
			'plan_mindate' 					=> 'required|date_format:d-m-Y',
			'plan_maxdate' 					=> 'required|date_format:d-m-Y',
			'plan_active' 					=> 'required|boolean',
			'plan_banner_active' 		=> 'required|boolean',
			'plan_special_service' 	=> 'max:3',
			'plan_services' 				=> 'array',
			'plan_promotion' 				=> '',
			'plan_description' 			=> '',
			'plan_terms' 						=> '',
		);

		$validator = Validator::make($request->all(), $keyProccess);

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors()->first());
		}

		$planImages = $savedPlan->plan_images;

		if (!$request->hasFile("plan_images") && !empty($request->input("plan_indexes_imgs"))) {
			$newOrderImages = array();

			$orderKeys = explode(",", $request->input("plan_indexes_imgs"));
			foreach ($orderKeys as $orderkey => $ordval) {
				$newOrderImages[] = $planImages[$ordval];
			}
			$savedPlan->plan_images = $newOrderImages;
		} else if ($request->hasFile('plan_images')) {
			foreach ($planImages as $arrImags) {
				foreach ($arrImags as $img) {
					deleteImage($img, 'plan');
				}
			}
			$newImages = $this->validateImages($request, 'plan_images', 'plan_indexes_imgs', 'plan/');
			$savedPlan->plan_images = $newImages;
		}


		$savedPlan->plan_name 						= $request->input("plan_name");
		$savedPlan->plan_city 						= $request->input("plan_city");
		$savedPlan->plan_room 						= $request->input("plan_room");
		$savedPlan->plan_hotel 						= $request->input("plan_hotel");
		$savedPlan->plan_rooms_quantity 	= $request->input("plan_rooms_quantity");
		$savedPlan->plan_price 						= $request->input("plan_price");
		$savedPlan->plan_currency 				= $request->input("plan_currency");
		$savedPlan->plan_mindate 					= date('Y-m-d', strtotime($request->input("plan_mindate")));
		$savedPlan->plan_maxdate 					= date('Y-m-d', strtotime($request->input("plan_maxdate")));
		$savedPlan->plan_active 					= $request->input("plan_active");
		$savedPlan->plan_banner_active 		= $request->input("plan_banner_active");
		$savedPlan->plan_special_service 	= $request->input("plan_special_service");
		$savedPlan->plan_services 				= $request->input("plan_services");
		$savedPlan->plan_promotion 				= sanitizeHtml($request->input("plan_promotion"));
		$savedPlan->plan_description 			= sanitizeHtml($request->input("plan_description"));
		$savedPlan->plan_terms 						= sanitizeHtml($request->input("plan_terms"));

		if ($savedPlan->save()) {
			return redirect("plan/editar-plan/{$savedPlan->plan_id}?new_reg=true");
		} else {
			return redirect()->back()->withErrors('Error al actualizar el plan. Intente nuevamente');
		}
	}

	/**
	 * (ADMIN) VIEW /plan/editar-plan/{planid}
	 */
	public function view__editPlan($planid)
	{
		$savedPlan = $this->getPlanById($planid, true, true, true);
		if (!$savedPlan) {
			return response()->json(array(
				'error' => true,
				'message' => 'Plan not found'
			));
		}

		return view('plan.create-plan', array(
			'updating' => true,
			'plan' => $savedPlan
		));
	}

	/**
	 * Obtener todos los planes activos 
	 * @param integer $limit
	 * @return void
	 */
	public function getActivePlans($limit = 5)
	{
		return DB::table("turi_planes")
			->join("turi_cities", "turi_planes.plan_city", "=", "turi_cities.city_id")
			->where("plan_active", true)
			->inRandomOrder()
			->select("*")
			->groupBy("plan_id")
			->take($limit)->get();
	}

	/**
	 * Ver y renderizar un plan por ID
	 * @param int $planid
	 * @return void
	 */
	public function view__showPlan($planid)
	{
		$plan = $this->getPlanById($planid, true, true, true);
		if (!$plan) {
			return response()->json(array(
				"error" => true,
				"message" => "El plan que está intentando ver no existe."
			));
		}
		return view("plan.ver-plan", array(
			"plan" => $plan
		));
	}

	/**
	 * Obtener los datos de todos los planes
	 *
	 * @param array $params Parámetros para filtrar las columnas
	 * @return array Resultados que se han encontrado 
	 */
	public function getAllPlans($params = array())
	{
		$planInstance = DB::table("turi_planes")
			->join("turi_cities", "plan_city", "=", "city_id")
			->join("turi_hotels", "plan_hotel", "=", "hotel_id")
			->select(array(
				"plan_id",
				"plan_name",
				"plan_price",
				"plan_currency",
				"plan_created",
				"plan_active",
				"hotel_id",
				"hotel_name",
				"city_id",
				"city_name"
			));

		if (array_key_exists("where", $params)) {
			$planInstance->where($params["where"]);
		}

		if (array_key_exists("start", $params)) {
			$planInstance->skip($params["start"]);
		}

		if (array_key_exists("count", $params)) {
			$planInstance->take($params["count"]);
		}
		if (array_key_exists("orderby", $params) && array_key_exists("order", $params)) {
			$planInstance->orderBy($params["orderby"], $params["order"]);
		}



		$allPlans = $planInstance->get();
		foreach ($allPlans as $plank => $planv) {
			$planv->plan_pricing = formatPrice($planv->plan_price, $planv->plan_currency);
			$planv->plan_date = formatDate($planv->plan_created);
			$planv->plan_activetext = $planv->plan_active == 1 ? "Activo" : "Inactivo";
		}
		return $allPlans;
	}

	/**
	 * VIEW /plan/ver-planes
	 * Ver los planes actuales
	 *
	 * @param Request $request
	 * @return void
	 */
	public function view__showAllPlans(Request $request)
	{
		$planes = $this->getAllPlans();

		$countCity = getCountOfAttributes($planes, "city_id");
		$hotelCount = getCountOfAttributes($planes, "hotel_id");
		$statusCount = getCountOfAttributes($planes, "plan_active");

		return view("plan.ver-planes", array(
			"cities" => $countCity,
			"hotels" => $hotelCount,
			"status" => $statusCount,
			"planes" => $planes
		));
	}

	/**
	 * /plan/paginador
	 * Paginador de Planes
	 * @return void
	 */
	public function ajax__getPaginator(Request $request)
	{
		$count = $request->get("count", 5);
		$start = intval($request->get("start", 0) - 1) * $count;
		$orderType = 'desc';
		$orderBy = "plan_created";

		if ($request->exists("orderby")) {

			if (in_array($request->get("orderby"), ["plan_name", 'plan_price', 'hotel_name', 'city_name', "plan_active", "plan_created"])) {
				$orderBy = $request->get("orderby");
			}
			if ($request->exists("order")) {
				if (in_array($request->get("order"), ["ASC", "DESC"])) {
					$orderType = $request->get("order");
				}
			}
		}
		$where = array();
		if ($request->exists("plan_city")) {
			$where[] = array("plan_city", $request->get("plan_city"));
		}
		if ($request->exists("plan_hotel")) {
			$where[] = array("plan_hotel", $request->get("plan_hotel"));
		}
		if ($request->exists("plan_active")) {
			$where[] = array("plan_active", $request->get("plan_active"));
		}

		$resultados = $this->getAllPlans(array(
			"count" => $count,
			"start" => $start,
			"orderby" => $orderBy,
			"order" => $orderType,
			"where" => $where
		));

		return response()->json($resultados);
	}
}
