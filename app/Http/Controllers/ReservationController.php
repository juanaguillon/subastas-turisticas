<?php

namespace App\Http\Controllers;

use App\Custom\FPDFReservation;
use App\Models\ReservationModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use stdClass;

class ReservationController extends Controller
{

	/**
	 * Renderizar el tipo de reserva que se esta usando actualmente.
	 *
	 * @param int $typeID ID de hotsale, plan u auctionrow
	 * @param string $type Tipo de reserva, puede ser plan, auction o hotsale.
	 * @return array Datos que se obteniene dependiento de las especificaciones
	 */
	private function checkTypeOfReservation($typeID, $type)
	{
		if ($type === "plan") {
			$model = new PlanController();
			$reservaTypeModel = $model->getPlanById($typeID);
			$reservaTName = $reservaTypeModel->plan_name;
			$reservaTID = $reservaTypeModel->plan_id;
			$reservaTMinDate = $reservaTypeModel->plan_mindate;
			$reservaTMaxDate = $reservaTypeModel->plan_maxdate;
			$reservaTText = "Plan";
		} else if ($type === "auction") {
			$model = new AuctionRowController();
			$reservaTypeModel = $model->getAuctionRowData($typeID, true, true);
			$reservaTName = "Oferta en " . $reservaTypeModel->hotel_name;
			$reservaTID = "Oferta en " . $reservaTypeModel->auctionrow_id;
			$reservaTMinDate = "Oferta en " . $reservaTypeModel->auction_init;
			$reservaTMaxDate = "Oferta en " . $reservaTypeModel->auction_end;
			$reservaTText = "Oferta";
		} else if ($type === "hotsale") {
			$model = new HotsaleController();
			$reservaTypeModel = $model->getHotsaleDataById($typeID);
			$reservaTName = $reservaTypeModel->hotsale_name;
			$reservaTID = $reservaTypeModel->hotsale_id;
			$reservaTMinDate = $reservaTypeModel->hotsale_mindate;
			$reservaTMaxDate = $reservaTypeModel->hotsale_maxdate;
			$reservaTText = "Hotsale";
		}

		return array(
			"reserva_name" => $reservaTName,
			"reserva_id" => $reservaTID,
			"reserva_init" => $reservaTMinDate,
			"reserva_end" => $reservaTMaxDate,
			"reserva_type" => $reservaTText
		);
	}


	/**
	 * Obtener texto del estado de la reserva
	 *
	 * @param int $statusNumber Estado de la reserva a procesar, puede ser 1, 2 o 3
	 * @return void
	 */
	private function checkStateOfReservation($statusNumber)
	{
		switch ($statusNumber) {
			case 1:
				return "Pendiente";
				break;
			case 2:
				return "Pagada";
				break;
			case 3:
				return "Cancelada";
				break;
			default:
				return "Sin estado";
				break;
		}
	}

	/**
	 * Obtener los datos de una reserva específica.
	 *
	 * @param int $reservaID ID de reserva a buscar
	 * @param boolean $getHotelData Obtener los datos de hotel ( Tambien de la ciudad )
	 * @return object Datos de la reserva.
	 */
	public function getReservationByID($reservaID, $getHotelData = false)
	{
		$reserva = DB::table("turi_reservations")
			->where("reservation_id", $reservaID);

		if ($getHotelData) {
			$reserva->join("turi_hotels", "reservation_hotel", "=", "hotel_id");
			$reserva->join("turi_cities", "hotel_city", "=", "city_id");
		}

		$reservaFull =  $reserva->first();
		$reservaData = $this->checkTypeOfReservation($reservaFull->reservation_typeid, $reservaFull->reservation_type);
		$reservaData["reserva_estadotext"] =  $this->checkStateOfReservation($reservaFull->reservation_status);
		return array(
			"reserva" => $reservaFull,
			"reserva_data" => $reservaData
		);
	}

	/**
	 * Obtener todas las reservas por un usuario en específico.
	 *
	 * @param int $userID ID de usuario a buscar
	 * @param bool $getHotelData Obtener los datos de el hotel de reserva
	 * @param array $otherParams Puede enviar otros parámetros, Puede ser "start", "limit", "orderby", "order", o un "where" con un array estilo where laravel.
	 * @return array Datos de todas las filas encontradas
	 */
	public function getAllReservationsByUser($userID, $getHotelData = false, $otherParams = array())
	{

		if ($userID === "admin") {
			if (!currentUserHasRol("admin")) {
				return response()->json(array(
					"error" => true,
					"message" => "A No allowed to show reservations."
				));
			}
			$reservasDV = DB::table("turi_reservations");
		} else if ($userID === "hotel") {
			if (!currentUserHasRol("hotel")) {
				return response()->json(array(
					"error" => true,
					"message" => "H No allowed to show reservations.."
				));
			}
			$reservasDV = DB::table("turi_reservations")
				->where("reservation_hotel", session()->get("user_hotel"));
		} else {
			$reservasDV = DB::table("turi_reservations")
				->where("reservation_user", $userID);
		}


		if ($getHotelData) {
			$reservasDV->join("turi_hotels", "reservation_hotel", "=", "hotel_id");
			$reservasDV->join("turi_cities", "hotel_city", "=", "city_id");
		}

		if (array_key_exists("where", $otherParams)) {
			$reservasDV->where($otherParams["where"]);
		}

		if (array_key_exists("start", $otherParams)) {
			$reservasDV->skip($otherParams["start"]);
		}
		if (array_key_exists("limit", $otherParams)) {
			$reservasDV->take($otherParams["limit"]);
		}

		$order = "ASC";
		if (array_key_exists("order", $otherParams)) {
			$order = $otherParams["order"];
		}

		if (array_key_exists("orderby", $otherParams)) {
			$reservasDV->orderBy($otherParams["orderby"], $order);
		}

		$selects = array(
			"city_id",
			"city_name",
			"hotel_name",
			"hotel_id",
			"hotel_city",
			"reservation_price",
			"reservation_id",
			"reservation_typeid",
			"reservation_status",
			"reservation_type",
			"reservation_created",
			"reservation_currency",
			"reservation_clientname",
		);

		$reservasResult =  $reservasDV->select($selects)
			->groupBy("reservation_id")
			->get();

		foreach ($reservasResult as $reserva) {
			$reserva->reservation_statustext = $this->checkStateOfReservation($reserva->reservation_status);
			$reserva->reservation_price = formatPrice($reserva->reservation_price, $reserva->reservation_currency);
			$reserva->reservation_date = formatDate($reserva->reservation_created);
			$reserva->reservation_typetext = $this->checkTypeOfReservation($reserva->reservation_typeid, $reserva->reservation_type)["reserva_type"];
		}
		return $reservasResult;
	}


	/**
	 * Este proceso se hará cuando se envio formulario de reservar ( Se puede ver en Hotsales, Planes o AuctionRows )
	 * @param Request $request
	 * @return void
	 */
	public function redirectToPayU(Request $request)
	{
		$keysToPRocess = array(
			"reservation_type" => array(
				"required",
				Rule::in(array(
					"hotsale",
					"plan",
					"auction"
				))
			),
			"reservation_docnumber" => "required|numeric",
			"reservation_doctype" => array(
				"required",
				Rule::in(array(
					"nit",
					"cc",
					"ci",
					"ce"
				))
			),
			"reservation_username" => "required|string",
			"reservation_useremail" => "required|email",
			"reservation_phone" => "required|numeric",
			"reservation_caseid" => "required|numeric",
			"reservation_city" => "required|string",
			"reservation_address" => "required|string"
		);
		$dataSender = $request->all(array_keys($keysToPRocess));
		$validator = Validator::make($dataSender, $keysToPRocess);
		if ($validator->fails()) {
			return response()->json(array(
				"error" => true,
				"message" => $validator->errors()->first()
			));
		}

		$encodeName = iconv(mb_detect_encoding($dataSender["reservation_username"], mb_detect_order(), true), "UTF-8", $dataSender["reservation_username"]);
		$dataSender["reservation_username"] = ucwords(mb_strtolower($encodeName));
		$dataSender["reservation_useremail"] = strtolower($dataSender["reservation_useremail"]);

		switch ($dataSender['reservation_type']) {
			case 'hotsale':
				$hotsaleController = new HotsaleController();
				$hotsaleID = $hotsaleController->getHotsaleDataById($dataSender['reservation_caseid'], false, true);
				$priceControl = $hotsaleID->hotsale_price;
				$hotelPlan = $hotsaleID->hotel_id;
				$currencyControl = $hotsaleID->hotsale_currency;
				$nameControll = $hotsaleID->hotsale_name;
				break;
			case 'plan':
				$planController = new PlanController();
				$planID = $planController->getPlanById($dataSender['reservation_caseid'], false, true);
				$priceControl = $planID->plan_price;
				$hotelPlan = $planID->hotel_id;
				$currencyControl = $planID->plan_currency;
				$nameControll = $planID->plan_name;
				break;
			case 'auction':
				$aurowController = new AuctionRowController();
				$aurowID = $aurowController->getAuctionRowData($dataSender['reservation_caseid'], true, true);
				$priceControl = $aurowID->auctionrow_price * $aurowID->auction_days;
				$hotelPlan = $aurowID->hotel_id;
				$currencyControl = 'COP';
				$nameControll = 'Reservar subasta en Hotel ' . $aurowID->city_name;
				break;
			default:
				return;
				break;
		}

		$reservationModel = new ReservationModel();

		$reservationModel->reservation_code = 3;
		$reservationModel->reservation_clientname = $dataSender['reservation_username'];
		$reservationModel->reservation_clientmail = $dataSender['reservation_useremail'];
		$reservationModel->reservation_clientphone = $dataSender['reservation_phone'];
		$reservationModel->reservation_clientcity = $dataSender['reservation_city'];
		$reservationModel->reservation_clientadress = $dataSender['reservation_address'];
		$reservationModel->reservation_price = $priceControl;
		$reservationModel->reservation_currency = $currencyControl;
		$reservationModel->reservation_clientdoctype = $dataSender['reservation_doctype'];
		$reservationModel->reservation_clientdocnumber = $dataSender['reservation_docnumber'];
		$reservationModel->reservation_status = 1;
		$reservationModel->reservation_hotel = $hotelPlan;
		$reservationModel->reservation_user = isAuth() ? getCurrentUserId() : null;
		$reservationModel->reservation_type = $dataSender['reservation_type'];
		$reservationModel->reservation_typeid = $dataSender['reservation_caseid'];
		$reservationModel->save();

		// Se debe guardar dos veces para hacer el reservation_code más exacto y dinámico
		$preReservID = $reservationModel->reservation_id;
		$refereceSuffix = $preReservID . rand(10000000, 99999999);
		$reservationModel->reservation_code = $refereceSuffix;

		$reservationModel->save();
		$reference = 'subtur-' . $refereceSuffix;

		$apiKey = env('PAYU_APIKEY');
		$merchant = env('PAYU_MERCHANTID');
		$accountID = env('PAYU_ACCOUNTID');

		return response()->json(array(
			'control_price' => $priceControl,
			'control_curency' => $currencyControl,
			'control_reference' => $reference,
			'control_mercant' => $merchant,
			'control_accountid' => $accountID,
			'control_desc' => $nameControll,
			'control_signature' => md5("{$apiKey}~{$merchant}~{$reference}~{$priceControl}~{$currencyControl}")
		));
	}


	/**
	 * VIEW /reserva/mis-reservas
	 * Ver todas las reservas que ha hecho el usuario actual
	 * @return void
	 */
	public function view__reservationsByCurrentUser()
	{
		$sessionID = null;
		$pathComponent = "";
		if (currentUserHasRol("hotel")) {
			$sessionID = "hotel";
			$pathComponent = "reservation.hotel-reservas";
		} else if (currentUserHasRol("client")) {
			$sessionID = session()->get("user_id");
			$pathComponent = "reservation.ver-reservas";
		} else if (currentUserHasRol("admin")) {
			$sessionID = "admin";
			$pathComponent = "reservation.admin-reservas";
		}

		$reservas = $this->getAllReservationsByUser($sessionID, true);
		// dd($reservas->all());

		$cityCount = getCountOfAttributes($reservas, 'city_id');
		$statusCount = getCountOfAttributes($reservas, 'reservation_status');
		$hotelCount = getCountOfAttributes($reservas, 'hotel_id');
		$typeCount = getCountOfAttributes($reservas, 'reservation_type');


		return view($pathComponent, array(
			"reservas" => $reservas,
			"cities" => $cityCount,
			"status" => $statusCount,
			"hotels" => $hotelCount,
			"type" => $typeCount,
		));
	}

	/**
	 * VIEW /reserva/ver-reserva/
	 * Vista de una reserva en específico.
	 * @param $id
	 * @return void
	 */
	public function view__showReservationByID($id)
	{
		$reservaData = $this->getReservationByID($id, true);
		return view("reservation.ver-reserva", $reservaData);
	}

	/**
	 * Mostrar la reserva por PDF
	 * @param int $id
	 */
	public function showReservationInPDF($id)
	{
		$reservaData = $this->getReservationByID($id, true);
		$reservaObject = $reservaData["reserva"];
		$reservaOthers = $reservaData["reserva_data"];

		$pdf = new FPDFReservation();

		$pdf->SetTitle($pdf->converToUtf8('Subastas Turísticas - Reserva'));
		$pdf->SetFont("Helvetica", "", 16);
		$pdf->AddPage();

		// Inforamcion de Titular
		$pdf->SetCol(true);
		$pdf->createCell("Información de Titular", "C", null, [20, 117, 186], [255, 255, 255]);
		$pdf->SetCol(true);
		$pdf->SetCol(true);

		$pdf->generateKeyAndValue("Nombres", $reservaObject->reservation_clientname);
		$pdf->generateKeyAndValue("Teléfono", $reservaObject->reservation_clientphone);

		$pdf->SetCol(true);
		$pdf->generateKeyAndValue("Email", $reservaObject->reservation_clientmail);

		// Informacion de Reserva
		$pdf->SetFontSize(16);
		$pdf->SetCol(true);
		$pdf->SetCol(true);
		$pdf->createCell("Información de Reserva", "C", 0, [20, 117, 186], [255, 255, 255]);

		$pdf->SetCol(true);
		$pdf->SetCol(true);

		$pdf->generateKeyAndValue("Código", $reservaObject->reservation_code);
		$pdf->generateKeyAndValue("Estado", $reservaOthers["reserva_estadotext"]);
		$pdf->SetCol(true);
		// $pdf->generateKeyAndValue("Nombre", $pdf->WordWrap($reservaOthers["reserva_name"], 80));
		$pdf->generateKeyAndValue("Nombre", $reservaOthers["reserva_name"]);
		$pdf->generateKeyAndValue("Precio", formatPrice($reservaObject->reservation_price, $reservaObject->reservation_currency));
		$pdf->SetCol(true);
		$pdf->generateKeyAndValue("Tipo", $reservaOthers["reserva_type"]);
		$pdf->generateKeyAndValue("Salida", formatDate($reservaOthers["reserva_end"]));


		// Inforamcio de Hotel
		$pdf->SetCol(true);
		$pdf->SetCol(true);
		$pdf->SetFontSize(16);
		$pdf->createCell("Información de Hotel", "C", 0, [20, 117, 186], [255, 255, 255]);

		$pdf->SetCol(true);
		$pdf->SetCol(true);
		$pdf->generateKeyAndValue("Nombre", $reservaObject->hotel_name);
		$pdf->generateKeyAndValue("Dirección", $reservaObject->hotel_address);
		$pdf->SetCol(true);
		$pdf->generateKeyAndValue("Ciudad", $reservaObject->city_name);

		$pdf->SetCol(true);
		$pdf->SetCol(true);
		$pdf->SetCol(true);

		$pdf->SetFontSize(11);
		$pdf->createCell("Subastas Turísticas ® | Todos los derechos reservados " . date("Y"), "L", null, null, 54, 54, 54);

		$pdf->SetCol(true, 11);
		// $url = "localhost:8080";

		$protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === 0 ? 'https://' : 'http://';
		$url = $protocol . $_SERVER['SERVER_NAME'];
		$pdf->Cell(0, 0, $url, 0, 0, "L", false,  $url);


		$response = response($pdf->Output("S"));
		$response->header('Content-Type', 'application/pdf');
		$response->header('Content-Disposition', 'inline; filename="output.pdf"');
		$response->header('Cache-Control:', 'private, max-age=0, must-revalidate');

		return $response;
	}

	/**
	 * /reserva/paginador
	 * Crear paginador de Reservas
	 * Como parámetro $_GET puede enviar
	 * @param int $count Cantidad de filas a obtener
	 * @param int $start Número de página
	 * @param string $city_id Seleccionar filas por id de una ciudad
	 * @param string $reservation_status Seleccionar filas por estado de reserva. Esto debe ser un Número
	 * @return void
	 */
	public function ajax__getPaginator(Request $request)
	{
		$count = $request->get("count", 5);
		$start = intval($request->get("start", 0) - 1) * $count;

		$queryParams = array(
			"start" => $start,
			"limit" => $count,
		);
		$whereParams = array();

		if ($request->exists("city_id")) {
			$whereParams[] = ["city_id", "=", $request->get("city_id")];
		}
		if ($request->exists("reservation_status")) {
			$whereParams[] = ["reservation_status", "=", $request->get("reservation_status")];
		}
		if ($request->exists("hotel_id")) {
			$whereParams[] = ["hotel_id", "=", $request->get("hotel_id")];
		}
		if ($request->exists("reservation_type")) {
			$whereParams[] = ["reservation_type", "=", $request->get("reservation_type")];
		}

		if ($request->exists("orderby")) {
			$orderBy = $request->get("orderby");
			if (in_array($orderBy, ["city_name", "reservation_created", "reservation_price", "reservation_type", "hotel_name", "reservation_id", "reservation_clientname", "reservation_created"])) {
				$queryParams["orderby"] = $orderBy;
			}
		}
		if ($request->exists("order")) {
			$orderType = $request->get("order");
			if (in_array($orderType, ["ASC", "DESC"])) {
				$queryParams["order"] = $orderType;
			}
		}

		$queryParams["where"] = $whereParams;

		$sessionID = null;
		if (currentUserHasRol("hotel")) {
			$sessionID = "hotel";
		} else if (currentUserHasRol("client")) {
			$sessionID = session()->get("user_id");
		} else if (currentUserHasRol("admin")) {
			$sessionID = "admin";
		}

		$allReservations = $this->getAllReservationsByUser($sessionID, true, $queryParams);
		return response()->json($allReservations);
	}
}
