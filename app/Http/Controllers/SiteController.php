<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
	/**
	 * Mostrar el inicio
	 */
	public function indexView()
	{
		$cityController = new CityController();
		$results = $cityController->getAllCitiesWithRooms();

		$planController = new PlanController();
		$plans = $planController->getActivePlans(10);

		$hotsaleController = new HotsaleController();
		$hotsales = $hotsaleController->getActiveHotsales(true);

		$plansBanner = array();
		$cities = array();
		foreach ($plans as $plan) {
			if ($plan->plan_banner_active === 1) {
				$plansBanner[] = $plan;
			}
			$cities[$plan->city_id] = $plan->city_name;
		}


		return view("site.home", array(
			"cities" => $results['results'],
			"hotsales" => $hotsales,
			"plansbanner" => $plansBanner,
			"plans" => $plans,
			"cityplan" => $cities
		));
	}


	/**
	 * GET /hotsales
	 * Ver todos los hotsales.
	 */
	public function view__todosHotsales()
	{
		$hotsaleController = new HotsaleController();
		$hotsales = $hotsaleController->getActiveHotsales(true);
		return view('site.todos-hotsales', array(
			'hotsales' => $hotsales
		));
	}
}
