<?php

namespace App\Http\Controllers;

use App\Models\HotelModel;
use App\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HotelController extends Controller
{


	/**
	 * Obtener los datos de el hotel con algunos filtros opcionales
	 * Esta función se usará tanto para entregar datos de el hotel en la vista como en el paginador (self::ajax__getPaginator)
	 *
	 * @param array $params Array con parámetros comos start, count, order, orderby, (array) select y (array) where. Estos dos últimos debes ser estructura laravel
	 * @see https://laravel.com/docs/5.8/queries#selects
	 * @see https://laravel.com/docs/5.8/queries#where-clauses
	 * @return array Resultados con filtros asignados
	 */
	private function getDataHotel($params = array())
	{
		$orderType = 'desc';
		$orderBy = "hotel_created";
		$select = array("*");
		$where = array();

		if (array_key_exists("order", $params)) {
			if (in_array($params["order"], ["DESC", "ASC"])) {
				$orderType = $params["order"];
			}
		}
		if (array_key_exists("orderby", $params)) {
			if (in_array($params["orderby"], ["hotel_name", "city_name", "hotel_created", "hotel_active"])) {
				$orderBy = $params["orderby"];
			}
		}
		if (array_key_exists("select", $params) && !empty($params["select"])) {
			$select = $params["select"];
		}
		$hotelModel = DB::table("turi_hotels")
			->join("turi_cities", "hotel_city", "=", "city_id")
			->select($select);

		if (array_key_exists("where", $params) && !empty($params["where"])) {
			$hotelModel->where($params["where"]);
		}

		if (array_key_exists("start", $params)) {
			if (is_numeric($params["start"])) {
				$hotelModel->skip($params["start"]);
			}
		}
		if (array_key_exists("count", $params)) {
			if (is_numeric($params["count"])) {
				$hotelModel->take($params["count"]);
			}
		}

		$hotelModel->orderBy($orderBy, $orderType);
		$hotelModel->groupBy("hotel_id");
		return $hotelModel->get();
	}

	/**
	 * (ADMIN) POST /hotel/create
	 * Crear un nuevo hotel 
	 */
	public function createHotel(Request $request)
	{

		$urlRedirect = url('hotel/create');

		$messages = array(
			'hotel_email.unique' => 'El email ingresado ya esta siendo usado, intente con uno nuevo.',
		);

		$validation = Validator::make($request->all(), array(
			'hotel_name' => 'required',
			'hotel_city' => 'required',
			'hotel_email' => 'required|email:rfc|unique:turi_users,user_email',
			'hotel_rpassword' => 'required',
			'hotel_password' => 'required|same:hotel_rpassword',
			'hotel_address' => 'required',
			'hotel_active' => 'required|boolean',
		), $messages);


		if ($validation->fails()) {
			$error = $validation->errors()->first();
			return redirect()->back()->withErrors([$error]);
		}

		// CODIGO GUARDAR IMAGENES
		// Este trozo de codigo ayudara a guardar las imagenes segun el orden deseado por el cliente.
		$imagesOrder = $request->input('hotel_indexes_imgs');
		$imagesToSave = array();
		if (!empty($imagesOrder) ) {

			$imagesOrder = explode(',', trim($imagesOrder));


			$filesRequest = $request->file('hotel_images');

			foreach ($imagesOrder as $value) {
				$imagesToSave[] = saveImage($filesRequest[$value], 'hotel/');
			}
		}
		// FINAL GUARDAR IMAGENES



		$hotelName = $request->input('hotel_name');
		$hotelActive = $request->input('hotel_active');
		$userSaved = UserModel::create(array(
			'user_name' => $hotelName,
			'user_password' => Hash::make($request->input('hotel_password')),
			'user_rol' => 'hotel',
			'user_email' => $request->input('hotel_email'),
			'user_valid' => $hotelActive,
		));

		if ($userSaved) {
			$dataToSave = array(
				'hotel_user' => $userSaved->user_id,
				'hotel_name' => $hotelName,
				'hotel_city' => $request->input('hotel_city'),
				'hotel_address' => $request->input('hotel_address'),
				'hotel_active' => $hotelActive,
				'hotel_images' => $imagesToSave,
				'hotel_rooms' => $request->input('hotel_rooms'),
				'hotel_phone' => $request->input('hotel_phone'),
			);
			if ($request->has('hotel_description')) {
				$html = $request->input('hotel_description');
				$dataToSave['hotel_description'] = sanitizeHtml($html);
			}

			$hotelSaved = HotelModel::create($dataToSave);
			if ($hotelSaved) {
				return redirect($urlRedirect . '?new_reg=true');
			} else {
				return redirect()->back()->withErrors('Error al crear hotel, intente nuevamente.');
			}
		} else {
			return redirect()->back()->withErrors('Error al crear acceso de usuario, creación de hotel fallida.');
		}
	}

	/**
	 * Obtener los datos de hotel según el ID
	 *
	 * @param int $hotelid HotelID a obtener
	 */
	public function getHotelById($hotelid, $cityData = false, $userData = false)
	{
		$hotelModel = DB::table("turi_hotels");

		if ($cityData) {
			$hotelModel->join("turi_cities", "hotel_city", "=", "city_id");
		}
		if ($userData) {
			$hotelModel->join("turi_users", "hotel_user", "=", "user_id");
		}
		return $hotelModel->where("hotel_id", $hotelid)->first();
	}


	/**
	 * Obtener hoteles que tengan habitaciones.
	 *
	 * @param boolean $get Obtener instancia de modelo o resultados? Verdadero para obtener los resultados.
	 * @return mixed Instancia de modelo o resultados de hoteles.
	 */
	public function getHotelsWithRooms($get = true)
	{

		$hotels = DB::table('turi_hotels')
			->join('turi_rooms', 'turi_hotels.hotel_id', '=', 'turi_rooms.room_hotel')
			->where('turi_rooms.room_active', true)
			->groupBy('turi_hotels.hotel_id')
			->inRandomOrder()
			->where('turi_hotels.hotel_active', true);

		if ($get) {
			return $hotels->get();
		} else {
			return $hotels;
		}
	}

	/**
	 * Obtener los hoteles para inicializar subasta
	 * Se debe tener en cuenta que la subasta solo obtendran una candidad maxima de hoteles para subastas. Este metodo obtendra estos hoteles.
	 * @param int $city ID de ciudad para filtrar hotel por ciudad.
	 * @return void
	 */
	public function getInitialHotelsForAuction($city)
	{
		$results = $this->getHotelsWithRooms(false);
		return $results->where('turi_hotels.hotel_city', $city)
			->limit(5)->get('hotel_id');
	}



	/**
	 * VIEW /hotel/configuracion
	 * Obtener la configuracion actual de el hotel, como Nombre, correo de acceso, y contrasena
	 * @return View
	 */
	public function view__hotelConfiguration()
	{

		$currentRol = session()->get("user_rol");
		if ($currentRol === "admin" || $currentRol === "hotel") {
			$hotelSession = session()->get('user_hotel');
			$hotelModel = $this->getHotelById($hotelSession, true, true);
			if (!$hotelModel) {
				return response()->json(array(
					'error' => true,
					'message' => 'Error de configuracion de actual hotel'
				));
			}

			return view('hotel.configuration-hotel', array(
				'data' => $hotelModel
			));
		} else {
			return response()->json(array(
				"error" => true,
				"message" => "No allowed to this page"
			));
		}
	}

	/**
	 * POST (Hotel) /hotel/configuracion
	 * Cambiar configuracion de el hotel. Esta accion solo la puede ejecutar el hotel.
	 * @param Request $request
	 * @return void
	 */
	public function editHotelConfiguration(Request $request)
	{

		$validation = Validator::make($request->all(), array(
			'hotel_name' => 'required',
			'hotel_rooms' => 'required|numeric',
			'hotel_address' => 'required',
			'hotel_phone' => 'required|numeric',
			'hotel_password' => 'same:hotel_rpassword'
		));

		if ($validation->fails()) {
			return redirect()->back()->withErrors($validation->errors()->first());
		}

		$hotelsession = session()->get('user_hotel');
		$currentHotel = HotelModel::find($hotelsession);

		if (!$currentHotel) {
			return redirect()->back()->withErrors('Error al actualizar la configuración de hotel');
		}

		$currentHotel->hotel_name = $request->input('hotel_name');
		$currentHotel->hotel_rooms = $request->input('hotel_rooms');
		$currentHotel->hotel_address = $request->input('hotel_address');
		$currentHotel->hotel_phone = $request->input('hotel_phone');
		$hotelImages = $currentHotel->hotel_images;

		if (!$request->hasFile("hotel_images") && !empty($request->input("hotel_indexes_imgs"))) {
			$newOrderImages = array();

			$orderKeys = explode(",", $request->input("hotel_indexes_imgs"));
			foreach ($orderKeys as $orderkey => $ordval) {
				$newOrderImages[] = $hotelImages[$ordval];
			}
			$currentHotel->hotel_images = $newOrderImages;
		} else if ($request->hasFile('hotel_images')) {
			foreach ($hotelImages as $arrImags) {
				foreach ($arrImags as $img) {
					deleteImage($img, 'hotel');
				}
			}
			$newImages = $this->validateImages($request, 'hotel_images', 'hotel_indexes_imgs', 'hotel/');
			$currentHotel->hotel_images = $newImages;
		}

		if ($request->has('hotel_password')) {
			$password = $request->input('hotel_password');
			$userController = new UserController();
		}

		$currentHotel->hotel_services = $request->input('hotel_services', []);
		$currentHotel->hotel_terms = sanitizeHtml($request->input('hotel_terms', ''));
		$currentHotel->hotel_description = sanitizeHtml($request->input('hotel_description', ''));

		if ($currentHotel->save()) {
			return redirect('hotel/configuracion?new_reg=true');
		} else {
			return redirect()->back()->withErrors('Error al actualizar la configuración de hotel, intente nuevamente');
		}
	}

	/**
	 * POST (Admin|Hotel) /hotel/editar-hotel/{hotelid}
	 * Editar un Hotel
	 * @return View create-hotel.blade.php Con success o error
	 */
	public function editHotel(Request $request, $hotelid)
	{
		$hotelModel = HotelModel::find($hotelid);
		if (!$hotelid) {
			return redirect()->back()->withErrors("Error al actualizar el hotel");
		}
		$currentRol = session()->get("user_rol");
		if (!($currentRol === "hotel" || $currentRol === "admin")) {
			return redirect()->back()->withErrors("Usted no tiene permisos para actualizar este hotel");
		}


		$keysCheck = array(
			'hotel_name' => 'required',
			'hotel_city' => 'required',
			// 'hotel_email' => 'required|email:rfc|unique:turi_users,user_email',
			'hotel_rpassword' => '',
			'hotel_password' => 'same:hotel_rpassword',
			'hotel_address' => 'required',
			'hotel_phone' => 'required',
			'hotel_active' => 'required|boolean',
			"hotel_description" => "",
			"hotel_rooms" => "",
		);

		$validation = Validator::make($request->all(), $keysCheck);
		if ($validation->fails()) {
			return redirect()->back()->withErrors($validation->errors()->first());
		}

		$hotelModel->hotel_name = $request->input("hotel_name");
		$hotelModel->hotel_city = $request->input("hotel_city");
		$hotelModel->hotel_active = $request->input("hotel_active");
		$hotelModel->hotel_phone = $request->input("hotel_phone");
		$hotelModel->hotel_rooms = $request->input("hotel_rooms");
		$hotelModel->hotel_description = sanitizeHtml($request->input("hotel_description"));

		$pass = $request->input("hotel_password");
		if (!empty($pass)) {
			$userController = new UserController();
			$userController->updateUserPassword($hotelModel->hotel_user, $pass);
		}

		$hotelImages = $hotelModel->hotel_images;

		if (!$request->hasFile("hotel_images") && !empty($request->input("hotel_indexes_imgs"))) {
			$newOrderImages = array();

			$orderKeys = explode(",", $request->input("hotel_indexes_imgs"));
			foreach ($orderKeys as $orderkey => $ordval) {
				$newOrderImages[] = $hotelImages[$ordval];
			}
			$hotelModel->hotel_images = $newOrderImages;
		} else if ($request->hasFile('hotel_images')) {
			foreach ($hotelImages as $arrImags) {
				foreach ($arrImags as $img) {
					deleteImage($img, 'hotel');
				}
			}
			$newImages = $this->validateImages($request, 'hotel_images', 'hotel_indexes_imgs', 'hotel/');
			$hotelModel->hotel_images = $newImages;
		}


		if ($hotelModel->save()) {
			return redirect(url("hotel/editar-hotel/{$hotelModel->hotel_id}") . '?new_reg=true');
		} else {
			return redirect()->back()->withErrors('Error al actualizar hotel, intente nuevamente.');
		}

		printcode($request->all());
	}

	/**
	 * (ADMIN) View /hotel/editar-hotel/{$hotelid}
	 *Editar un hotel
	 * @param int $hotelid
	 * @return void
	 */
	public function view__editHotel($hotelid)
	{

		$rol = session()->get("user_rol");

		// Verificar si el hotel que se está intentando editar es parte de el hotel actual.
		if ($rol === "hotel" && session()->get("user_hotel") !== $hotelid) {
			return response()->json(array(
				"error" => true,
				"message" => "Usted no tiene permiso para editar este hotel"
			));
		}

		$hotelModel = $this->getHotelById($hotelid, true, true);
		if (!$hotelModel) {
			return response()->json(array(
				"error" => true,
				"message" => "Hotel not found"
			));
		}

		return view("hotel.create-hotel", array(
			"updating" => true,
			"hotel" => $hotelModel
		));
	}

	/**
	 * GET /hotel/ver-hoteles
	 * Ver todos los hoteles.
	 */
	public function view__showHotels()
	{

		$hotelsModel = $this->getDataHotel(array(
			"select" => ["city_id", "city_name", "hotel_active"]
		));

		$cityCount = getCountOfAttributes($hotelsModel, "city_id");
		$statusCount = getCountOfAttributes($hotelsModel, "hotel_active");

		return view("hotel.ver-hoteles", array(
			"hotels" => $hotelsModel,
			"cities" => $cityCount,
			"status" => $statusCount
		));
	}


	/**
	 * GET /hotel/get-by-city-id/{cityid}
	 * Obtener todos los hoteles por el id de una ciudad
	 */
	public function ajax__getByCityID($cityid)
	{
		$results = HotelModel::where('hotel_city', $cityid)->get();
		return response()->json($results);
	}

	/**
	 * Paginador de Hoteles
	 * @param Request $request
	 * @return JSON
	 */
	public function ajax__getPaginator(Request $request)
	{
		$count = $request->get("count", 5);
		$start = intval($request->get("start", 0) - 1) * $count;

		$where = array();
		if ($request->exists("city_id")) {
			$where[] = array("city_id", $request->get("city_id"));
		}
		if ($request->exists("hotel_active")) {
			$where[] = array("hotel_active", $request->get("hotel_active"));
		}

		$data = $this->getDataHotel(array(
			"start" => $start,
			"count" => $count,
			"order" => $request->get("order", "ASC"),
			"orderby" => $request->get("orderby", "hotel_id"),
			"where" => $where,
			"select" => array(
				"hotel_id",
				"hotel_name",
				"city_id",
				"city_name",
				"hotel_phone",
				"hotel_created",
				"hotel_active"
			)
		));

		foreach ($data as $v) {
			$v->hotel_date = formatDate($v->hotel_created);
			$v->hotel_status = $v->hotel_active === 1 ? "Activo" : "Inactivo";
		}

		return response()->json($data);
	}
}
