<?php

namespace App\Http\Controllers;

use App\Models\HotsaleModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class HotsaleController extends Controller
{



	/**
	 * Obtener los datos de un hotsale por ID específico
	 * Esta función solo retorarná los datos del HotsaleID especificado
	 *
	 * @param int $id ID de Hotsale a leer
	 * @param boolean $getCityData Obtener los datos de la ciudad de hotsale
	 * @param boolean $getHotelData Obtener los datos de el hotel de hotsale
	 * @param boolean $getRoomData Obtener los datos de la habitación del hotsale
	 * @return array Hotsale con los datos especificados.
	 */
	public function getHotsaleDataById($id, $getCityData = false, $getHotelData = false, $getRoomData = false)
	{
		$dbInstance = DB::table("turi_hotsales")
			->where("hotsale_id", "=", $id);
		// ->where("hotsale_active", true);
		if ($getCityData) {
			$dbInstance->join("turi_cities", "hotsale_city", "=", "city_id");
		}
		if ($getHotelData) {
			$dbInstance->join("turi_hotels", "hotsale_hotel", "=", "hotel_id");
		}
		if ($getRoomData) {
			$dbInstance->join("turi_rooms", "hotsale_room", "=", "room_id");
		}
		return $dbInstance->first();
	}

	public function createHotsale(Request $request)
	{

		$keysToVal = array(
			'hotsale_name' => 'required',
			'hotsale_city' => 'required|numeric',
			'hotsale_hotel' => 'required|numeric',
			'hotsale_room' => 'required|numeric',
			'hotsale_price' => 'required|numeric',
			'hotsale_currency' => array('required', Rule::in(['COP', 'USD'])),
			'hotsale_discount' => 'numeric',
			'hotsale_mindate' => 'required|date_format:d-m-Y',
			'hotsale_maxdate' => 'required|date_format:d-m-Y',
			'hotsale_active' => 'required|boolean',
			'hotsale_services' => '',
			'hotsale_description' => '',
			'hotsale_terms' => '',
		);

		$validator = Validator::make($request->all(), $keysToVal);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}

		$imagesSaved = $this->validateImages($request, "hotsale_images", "hotsale_indexes_imgs", "hotsale/");

		$dataToSave = $request->all(array_keys($keysToVal));
		$dataToSave['hotsale_services'] = json_encode($dataToSave['hotsale_services']);
		$dataToSave['hotsale_images'] = json_encode($imagesSaved);
		$dataToSave['hotsale_terms'] = sanitizeHtml($dataToSave['hotsale_terms']);
		$dataToSave['hotsale_description'] = sanitizeHtml($dataToSave['hotsale_description']);
		$dataToSave['hotsale_maxdate'] = date('Y-m-d', strtotime($dataToSave['hotsale_maxdate']));
		$dataToSave['hotsale_mindate'] = date('Y-m-d', strtotime($dataToSave['hotsale_mindate']));
		$dataToSave['hotsale_created'] = Carbon::now()->toDateTimeLocalString();
		$dataToSave['hotsale_updated'] = Carbon::now()->toDateTimeLocalString();

		$insertedHotsale = HotsaleModel::insert($dataToSave);
		if ($insertedHotsale) {
			return redirect('hotsale/create?new_reg=true');
		} else {
			return redirect()->back()->withErrors('Error al crear el nuevo hotsale. Intenten nuevamente');
		}
	}

	/**
	 * Obtener todos los hotsales activos
	 *
	 * @param boolean $getCityData Obtener datos de la ciudad de Hotsale
	 * @param boolean $getHotelData Obtener datos de el hotels de hotsale
	 * @param boolean $getRoomData Obtener datos la habitacion de hotsale
	 * @return void
	 */
	public function getActiveHotsales($getCityData = false, $getHotelData = false, $getRoomData = false)
	{
		$dbInstance =  DB::table("turi_hotsales")
			->where("turi_hotsales.hotsale_active", true);

		if ($getCityData) {
			$dbInstance->join("turi_cities", "turi_hotsales.hotsale_city", "=", "turi_cities.city_id");
		}
		if ($getHotelData) {
			$dbInstance->join("turi_hotels", "turi_hotsales.hotsale_hotel", "=", "turi_hotels.hotel_id");
		}
		if ($getRoomData) {
			$dbInstance->join("turi_rooms", "turi_hotsales.hotsale_room", "=", "turi_rooms.room_id");
		}

		return $dbInstance->inRandomOrder()->limit(10)
			->select("*")->groupBy("hotsale_id")->get();
	}

	/**
	 * VIEW /hotsale/ver-hotsale/
	 *	Mostrar los datos de un Hotsale por ID
	 * @param [type] $id
	 * @return void
	 */
	public function view__showHotsaleByID($id)
	{

		$hotsale = $this->getHotsaleDataById($id, true, true, true);
		return view("hotsale.ver-hotsale", array(
			"hotsale" => $hotsale
		));
	}

	/**
	 * Obtener todos los hotsales. Esta función permite filtrar los resultados, con algunos parámetros $params
	 *
	 * @param array $params Un array con los siguientes keys: start, count,orderby, order, y where.
	 * @return array Resultados con filtros.
	 */
	public function getAllHotsales($params = array())
	{
		$hotsalesInstance = DB::table("turi_hotsales")
			->join("turi_hotels", "hotsale_hotel", "=", "hotel_id")
			->join("turi_cities", "hotsale_city", "=", "city_id")
			->select(array(
				"hotsale_id",
				"hotsale_name",
				"hotsale_active",
				"hotsale_price",
				"hotsale_currency",
				"hotsale_created",
				"city_id",
				"city_name",
				"hotel_name",
				"hotel_id"
			));

		if (array_key_exists("where", $params)) {
			$hotsalesInstance->where($params["where"]);
		}

		if (array_key_exists("start", $params)) {
			$hotsalesInstance->skip($params["start"]);
		}

		if (array_key_exists("count", $params)) {
			$hotsalesInstance->take($params["count"]);
		}
		if (array_key_exists("orderby", $params) && array_key_exists("order", $params)) {
			$hotsalesInstance->orderBy($params["orderby"], $params["order"]);
		}

		$allHotsales = $hotsalesInstance->get();
		foreach ($allHotsales as $hotk => $hotv) {
			$hotv->hotsale_pricing = formatPrice($hotv->hotsale_price, $hotv->hotsale_currency);
			$hotv->hotsale_date = formatDate($hotv->hotsale_created);
			$hotv->hotsale_activetext = $hotv->hotsale_active == 1 ? "Activo" : "Inactivo";
		}
		return $allHotsales;
	}


	/**
	 * (ADMIN) VIEW /hotsale/editar-hotsale/{hotsaleid}
	 * Editar un hotsale
	 *
	 * @param int $hotsaleid
	 * @return View
	 */
	public function view__editHotsale($hotsaleid)
	{

		$hotsale = DB::table("turi_hotsales")
			->where("hotsale_id", $hotsaleid)
			->join("turi_hotels", "hotsale_hotel", "=", "hotel_id")
			->join("turi_cities", "hotsale_city", "=", "city_id")
			->join("turi_rooms", "hotsale_room", "=", "room_id")
			->select(array(
				"room_id",
				"room_name",
				"hotel_id",
				"hotel_name",
				"city_id",
				"city_name"
			))
			->selectRaw("turi_hotsales.*")
			->groupBy("hotsale_id")
			->first();

		return view("hotsale.create-hotsale", array(
			"hotsale" => $hotsale,
			"updating" => true
		));
	}

	public function editHotsale(Request $request, $hotsaleid)
	{

		$keysToVal = array(
			'hotsale_name' => 'required',
			'hotsale_city' => 'required|numeric',
			'hotsale_hotel' => 'required|numeric',
			'hotsale_room' => 'required|numeric',
			'hotsale_price' => 'required|numeric',
			'hotsale_currency' => array('required', Rule::in(['COP', 'USD'])),
			'hotsale_discount' => 'numeric',
			'hotsale_mindate' => 'required|date_format:d-m-Y',
			'hotsale_maxdate' => 'required|date_format:d-m-Y',
			'hotsale_active' => 'required|boolean',
			'hotsale_services' => '',
			'hotsale_description' => '',
			'hotsale_terms' => '',
		);

		$validator = Validator::make($request->all(), $keysToVal);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}

		$savedHotsale = HotsaleModel::find($hotsaleid);

		if (!$savedHotsale) {
			return response()->json(array(
				'error' => true,
				'message' => 'Hotsale not found'
			));
		}

		$hotsaleImages = $savedHotsale->hotsale_images;

		if (!$request->hasFile("hotsale_images") && !empty($request->input("hotsale_indexes_imgs"))) {
			$newOrderImages = array();

			$orderKeys = explode(",", $request->input("hotsale_indexes_imgs"));
			foreach ($orderKeys as $orderkey => $ordval) {
				$newOrderImages[] = $hotsaleImages[$ordval];
			}
			$savedHotsale->hotsale_images = $newOrderImages;
		} else if ($request->hasFile('hotsale_images')) {
			foreach ($hotsaleImages as $arrImags) {
				foreach ($arrImags as $img) {
					deleteImage($img, 'hotsale');
				}
			}
			$newImages = $this->validateImages($request, 'hotsale_images', 'hotsale_indexes_imgs', 'hotsale/');
			$savedHotsale->hotsale_images = $newImages;
		}

		$savedHotsale->hotsale_name = $request->input('hotsale_name');
		$savedHotsale->hotsale_city = $request->input('hotsale_city');
		$savedHotsale->hotsale_hotel = $request->input('hotsale_hotel');
		$savedHotsale->hotsale_room = $request->input('hotsale_room');
		$savedHotsale->hotsale_price = $request->input('hotsale_price');
		$savedHotsale->hotsale_currency = $request->input('hotsale_currency');
		$savedHotsale->hotsale_discount = $request->input('hotsale_discount');
		$savedHotsale->hotsale_mindate = 	date('Y-m-d', strtotime($request->input('hotsale_mindate')));
		$savedHotsale->hotsale_maxdate = date('Y-m-d', strtotime($request->input('hotsale_maxdate')));
		$savedHotsale->hotsale_active = $request->input('hotsale_active');
		$savedHotsale->hotsale_services = $request->input('hotsale_services');
		$savedHotsale->hotsale_description = sanitizeHtml($request->input('hotsale_description'));
		$savedHotsale->hotsale_terms = sanitizeHtml($request->input('hotsale_terms'));

		if ($savedHotsale->save()) {
			return redirect("hotsale/editar-hotsale/{$savedHotsale->hotsale_id}?new_reg=true");
		} else {
			return redirect()->back()->withErrors('Error al crear el nuevo hotsale. Intenten nuevamente');
		}

		// if ($request->hasFile("hotsale_images")) {
		// 	$imagesSaved = $this->validateImages($request, "hotsale_images", "hotsale_indexes_imgs", "hotsale/");
		// }
	}

	/**
	 * VIEW /hotsale/ver-hotsales
	 * Ver todos los hotsales.
	 */
	public function view__showAllHotsales()
	{

		$hotsales = $this->getAllHotsales();
		$cities = getCountOfAttributes($hotsales, "city_id");
		$hotels = getCountOfAttributes($hotsales, "hotel_id");
		$status = getCountOfAttributes($hotsales, "hotsale_active");
		return view("hotsale.ver-hotsales", array(
			"hotsales" => $hotsales,
			"cities" => $cities,
			"hotels" => $hotels,
			"status" => $status
		));
	}


	/**
	 * AJAX /hotsale/paginador
	 * Obtener paginador de Hotslaes
	 *
	 * @param Request $request
	 * @return void
	 */
	public function ajax__getPaginator(Request $request)
	{
		$count = $request->get("count", 5);
		$start = intval($request->get("start", 0) - 1) * $count;
		$orderType = 'desc';
		$orderBy = "hotsale_created";

		if ($request->exists("orderby")) {

			if (in_array($request->get("orderby"), ["hotsale_name", 'hotsale_price', 'hotel_name', 'city_name', "hotsale_active", "hotsale_created"])) {
				$orderBy = $request->get("orderby");
			}
			if ($request->exists("order")) {
				if (in_array($request->get("order"), ["ASC", "DESC"])) {
					$orderType = $request->get("order");
				}
			}
		}
		$where = array();
		if ($request->exists("city_id")) {
			$where[] = array("city_id", $request->get("city_id"));
		}
		if ($request->exists("hotel_id")) {
			$where[] = array("hotel_id", $request->get("hotel_id"));
		}
		if ($request->exists("hotsale_active")) {
			$where[] = array("hotsale_active", $request->get("hotsale_active"));
		}

		$resultados = $this->getAllHotsales(array(
			"count" => $count,
			"start" => $start,
			"orderby" => $orderBy,
			"order" => $orderType,
			"where" => $where
		));

		return response()->json($resultados);
	}
}
