<?php

namespace App\Http\Controllers;

use App\Models\CityModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
	/**
	 * GET city/get-by-name/{name}
	 * Obtener todas diez ciudades por nombre
	 */
	public function ajax__getCityByName($name)
	{
		$coincides = CityModel::where('city_name', 'like', "%{$name}%")->take(5)->get();
		return response()->json($coincides);
	}


	/**
	 * /city/get-with-some-room
	 * Obtener todas las ciudades que tengan almenos una habitación.
	 */
	public function getAllCitiesWithRooms()
	{
		$resultsDB = DB::table("turi_cities")
			->join("turi_hotels", "turi_cities.city_id", "=", "turi_hotels.hotel_city")
			->join("turi_rooms", "turi_hotels.hotel_id", "=", "turi_rooms.room_hotel")
			->select('turi_cities.*')
			->groupBy('turi_cities.city_name');
		$sql = $resultsDB->toSql();
		$results = $resultsDB->get();
		return array(
			'results' => $results,
			'sql' => $sql
		);
	}

	public function getCityById($id){
		return CityModel::find($id);
	}
}
