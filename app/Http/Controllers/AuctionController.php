<?php

namespace App\Http\Controllers;

use App\Models\AuctionModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuctionController extends Controller
{
	/**
	 * Crear una nueva auction
	 * POST /auction/create
	 *
	 * @param Request $request
	 * @return void
	 */
	public function createAuction(Request $request)
	{

		$auctionKeys = array(
			"auction_city" 			=> "required|numeric",
			"auction_days" 			=> "required|numeric",
			"auction_minval" 		=> "required|numeric",
			"auction_maxval" 		=> "required|numeric",
			"auction_adults" 		=> "required|numeric|min:1",
			"auction_useremail" => "required|email:rfc",
			"auction_username" 	=> "required",
			"auction_childrens" => "numeric|min:0",
			"auction_infants" 	=> "numeric|min:0",
			"auction_init" 			=> "required|date_format:d-m-Y",
			"auction_end" 			=> "required|date_format:d-m-Y",
		);

		$validate = Validator::make($request->all(), $auctionKeys);
		if ($validate->fails()) {
			return response()->json(array(
				"error" => true,
				"message" => $validate->errors()->first()
			));
		}

		$hotelController = new HotelController();

		// Hoteles iniciales para crear subastas
		$initialHotels = $hotelController->getInitialHotelsForAuction($request->input('auction_city'));
		$initalHotelsIDs = array();
		foreach ($initialHotels as $hotel) {
			$initalHotelsIDs[] = $hotel->hotel_id;
		}

		$dataSave = $request->all(array_keys($auctionKeys));
		$dataSave["auction_status"] = 1;
		$dataSave["auction_hotels"] = $initalHotelsIDs;
		$dataSave["auction_init"] = date('Y-m-d', strtotime($dataSave['auction_init']));
		$dataSave["auction_end"] = date('Y-m-d', strtotime($dataSave['auction_end']));
		$dataSave["auction_childrens"] = $dataSave['auction_childrens'];
		$dataSave["auction_infants"] = $dataSave['auction_infants'];
		if (isAuth()) {
			$dataSave["auction_userid"] = getCurrentUserId();
		}

		// Guardar la subasta ( table:turi_auctions)
		$auctionInsert = AuctionModel::create($dataSave);

		if ($auctionInsert) {

			/**
			 * Se Comenzara a filtar los datos necesarios para crear los nuevos auctionsRows
			 */
			$auctionInsert["auction_hotelscount"] = count($initalHotelsIDs);
			$cityControll = new CityController();
			$cityInserted = $cityControll->getCityById($auctionInsert->auction_city);

			$detailsResults = array('auction' => $auctionInsert, 'city' => $cityInserted);

			/**
			 * Los datos seran renderizados con algunos input:hidden (Ver en auction-details) los cuales seran usados para obtener los nuevos auctionRows ( Ofertas de subastas )
			 */
			return response()->json(array(
				"error" => false,
				'details_results' => view('components.auction-details', $detailsResults)->render(),
				"inserted" => $auctionInsert
			));
		} else {
			return response()->json(array(
				"error" => true,
				"message" => "No se ha podido crear la subasta, intente nuevamente"
			));
		}
	}

	public function getAuctionById($id)
	{
		return AuctionModel::find($id);
	}

	/**
	 * Finalizar una subasta por iD
	 * @param int $auctionid ID de subasta a finalizar
	 */
	public function alterAuction(Request $request, $auctionid)
	{


		$newStatus = $request->get("status", 3);

		$auctionToFin = AuctionModel::find($auctionid);
		if (!$auctionToFin) {
			return response()->json(array(
				"error" => true,
				"message" => "No se ha encontrado ninguna subasta con el ID"
			));
		}

		if ($auctionToFin->auction_userid) {
			if ($request->session()->get("user_id") !== $auctionToFin->auction_userid) {
				return response()->json(array(
					"error" => true,
					"message" => "No allowe to change auction"
				));
			}
		}

		if ($newStatus > 5 || $newStatus < 1) {
			return response()->json(array(
				"error" => true,
				"message" => "Status type no allowed"
			));
		}

		$auctionToFin->auction_status = $newStatus;
		$auctionToFin->save();
	}

	/**
	 * Obtener todas las subastas por un Rol el especifico, puede ser hotel, client o admin
	 *
	 * @param string $rol Tipo de rol que se esta buscando
	 * @param array $params Datos con algunos parametros adicionales, puede ser "start", "order","orderby", "limit" y "where". Debe tener en cuenta que el where, debe ser un where syntax Laravel
	 * @return array
	 */
	public function getAllAuctionsByRol($rol, $params = array())
	{
		$auctionRows = DB::table('turi_auctionrows')
			->select(array(
				'auctionrow_id',
				'auctionrow_auction',
				'auctionrow_price',
				'auctionrow_hotel',
			))
			->selectRaw('MAX(auctionrow_updated) AS auctionrow_updated')
			->groupBy('auctionrow_id')
			->orderBy('auctionrow_updated');

		$auctions = DB::table('turi_auctions')
			->select(array(
				'auctionrow_price',
				'city_id',
				'auctionrow_updated',
				'auctionrow_id',
				'auction_id',
				'city_name',
				'auction_maxval',
				'auction_status',
				'auction_days',
				'hotel_id',
				'hotel_name',
				'auction_status',
			))
			->selectRaw('COUNT(city_name) as city_namemoda, COUNT(hotel_name) as hotel_namemoda, COUNT(auction_status) as auction_statusmoda')
			->joinSub($auctionRows, 'auction_rows', 'auction_id', '=', 'auctionrow_auction')
			->join('turi_cities', 'auction_city', '=', 'city_id')
			->join('turi_hotels', 'auctionrow_hotel', '=', 'hotel_id')
			->groupBy('auction_id');

		if (currentUserHasRol($rol)) {
			if ($rol == 'hotel') {
				$auctions->where('hotel_id', session()->get('user_hotel'));
			} else if ($rol === 'client') {
				$auctions->where('auction_userid', session()->get('user_id'));
			}
			// Si es hotel, solo obtendra todas las subastas. No se agregara un where adicional.
		}

		if (array_key_exists('where', $params)) {
			$auctions->where($params['where']);
		}
		$sq = $auctions->toSql();
		if (array_key_exists('orderby', $params)) {

			// Evitar SQL Injection
			$orderbySQL = in_array($params['orderby'], array("auctionrow_price", "auctionrow_id", "city_name", "auction_maxval", "auction_days")) ? $params['orderby'] : "auctionrow_updated";

			$orderSQL = 'DESC';

			if (array_key_exists('order', $params)) {
				// Evitar SQL Injection
				$orderSQL = in_array($params['order'], ["ASC", "DESC"]) ? $params['order'] : "DESC";
			}
			$auctions->orderBy($orderbySQL, $orderSQL);
		}

		if (array_key_exists('start', $params)) {
			$auctions->skip($params['start']);
			if (array_key_exists('limit', $params)) {
				$auctions->take($params['limit']);
			}
		}

		$auctions = $auctions->get();
		return $auctions;
	}

	/**
	 * VIEW /subasta/mis-subastas
	 * Mostrar las ofertas de el usario actual
	 *
	 * @return view
	 */
	public function view__showMyAuctions()
	{
		$auctions = $this->getAllAuctionsByRol(getCurrentRol());

		$citiesCount = array();
		$hotelsCount = array();
		$statusCount = array();
		foreach ($auctions as $key => $value) {
			$citiesCount[$value->city_id] = array(
				"name" => $value->city_name,
				"count" => $value->city_namemoda
			);
			$hotelsCount[$value->hotel_id] = array(
				"name" => $value->hotel_name,
				"count" => $value->hotel_namemoda
			);
			$statusCount[$value->auction_status] = array(
				"name" => getStatusOfAuction($value->auction_status),
				"count" => $value->auction_statusmoda
			);
		}

		return view("auction.ver-auctions", array(
			"auctions" => $auctions,
			"hotels" => $hotelsCount,
			"status" => $statusCount,
			"cities" => $citiesCount
		));
	}

	/**
	 * AJAX /acution/paginador
	 * Obtener resultados una cantidad de resultados ( $count) desde una página es específico ($start )
	 *
	 * @param Request $request
	 * @param int $start Página a obtener
	 * @param int $count Cantidad de resultados a obtener.
	 * @return void
	 */
	public function ajax__getPaginator(Request $request)
	{

		$count = $request->get("count", 5);
		$start = intval($request->get("start", 0) - 1) * $count;

		$params = array(
			'start' => $start,
			'limit' => $count
		);

		$whereParams = array();
		if ($request->exists("city_id")) {
			$whereParams[] = ["city_id", $request->get("city_id")];
		}
		if ($request->exists("auctionrow_hotel")) {
			$whereParams[] = ["auctionrow_hotel", $request->get("auctionrow_hotel")];
		}
		if ($request->exists("auction_status")) {
			$whereParams[] = ["auction_status", $request->get("auction_status")];
		}
		$params['where'] = $whereParams;

		if ($request->exists("order")) {
			$params['order'] = $request->get("order");
		}
		if ($request->exists("orderby")) {
			$params['orderby']  = $orderbySQL = $request->get("orderby");
		}
		$pagActions = $this->getAllAuctionsByRol(getCurrentRol(), $params);

		foreach ($pagActions as $auction) {
			$auction->auction_date = formatDate($auction->auctionrow_updated);
			$auction->auction_price = formatPrice($auction->auctionrow_price);
			$auction->auction_maxprice = formatPrice($auction->auction_maxval);
			$auction->auction_statustext = getStatusOfAuction($auction->auction_status);
		}
		return response()->json($pagActions);
	}
}
