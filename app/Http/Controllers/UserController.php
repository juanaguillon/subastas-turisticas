<?php

namespace App\Http\Controllers;

use App\Models\HotelModel;
use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class UserController extends Controller
{

	/**
	 * VIEW /user/register
	 * Ingresar a el formulario para crear un usuario.
	 *
	 * @return void
	 */
	public function createUserView()
	{
		return view('user.create-user');
	}

	/**
	 * POST /user/register
	 * Crear un nuevo usuario.
	 */
	public function createUser(Request $request)
	{
		$messages = [
			'email_register.required' => 'Debe ingresar un email válido.',
			'name_register.required' => 'El nombre es obligatorio.',
			'email_register.unique' => 'El email ingresado ya está registrado, intente con uno diferente.',
			'password_register.same' => 'Las contraseñas no coinciden.',
		];
		$validator = Validator::make($request->all(), array(
			'email_register' => 'required|unique:turi_users,user_email',
			'name_register' => 'required',
			'password_register' => 'required|same:rpassword_register'
		), $messages);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(array(
				'success' => false,
				'message' => $error
			));
		}


		$dataInsert = array(
			'user_name' => $request->input('name_register'),
			'user_rol' => 'client',
			'user_email' => $request->input('email_register'),
			'user_password' => Hash::make($request->input('password_register')),
			'user_valid' => true,
			'user_created' => Carbon::now()->toDateTimeLocalString(),
			'user_updated' => Carbon::now()->toDateTimeLocalString(),
		);
		$isInserted = UserModel::create($dataInsert);

		return response()->json(array(
			'success' => true,
			'message' => 'Usuario creado correctamente',
			'id' => $isInserted->id
		));

		if ($isInserted) {
			return response()->json(array(
				'success' => true,
				'message' => 'Usuario creado correctamente',
				'id' => $isInserted
			));
		} else {
			return response()->json(array(
				'success' => false,
				'message' => 'Error al registrar un nuevo usuario, intente nuevamente.'
			));
		}
	}

	/**
	 * Ingresar usuario
	 *	POST /user/login
	 */
	public function loginUser(Request $request, $api = false)
	{
		$email = $request->input('email_login');
		$password = $request->input('password_login');

		$user = UserModel::where('user_email', $email)->first();

		if ($user) {
			if (Hash::check($password, $user->user_password)) {
				$response = array(
					'success' => true,
				);
				if (!$api) {
					$request->session()->put('user_id', $user->user_id);
					$request->session()->put('user_email', $user->user_email);
					$request->session()->put('user_rol', $user->user_rol);
					$request->session()->put('user_name', $user->user_name);

					if ($user->user_rol === "admin") {
						$response["redirect"] = url("/hotel/create");
					} else if ($user->user_rol === "hotel") {
						$hotelModel = HotelModel::where('hotel_user', $user->user_id)->first();
						$request->session()->put('user_hotel', $hotelModel->hotel_id);
						$response["redirect"] = url("/hotel/configuracion");
					} else {
						$response["redirect"] = url("/");
					}
				} else {
					$customClaims = ['foo' => 'bar', 'baz' => 'bob', 'sub' => 'user_rol'];
					$response['token'] = JWTAuth::fromUser($user, $customClaims);
				}

				return response()->json($response);
			} else {
				return response()->json(array(
					'success' => false,
					'message' => 'Usuario o contraseña inválida'
				));
			}
		} else {
			return response()->json(array(
				'success' => false,
				'message' => 'Usuario o contraseña inválida'
			));
		}
	}

	/**
	 * GET /user/configuracion Vista para ver la configuracon actal de usuario
	 * @return View Vista de configuracion de usaurio
	 */
	public function view__editUserConfig()
	{
		$currentuser = UserModel::find(getCurrentUserId());
		if (!$currentuser) {
			return response()->json(array(
				'error' => true,
				'message' => 'User not found'
			));
		}

		return view('user.configuracion-usuario', array(
			'user' => $currentuser
		));
	}

	/**
	 * POST /user/configuracion Guardar cambios de la configuracion de usuario
	 *
	 * @param Request $request
	 * @return View
	 */
	public function editUserConfig(Request $request)
	{
		$currentuser = UserModel::find(getCurrentUserId());
		if (!$currentuser) {
			return redirect()->back()->withErrors('Error al actualizar el usuario');
		}

		$validator = Validator::make($request->all(), array(
			'user_name' => 'required',
			'user_email' => 'required|email',
			'user_password' => 'same:user_rpassword'
		));
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors()->first());
		}

		$currentuser->user_name = $request->input('user_name');
		$currentuser->user_email = $request->input('user_email');

		if ($request->has('user_password')) {
			$currentuser->user_password = Hash::make($request->input('user_password'));
		}

		if (!$currentuser->save()) {
			return redirect()->back()->withErrors("Error al actualizar el usuario, intente nuevamente");
		} else {
			return redirect('user/configuracion?new_reg=true');
		}
	}

	/**
	 * Cerrar sesion
	 * /user/logout
	 *
	 */
	function logoutUser()
	{
		session()->flush();
		return redirect('/');
	}

	public function updateUserPassword($userid, $password)
	{

		if (currentUserHasRol("admin") || $userid === getCurrentUserId()) {
			$user = UserModel::find($userid);

			if (!$user) {
				return false;
			}

			$user->user_password = Hash::make($password);
			return $user->save();
		} else {
			return false;
		}
	}

	/**
	 * POST /user/crear-suscripcion
	 * Crear un nuevo suscriptor
	 * @return void
	 */
	public function createSubscriber(Request $request)
	{

		$keys = array(
			"user_email" => 'required|string|email',
			"user_name" => 'required|string'
		);

		$validator = Validator::make($request->all(), $keys);
		try {
			if ($validator->fails()) {
				throw new \Error($validator->errors()->first());
			}

			$dataInsert = array_merge(
				$request->all(array_keys($keys)),
				array(
					"user_rol" => 'subscriber',
					"user_valid" => true,
					'user_created' => Carbon::now()->toDateTimeLocalString(),
					'user_updated' => Carbon::now()->toDateTimeLocalString(),
				)
			);

			$user = UserModel::create($dataInsert);

			if ($user) {
				return response()->json(array(
					'error' => false,
					"message" => "Se ha creado correctamente la suscripción"
				));
			} else {
				throw new \Error("Error al crear la suscripción");
			}

		} catch (\Throwable $th) {
			return response()->json(array(
				"error" => true,
				"message" => $th->getMessage()
			));
		}
	}
}
