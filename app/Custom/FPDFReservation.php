<?php

namespace App\Custom;

use FPDF;

class FPDFReservation extends FPDF
{

  private $col = 0; // Current column
  private $currentHeight = 25; // Current Y AXIS Column
  private $ySpaceCol = 7; // Y AXIS Space Column
  private $numberCols = 1;


  public function converToUtf8($string)
  {
    return iconv('UTF-8', 'windows-1252', $string);
  }

  /**
   * Crear una celda sin parámetros de height, width, etc
   *
   * @param string $text Texto a mostrar
   * @param string $align "C"|"R" |"L" or empty
   * @param array $background 3 Valores para ser r(0) g(1) b(2)
   * @param array $color 3 Valores para ser r(0) g(1) b(2)
   */
  public function createCell($text, $align, $w = 0, $background = null, $color = null)
  {
    if ($background) {
      $filling = true;
      $this->SetFillColor($background[0], $background[1], $background[2]);
    } else {
      $filling = false;
    }
    if ($color) {
      $this->SetTextColor($color[0], $color[1], $color[2]);
    }
    $this->Cell($w, 9, $this->converToUtf8($text), 0, 0, $align, $filling);
  }

  public function generateKeyAndValue($key, $value)
  {
    $this->SetFontSize(11);
    $this->createCell($key, "L", 18, null, [97, 97, 97]);
    $this->createCell($value, "L", 74,  null, [0, 0, 0]);
  }

  function Header()
  {
    $this->Image( base_path("public/images/logo-subastas-turisticas.png") , 10, 10, 50);
    $this->SetFont("Helvetica", "B", 18);
    $this->Cell(0, 20, "Reserva Actual", 0, 0, "R");
  }

  function WordWrap(&$text, $maxwidth)
  {
    $text = trim($text);
    if ($text === '')
      return 0;
    $space = $this->GetStringWidth(' ');
    $lines = explode("\n", $text);
    $text = '';
    $count = 0;

    foreach ($lines as $line) {
      $words = preg_split('/ +/', $line);
      $width = 0;

      foreach ($words as $word) {
        $wordwidth = $this->GetStringWidth($word);
        if ($wordwidth > $maxwidth) {
          // Word is too long, we cut it
          for ($i = 0; $i < strlen($word); $i++) {
            $wordwidth = $this->GetStringWidth(substr($word, $i, 1));
            if ($width + $wordwidth <= $maxwidth) {
              $width += $wordwidth;
              $text .= substr($word, $i, 1);
            } else {
              $width = $wordwidth;
              $text = rtrim($text) . "\n" . substr($word, $i, 1);
              $count++;
            }
          }
        } elseif ($width + $wordwidth <= $maxwidth) {
          $width += $wordwidth + $space;
          $text .= $word . ' ';
        } else {
          $width = $wordwidth + $space;
          $text = rtrim($text) . "\n" . $word . ' ';
          $count++;
        }
      }
      $text = rtrim($text) . "\n";
      $count++;
    }
    $text = rtrim($text);
    return $count;
  }


  /**
   * Agregar una nueva columna
   *
   * @param boolean $newCols Romper columas e iniciar con un linea de columnas nueva
   * @return void
   */
  function SetCol($newCols = false, $margin = null)
  {
    if ($newCols) {
      $this->col = 0;
      if (!$margin) {
        $this->currentHeight += $this->ySpaceCol;
      } else {
        $this->currentHeight += $margin;
      }
    }

    $this->col += 1;

    if ($this->col === $this->numberCols) {
      $x = 10;

      $this->SetTopMargin($this->currentHeight);
      $this->SetY($this->currentHeight);
    } else {
      $x =  200 % $this->numberCols;
      $this->currentHeight += $this->ySpaceCol;
    }

    $this->SetLeftMargin($x);
    $this->SetX($x);
  }



  function ChapterTitle($num, $label)
  {
    // Title
    $this->SetFont('Arial', '', 12);
    $this->SetFillColor(200, 220, 255);
    $this->Cell(0, 6, "Chapter $num : $label", 0, 1, 'L', true);
    $this->Ln(4);
    // Save ordinate
    $this->y0 = $this->GetY();
  }
}
