<?php

use Illuminate\Support\Collection;
use Intervention\Image\ImageManager as Image;

function isAuth()
{
  return session()->has('user_id');
}

function getCurrentUserName()
{
  return session()->get('user_name');
}

function getCurrentRol()
{
  return session()->get('user_rol');
}

/**
 * Obtener la session ID de el actual usuario
 *
 * @return void
 */
function getCurrentUserId()
{
  return session()->get("user_id");
}

function printcode($code)
{
  echo "<pre>" . htmlspecialchars(print_r($code, true)) . "</pre>";
}

/**
 * Eliminar etiquetas script para evitar XXS en el sitio
 *
 * @param [type] $html
 * @return void
 */
function sanitizeHtml($html)
{
  if ($html && $html !== "") {
    return preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
  } else {
    return $html;
  }
}

/**
 * Verificar si el actual usuario un rol en especifico
 *
 * @param [type] $rol
 * @return void
 */
function currentUserHasRol($rol)
{

  if (!isAuth()) {
    return false;
  }
  return session()->get("user_rol") === $rol;
}


/**
 * Dar formato correcto a los precios.
 *
 * @param int $price
 * @param string $currency
 * @return string Formato correcto
 */
function formatPrice($price, $currency = "COP")
{
  $end = "";
  if ($currency !== "") {
    $end = " " . $currency;
  }
  return "$" . number_format($price, 0, ".", ".") . $end;
}

function formatTime($time)
{
  return gmdate("i's''", intval($time));
}

/**
 * Obtener el numero descuento de un valor normal y el descuento.
 *
 * @param string|int $normal Precio numerico de el precio normal
 * @param string|int $discount Porcentaje numerico de el descuento a aplicar
 * @return void
 */
function getDiscountPrice($normal, $discount)
{
  $normal = intval($normal);
  $discount = intval($discount);

  return $normal - ($discount / 100) * $normal;
}

/**
 * Guardar una imagen
 *
 * @param object $imageReq Debe ser un Request/file ( $request->file() ) para ser procesado.
 * @param string $path Path de relativo public/storage EJ: Si $path es hotel/, la imagen se guardará en ${ROOT}/public/storage/hotel/${imagenToSave}. Tenga en cuenta la estructura de el string, terminando en Barra Diagonal (/)
 * @return array
 */
function saveImage($imageReq, $path = '')
{
  $imageINT = new Image();
  $image = $imageINT->make($imageReq->getRealPath());
  $widthImage = $image->width();
  $image->backup();

  $basepath = base_path() . '/public/storage/' . $path;

  if (!file_exists($basepath)) {
    mkdir($basepath);
  }

  $fileTime = time() . rand(1000000, 9999999);

  $fileName = $fileTime . '.jpg';

  $image->save($basepath . $fileName, 95, 'jpg');
  $image->reset();

  $fileName765 = $fileTime . '_765.jpg';
  $fileName350 = $fileTime . '_350.jpg';
  $fileNamexl = $fileTime . '_xl.jpg';

  if ($widthImage > 1200) {
    $image->resize(1200, null, function ($constraint) {
      $constraint->aspectRatio();
    });
    $image->save($basepath . $fileNamexl, 82, 'jpg');
    $image->reset();
  } else {
    $fileNamexl = $fileName;
  }


  if ($widthImage > 765) {
    $image->resize(765, null, function ($constraint) {
      $constraint->aspectRatio();
    });
    $image->save($basepath . $fileName765, 75, 'jpg');
    $image->reset();
  } else {
    $fileName765 = $fileName;
  }


  if ($widthImage > 350) {
    $image->fit(350);
    $image->save($basepath . $fileName350, 69, 'jpg');
  } else {
    $fileName350 = $fileName;
  }


  return array(
    'original' => $fileName,
    "xl" => $fileNamexl,
    '765' => $fileName765,
    '350' => $fileName350,
    'alt' => $imageReq->getClientOriginalName()
  );
}

function deleteImage($name, $path = "")
{
  if ($path !== "") {
    $path .= "/";
  }
  $fileDelete = base_path() . "/public/storage/" . $path . $name;

  if (file_exists($fileDelete)) {
    unlink($fileDelete);
  }
}


/**
 * Mostrar correctamente un nombre de Usuario
 * @return string
 */
function formatName($nombre)
{
  return ucwords(mb_strtolower($nombre, "utf-8"));
}

/**
 * Mostrar una dia, mes y año de una fecha.
 * @param string Fecha a ser mostrada
 * @return void
 */
function formatDate($dateString)
{
  return date("d-m-Y", strtotime($dateString));
}

/**
 * Ibtener el estado en texto de una subasta (Auction)
 * 
 * @param int $number Estado de auction , puede ser 1,2,3,4,5
 * @return void
 */
function getStatusOfAuction($number)
{
  switch ($number) {
    case 1:
      $statusText = "Inicializado";
      break;
    case 2:
      $statusText = "En Proceso";
      break;
    case 3:
      $statusText = "Finalizada";
      break;
    case 4:
      $statusText = "Aceptada";
      break;
    case 5:
      $statusText = "Rechazada";
      break;
    default:
      $statusText = "Sin Estado";
      break;
  }
  return $statusText;
}


/**
 * Contar la cantidad de items que aparece en una coleccion.
 *
 * @param Collection $collection
 * @param string $keyToCount Item key a contar
 * @return void
 */
function getCountOfAttributes(Collection $collection, $keyToCount)
{
  $counter = array();
  $collectionAll = $collection->all();

  if (count($collectionAll) < 1) return $counter;

  if (get_class($collectionAll[0]) === 'App\Models\RoomModel') {
    // dd($collectionAll);
    foreach ($collectionAll as $val) {
      // printcode($val);
      $hash = $val->getAttributes()[$keyToCount];
      if (isset($counter[$hash])) {
        $counter[$hash]['count'] += $counter[$hash]['count'];
      } else {
        $counter[$hash] = $val;
        $counter[$hash]['count'] = 1;
      }
    }
  } else if (get_class($collectionAll[0]) === 'stdClass') {
    foreach (array_map("get_object_vars", $collectionAll) as $val) {
      // printcode($val);
      $hash = $val[$keyToCount];
      if (isset($counter[$hash])) {
        $counter[$hash]['count'] += $counter[$hash]['count'];
      } else {
        $counter[$hash] = $val;
        $counter[$hash]['count'] = 1;
      }
    }
  }
  return $counter;
}
