<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuriPlanesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turi_planes', function (Blueprint $table) {
            $table->bigIncrements('plan_id');
            
            $table->string('plan_name');
            $table->integer('plan_city');
            $table->integer('plan_hotel');
            $table->integer('plan_room');
            $table->integer('plan_rooms_quantity');
            $table->integer('plan_price');
            $table->string('plan_currency', 3);
            $table->date('plan_mindate');
            $table->date('plan_maxdate');
            $table->boolean('plan_active');
            $table->boolean('plan_banner_active');
            $table->text('plan_images')->nullable();
            $table->text('plan_special_service')->nullable();
            $table->text('plan_services')->nullable();
            $table->text('plan_promotion')->nullable();
            $table->text('plan_description')->nullable();
            $table->text('plan_terms')->nullable();

            $table->timestamp('plan_created')->nullable();
            $table->timestamp('plan_updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turi_planes');
    }
}
