<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuriReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('turi_reservations', function (Blueprint $table) {
			$table->bigIncrements('reservation_id');

			$table->string("reservation_code");
			$table->string("reservation_clientname");
			$table->string("reservation_clientmail");
			$table->string("reservation_clientphone");
			$table->string("reservation_clientcity");
			$table->string("reservation_clientadress");

			$table->integer("reservation_price");
			$table->string("reservation_currency");

			$table->string("reservation_clientdoctype");
			$table->string("reservation_clientdocnumber");

			$table->integer("reservation_status")->comment('1:En proceso, 2: Pagada, 3:Cancelada');

			$table->integer('reservation_hotel')->nullable();
			$table->integer('reservation_user')->nullable();

			$table->string("reservation_type");
			$table->integer("reservation_typeid");

			$table->timestamp("reservation_created")->nullable();
			$table->timestamp("reservation_updated")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('turi_reservations');
	}
}
