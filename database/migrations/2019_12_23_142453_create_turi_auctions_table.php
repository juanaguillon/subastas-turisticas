<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuriAuctionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('turi_auctions', function (Blueprint $table) {
			$table->bigIncrements('auction_id');


			$table->integer("auction_city");
			$table->text("auction_hotels");
			$table->string("auction_code")->nullable();
			$table->integer("auction_days");
			$table->integer("auction_adults");
			$table->integer("auction_childrens")->nullable();
			$table->integer("auction_infants")->nullable();
			$table->integer("auction_minval");
			$table->integer("auction_maxval");

			$table->date('auction_init')->nullable();
			$table->date('auction_end')->nullable();
			
			$table->smallInteger("auction_status")->comment("1: Inicializado, 2: En Proceso, 3: Finalizada, 4: Aceptada, 5: Rechazada");

			$table->string("auction_username")->nullable();
			$table->string("auction_useremail")->nullable();
			$table->integer("auction_userid")->nullable();

			$table->timestamp("auction_created")->nullable();
			$table->timestamp("auction_updated")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('turi_auctions');
	}
}
