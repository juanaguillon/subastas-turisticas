<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuriAuctionrowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turi_auctionrows', function (Blueprint $table) {
            $table->bigIncrements('auctionrow_id');

            $table->integer('auctionrow_auction');
            $table->integer('auctionrow_number');
            $table->integer('auctionrow_price');
            $table->integer('auctionrow_hotel');
            $table->string('auctionrow_time');
            $table->integer('auctionrow_room');
            $table->integer('auctionrow_user')->nullable();
            $table->boolean('auctionrow_active');

            $table->timestamp('auctionrow_created')->nullable();
            $table->timestamp('auctionrow_updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turi_auctionrows');
    }
}
