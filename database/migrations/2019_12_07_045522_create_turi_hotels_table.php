<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuriHotelsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('turi_hotels', function (Blueprint $table) {
			$table->bigIncrements('hotel_id');
			$table->string('hotel_user');
			$table->string('hotel_name');
			$table->string('hotel_city');
			$table->text('hotel_images')->nullable();
			$table->text('hotel_description')->nullable();
			$table->text('hotel_terms')->nullable();
			$table->string('hotel_address')->nullable();
			$table->string('hotel_phone')->nullable();
			$table->text('hotel_services')->nullable();
			$table->integer('hotel_rooms')->nullable();
			$table->integer('hotel_rating')->nullable();
			$table->boolean('hotel_active');
			$table->timestamp('hotel_created')->nullable();
			$table->timestamp('hotel_updated')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('turi_hotels');
	}
}
