<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuriRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turi_rooms', function (Blueprint $table) {
            $table->bigIncrements('room_id');
            $table->integer('room_hotel');
            $table->string('room_name');
            $table->text('room_description')->nullable();
            $table->text('room_images')->nullable();
            $table->text('room_terms')->nullable();
            $table->text('room_services')->nullable();
            $table->text('room_equipament')->nullable();
            $table->text('room_gifts')->nullable();
            $table->integer('room_meters');
            $table->integer('room_rooms');
            $table->integer('room_adults');
            $table->integer('room_childs')->nullable();
            $table->integer('room_babys')->nullable();
            $table->integer('room_minprice');
            $table->integer('room_maxprice');
            $table->boolean('room_active');
            $table->dateTime('room_created')->nullable();
            $table->dateTime('room_updated')->nullable();
        });
    }

    /**
     * Reverse rhe migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turi_rooms');
    }
}
