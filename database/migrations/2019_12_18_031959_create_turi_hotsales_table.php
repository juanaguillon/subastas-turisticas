<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuriHotsalesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('turi_hotsales', function (Blueprint $table) {
			$table->bigIncrements('hotsale_id');
			$table->string('hotsale_name');
			$table->integer('hotsale_city');
			$table->integer('hotsale_hotel');
			$table->integer('hotsale_room');
			$table->integer('hotsale_price');
			$table->integer('hotsale_discount')->nullable();
			$table->string('hotsale_currency');
			$table->date('hotsale_mindate');
			$table->date('hotsale_maxdate');
			$table->boolean('hotsale_active');
			$table->text('hotsale_images')->nullable();
			$table->text('hotsale_services')->nullable();
			$table->text('hotsale_description')->nullable();
			$table->text('hotsale_terms')->nullable();
			$table->dateTime('hotsale_created')->nullable();
			$table->dateTime('hotsale_updated')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('turi_hotsales');
	}
}
