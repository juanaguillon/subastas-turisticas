<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('turi_users', function (Blueprint $table) {
			$table->bigIncrements('user_id');
			$table->string('user_name');
			$table->enum('user_rol', array(
				'admin',
				'hotel',
				'client',
				'subscriber'
			));
			$table->string('user_email');
			$table->string('user_password')->nullable();
			$table->string('user_respass_token')->nullable();
			$table->datetime('user_respass_date')->nullable();
			$table->boolean('user_respass_check')->nullable();
			$table->boolean('user_valid');
			$table->timestamp('user_created')->nullable();
			$table->timestamp('user_updated')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('turi_users');
	}
}
