@extends('layout-admin')
@section('title', $updating ? "Editar Hotsale" : "Crear Hotsale" )

@section('content')

@php
if ($updating) {
$hotsaleImages = json_decode($hotsale->hotsale_images, true);
}
@endphp

<section class="nuevos" id="index">
	<div class="container">
		<form class="modalForm" enctype="multipart/form-data" id="hotsale-form"
			action="{{url( $updating ? "/hotsale/editar-hotsale/{$hotsale->hotsale_id}" :'/hotsale/create')}}" method="post">
			@csrf
			<div class="box">
				@if (isset($_GET['new_reg']))
				<div class="alert alert-success">
					@if ($updating)
					Hotsale actualizado correctamente
					@else
					Hotsale creado correctamente
					@endif
				</div>
				@elseif($errors->any())
				<div class="alert alert-danger">
					{{$errors->first()}}
				</div>
				@endif

				<div id='create_hotsale_danger' class="alert alert-danger form_alert_toggle"></div>

				<div class="row">
					<div class="col-12">
						<h5 class="titleServiciosNM">Aspectos básicos</h5>
						<p class="titleHabitaciones">Los campos con <span style="color:red;">*</span> son obligatorios</p>

						<div class="form-group">
							<label>Imágenes de habitación</label>
							<div class="hotel_images">
								<ul id='hotel_selectize_imgul'>

									@if ($updating)
									@foreach ( $hotsaleImages as $ik => $img)
									<li data-index="{{$ik}}" class="hotel_image_item dyn_img_item">
										<div class="imgHotel">
											<img src="{{url("/storage/hotsale") . "/" . $img["350"]}}" alt="foto">
											<button class="button button_remove_img button_red button_small">
												<i class="fa fa-times"></i>
											</button>
										</div>
									</li>
									@endforeach
									@endif
									<li class="hotel_image_item hotel_image_new">
										<div class="imgHotel">
											<i class="fa fa-plus"></i>
											<input name='hotsale_images[]' id='hotel_selectize_img' multiple type="file"
												class="custom-file-input nuevaImagen" accept="image/*" max="5">
											<input name='hotsale_indexes_imgs' type="hidden" id='hotel_selectize_imgindx'>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<!--
						<div class="row mt-4" id="imgagenesHotel"></div>
											-->

						<div class="form-row">
							<div class="col-md-6 mb-3">
								<label for="hotsale_name" class="required">Nombre del Hot Sale <span class="required">*</span></label>
								<div class="input-group">
									<input maxlength="50" value="{{$hotsale->hotsale_name ?? ""}}" class="form-control"
										placeholder="Nombre del Hot Sale" required="required" name="hotsale_name" id="hotsale_name"
										type="text" />
								</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="hotsale_city" class="required">Ciudad <span class="required">*</span></label>
								<div class="input-group position-relative">
									<input class="form-control" autocomplete="off" placeholder="Digite la ciudad"
										value="{{$hotsale->city_name ?? ""}}" type="text" id='hotsale_city' />
									<input type="hidden" name="hotsale_city" value="{{$hotsale->city_id ?? ""}}"
										id="hotsale_city_real_val">
									<div id='hotsale_cities_results' class="list-group autocomplete_lists">
									</div>
								</div>

							</div>
						</div>

						<div class="form-row">
							<div class="col-md-6 mb-3">
								<label for="hotsale_hotel_name" class="required">Nombre Hotel <span class="required">*</span></label>
								<div class="input-group">
									<select class="select_principal" {{!$updating ? "disabled" : "" }} required="required"
										name="hotsale_hotel" id="hotsale_hotel_name">
										@if ($updating)
										<option selected value="{{$hotsale->hotel_id}}">{{$hotsale->hotel_name}}</option>
										@endif
									</select>
								</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="Hotsale_room_id" class='required'>Habitación <span class='required'>*</span></label>
								<div class="input-group">
									<select class="select_principal" {{!$updating ? "disabled" : "" }} name="hotsale_room"
										id="hotsale_room">
										@if ($updating)
										<option selected value="{{$hotsale->room_id}}">{{$hotsale->room_name}}</option>
										@endif
									</select>
								</div>
							</div>

						</div>
						<div class="form-row">
							<div class="col-md-3 mb-3">
								<label for="hotsale_price" class="required">Precio total <span class="required">*</span></label>
								<div class="input-group">
									<input class="form-control" required="required" placeholder="Precio total" name="hotsale_price"
										id="hotsale_price" type="text" value="{{$hotsale->hotsale_price ?? ""}}" />
								</div>
							</div>
							<div class="col-md-3 mb-3">
								<label for="hotsale_currency" class="required">Seleccionar Moneda<span class="required">*</span></label>
								<div class="input-group">
									<select class="select_principal" name="hotsale_currency" id="hotsale_currency">
										@php
										$currencies = ["COP","USD"]
										@endphp
										@foreach ($currencies as $cur)
										<option {{$updating && $hotsale->hotsale_currency === $cur ? "selected" : ""}} value="{{$cur}}">{{$cur}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="hotsale_discount" class="required">Porcentaje descuento <span
										class="required">*</span></label>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="icon-icono-ultimaD"></i>
									</span>
									<input class="form-control" value="{{$hotsale->hotsale_discount ?? ""}}" required="required"
										placeholder="% Descuento" name="hotsale_discount" id="hotsale_discount" type="number" />
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="col-md-6 mb-3">
								<label for="hotsale_mindate" class="required">Fecha inicio <span class="required">*</span></label>
								<div class="input-group">
									<span class="input-group-addon"><i class="icon-icono-diasD"></i></span>
									<input value="{{$updating ? formatDate($hotsale->hotsale_mindate) : ""}}" class="form-control dia_salida"
										autocomplete="off" placeholder="# Dias" required="required" name="hotsale_mindate"
										id="hotsale_mindate" type="text" />
								</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="hotsale_maxdate" class="required">Fecha fin <span class="required">*</span></label>
								<div class="input-group">
									<span class="input-group-addon"><i class="icon-icono-diasD"></i></span>
									<input value="{{$updating ? formatDate($hotsale->hotsale_maxdate) : ""}}" class="form-control dia_llegada"
										autocomplete="off" placeholder="# Dias" required="required" name="hotsale_maxdate"
										id="hotsale_maxdate" type="text" />
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label class="label_principal">Activo</label>
									<div class="radio_custom">
										<div class="radio_wrap">
											@if ($updating)
											<input id="hotsale_active_1" value="1" {{$hotsale->hotsale_active == 1 ? "checked" : ""}}
												type="radio" name="hotsale_active">
											@else
											<input id="hotsale_active_1" value="1" checked type="radio" name="hotsale_active">
											@endif
											<div class="radio_circle"></div>
										</div>
										<label for="hotsale_active_1">SI</label>
									</div>
									<div class="radio_custom">
										<div class="radio_wrap">

											@if ($updating)
											<input id="hotsale_active_0" value="0" {{$hotsale->hotsale_active == 0 ? "checked" : ""}}
												type="radio" name="hotsale_active">
											@else
											<input id="hotsale_active_0" value="0" type="radio" name="hotsale_active">
											@endif
											<div class="radio_circle"></div>
										</div>
										<label for="hotsale_active_0">NO</label>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
			<div class="box">
				<div class="row">
					<div class="col-12">
						<h5 class="titleServiciosNM mt-4">Aspectos detallados</h5>
						<div class="subseccion">
							<p>
								<label for="Hotsale_description">Descripción del Hot Sale</label>
							</p>
							{{-- <textarea rows="10" cols="30" name="Hotsale[description]" id="Hotsale_description"></textarea> --}}
							<div id="hotsale_description" data-name='hotsale_description' class='ckeditor_initial'>
								{!! $hotsale->hotsale_description ?? "" !!}
							</div>
						</div>

						<div class="aditionals_wrap  mt-3">
							<label class="label_principal">Servicios Adicionales</label>
							<div class="row flex-column-reverse flex-md-row formHome formPerfil mt-2">
								<div class="col-12" id="listservhb">
									<div class="row prop_list_wrap_services">

										@php
										$services = ["Piscina","Bolos","Bar","Parqueadero"];
										if ( $updating){
										$moreServices = json_decode($hotsale->hotsale_services);
										}
										@endphp
										@foreach ($services as $ks => $serv)
										<div class="col-md-6 list_item_eqroom">
											<div class="checkbox_custom">
												<div class="checkbox_wrap">

													@if ($updating && in_array($serv, $moreServices))
													<input id="service_{{$ks}}" checked name="hotsale_services[]" type="checkbox"
														class="form-check-input" value="{{$serv}}">
													@else
													<input id="service_{{$ks}}" name="hotsale_services[]" type="checkbox" class="form-check-input"
														value="{{$serv}}">
													@endif


													<i class="fa fa-check"></i>
												</div>
												<label class="ml-2" for="service_{{$ks}}">{{$serv}}</label>
											</div>
										</div>
										@endforeach

										@if ($updating)
										@foreach ($moreServices as $sk => $serv)
										@if (in_array($serv, $services))
										@continue
										@endif
										<div class="col-md-6 list_item_eqroom">
											<div class="checkbox_custom">
												<div class="checkbox_wrap">
													<input id="service_add_{{$sk}}" checked name="hotsale_services[]" type="checkbox"
														class="form-check-input" value="{{$serv}}">
													<i class="fa fa-check"></i>
												</div>
												<label class="ml-2" for="service_add_{{$sk}}">{{$serv}}</label>
											</div>
										</div>
										@endforeach
										@endif

									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="checkbox_custom" id="checbkox_hab">
												<div class="checkbox_wrap">
													<input id="service_add" type="checkbox" class="form-check-input">
													<i class="fa fa-check"></i>
												</div>
												<label class="ml-2" for="service_add">Otro Servicio</label>
											</div>
											<div class="add_service_wrap other_proper">
												<input placeholder="¿Qué Servicio?" type="text" id="add_service_text"
													class="form-control other_proper_text">
												<div class="mt-1">
													<button id="add_service_button"
														class="button m-0 button_blue other_proper_button button_small">
														<i class="fa fa-plus"></i>
													</button>
													<button id="cancel_service_button"
														class="button m-0 button_red other_proper_button button_small">
														<i class="fa fa-ban"></i>
													</button>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

						<!--<p id="servsHb"><span></span></p>-->
						<p><label for="Hotsale_terms">Términos y condiciones</label></p>
						<div id="hotsale_terms" data-name='hotsale_terms' class='ckeditor_initial'>
							{!! $hotsale->hotsale_terms ?? "" !!}
						</div>

						<div class="form-group">
							<button class="button button_blue mt-3" type="submit">{{$updating ? "Guardar" : "Crear"}}</button>
						</div>
					</div>

				</div>

			</div>
		</form>
	</div>
	</div>
</section>

@endsection