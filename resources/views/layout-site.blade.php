<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Subastas Turísticas</title>
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
  {{-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet"> --}}
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap" rel="stylesheet">
</head>

<body id="@yield('body_id', " site_body")">
  <header id='header_site'>
    <a href="{{url('/')}}">
      <h1 class="logo" title="Subastas turisticas"><img src="{{url('images/logo-subastas-turisticas.png')}}" alt="">
      </h1>
    </a>
    <ul id="three">
      <li></li>
      <li></li>
      <li></li>
    </ul>
    <nav>
      <div class="nav-wrapper">
        <div class="nav-top">
          <ul id="nav_top_user">
            <!-- <li id="search"><input type="text" class="bradius" placeholder="Buscar"></li> -->
            @if (!isAuth())
            <li id="div_login">
              <button id='toggle_modal_login' class="button button_blue button_small mb-0 no_hover" type="submit"
                role="button">
                <span class="icon-user"></span>
              </button>
            </li>
            @endif
            <li id="search_in_mobile" class="">
              <div id="search_in_mobile_wrap">
                <form action="index.php?r=site/searchinweb" method="POST">
                  <div class="search_mobile_content group-field">
                    <input name="term" placeholder="Buscar en la web." type="text" class="input_text group-before">
                    <button class="group-after group-addon button button_white"><i class="icon-search"></i></button>
                  </div>
                </form>
              </div>
            </li>

            @if (isAuth())
            <li id="div_profile">
              <button class="mb-0 button button_blue button_small no_hover">
                <span class="curreent_user_header">{{ explode(' ', getCurrentUserName())[0]  }}</span>
                <i class="icon-user"></i>
                <div class="subitem_header">
                  <ul class="sublist_header">
                    @if (currentUserHasRol('client'))
                    <li><a href="{{url('user/configuracion')}}">Mi configración</a></li>
                    @elseif(currentUserHasRol('hotel'))
                    <li><a href="{{url('/hotel/configuracion')}}">Mi configración</a></li>
                    @endif
                    <li><a href="{{url('/reserva/mis-reservas')}}">Mis Reservas</a></li>
                    <li><a href="{{url('/user/logout')}}">Cerrar Sesión</a></li>
                  </ul>
                </div>
              </button>
            </li>
            @endif
            <li id="div_search">
              <button class="button button_red button_small no_hover mb-0" type="submit">
                <i class="icon-search"></i>
              </button>
            </li>

            <!--<li id="div_login" style="display:none;"><a href="#" role="button" data-toggle="modal" data-target="#loginModal"><i class="icon-login"></i></a><span role="button" data-toggle="modal" data-target="#loginModal">Login</span></li>-->
          </ul>

        </div>

        <ul class="menu">
          <!-- Menu solo se mostrará en mobile. -->


          <li><a href="index.php">Home</a></li>
          <li><a href="como-funciona">Cómo funciona?</a></li>
          <li><a href="all_hotsales.html">Hot Sales</a></li>
          <li><a href="planes.html">Planes</a></li>
          <li><a href="#contact">Contáctenos</a></li>
        </ul>
      </div>
    </nav>

  </header>
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title bold" id="loginModalLabel">Ingresar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="alert alert-success modal_login_alert" id='modal_login_success' role="alert">
          </div>
          <div class="alert alert-danger modal_login_alert" id='modal_login_error' role="alert">
          </div>
          <input type="hidden" id='_token' value="{{csrf_token()}}">
          <div id='modal_login_body' class='login_modal_body show'>
            <form class='modal_login_form' action="" id='modal_login_form' data-url='{{url('/user/login')}}'>
              <div class="form-group">
                <label for="email_login">
                  <i class="fa fa-envelope label_icon"></i>
                  <span>Email</span>
                </label>
                <input type="email" name='email_login' id="email_login" class='form-control'>
              </div>
              <div class="form-group">
                <label for="password_login">
                  <i class="fas fa-key label_icon"></i>
                  <span>Contraseña</span>
                </label>
                <input type="password" name='password_login' id="password_login" class='form-control'>
              </div>
              <button class="button button_blue">Ingresar</button>
            </form>
          </div>
          <div id='modal_register_body' class='login_modal_body'>
            <form class='modal_register_form' action="" id='modal_register_form' data-url='{{url('/user/register')}}'>
              <div class="form-group">
                <label for="name_register">
                  <i class="fa fa-user label_icon"></i>
                  <span>Nombre</span>
                </label>
                <input type="text" name='name_register' id="name_register" class='form-control'>
              </div>
              <div class="form-group">
                <label for="email_register">
                  <i class="fa fa-envelope label_icon"></i>
                  <span>Email</span>
                </label>
                <input type="email" name='email_register' id="email_register" class='form-control'>
              </div>
              <div class="form-group">
                <label for="password_register">
                  <i class="fas fa-key label_icon"></i>
                  <span>Contraseña</span>
                </label>
                <input type="password" name='password_register' id="password_register" class='form-control'>
              </div>
              <div class="form-group">
                <label for="rpassword_register">
                  <i class="fas fa-key label_icon"></i>
                  <span>Repetir Contraseña</span>
                </label>
                <input type="password" name='rpassword_register' id="rpassword_register" class='form-control'>
              </div>
              <button class="button button_blue">Registrar</button>
            </form>
          </div>
          <div id='modal_forgotpassword_body' class='login_modal_body'>
            <form class='modal_restpass_form' action="" id='modal_restpass_form'
              data-url='{{url('/user/restore-password')}}'>
              <div class="form-group">
                <label for="email_forgotpassword">
                  <i class="fa fa-envelope label_icon"></i>
                  <span>Email</span>
                </label>
                <input type="email" id="email_forgotpassword" name="email_forgotpassword" class='form-control'>
              </div>
              <button class="button button_blue">Restaurar</button>
            </form>
          </div>

        </div>
        <div class="modal-footer">

          <button class="btn btn-link btn-sm modal_login_change_body" data-body='modal_login_body'>Ingresar</button>
          <button class="btn btn-link btn-sm modal_login_change_body" data-body='modal_register_body'>Crear
            cuenta</button>
          <button class="btn btn-link btn-sm modal_login_change_body" data-body='modal_forgotpassword_body'>¿Olvidó su
            contraseña?</button>

        </div>
      </div>
    </div>
  </div>

  @yield('content')

  <footer id="contact">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-4 footer-info1"><img class="img-fluid"
            src="{{url('images/logo-subastas-turisticas.png')}}" alt="">
          <p>Subastas Turísticas es una plataforma online que ofrece planes y ofertas hoteleras, que se ajustan al
            precio del usuario.</p>
          <p>Poseemos convenios con lo mejores hoteles de Colombia, regístrate en nuestra plataforma y obtén grandes
            descuentos.</p>
          <ul class="list_social">
            <li><a href="https://facebook.com"><i class="fab fa-facebook-f"></i></i></a></li>
            <li><a href="https://facebook.com"><i class="fab fa-twitter"></i></a></li>
            <li><a href="https://facebook.com"><i class="fab fa-instagram"></i></a></li>
            <li><a href="https://facebook.com"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
        <div class="col-md-12 col-lg-4 footer-info2">
          <h4 class="b8 mb-4">MAPA DEL SITIO</h3>
            <div class="linea"></div>
            <ul class="mapa-sitio">
              <li><a href="index.php">Home</a></li>
              <li><a href="#">Cómo funciona?</a></li>
              <li><a href="hotsales.html">Hot sales</a></li>
              <li><a href="planes.html">Planes</a></li>
              <li><a href="#contact">Contáctenos</a></li>
              <li><a href="#">Términos y condiciones</a></li>
            </ul>
        </div>
        <div class="col-md-12 col-lg-4 footer-info3 p-0">
          <h4 class="b8 mb-4">FORMULARIO DE CONTACTO</h4>
          <form action="" autocomplete="off">
            <div class="form-row">
              <div class="col-lg-12 col-sm-12 my-1 col-md-12">
                <label class="sr-only" for="inlineFormInputGroupUsername1">Nombre</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-user"></i></div>
                  </div>
                  <input name="nombre" type="text" class="form-control" id="inlineFormInputGroupUsername1"
                    placeholder="Nombre">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-lg-12 col-sm-12 my-1 col-md-12">
                <label class="sr-only" for="inlineFormInputGroupUsername2">Email</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">@</div>
                  </div>
                  <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Email">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-lg-12 col-sm-12 my-1 col-md-12">
                <label class="sr-only" for="inlineFormInputGroupUsername3">Teléfono</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-phone"></i></div>
                  </div>
                  <input type="text" class="form-control" id="inlineFormInputGroupUsername3" placeholder="Teléfono">
                </div>
              </div>
            </div>

            <div class="form-group">
              <textarea class="form-control" id="exampleTextarea" rows="5"
                placeholder="Escriba sus comentarios"></textarea>
            </div>
            <button class="button button_red" type="submit">Enviar</button>

          </form>
        </div>
        <div class="col-md-12 rights">
          <p>Subastas Turisticas ® | Todos los derechos reservados 2020</p>
        </div>
      </div>
    </div>

  </footer>
  <script src="{{asset('app.js')}}"></script>
</body>

</html>