@extends('layout-admin')
@section('body_id','hotel_config')
@section('title','Mi Configuración')
@section('content')

@php
$hotelImages = json_decode( $data->hotel_images, true);
@endphp

<section id="index">
  <div class="container">
    <div class="box">
      @if (isset($_GET['new_reg']))
      <div class="alert alert-success">
        Hotel Actualizado correctamente
      </div>
      @elseif($errors->any())
      <div class="alert alert-danger">
        {{$errors->first()}}
      </div>
      @endif
      <div id="create_hotel_danger" class="alert alert-danger form_alert_toggle"></div>

      <form id="update_hotel_config" action="{{'/hotel/configuracion'}}" enctype="multipart/form-data" method="post">
        <div class="formHome">
          @csrf
          <div class="form-row">
            <div class="col-12  my-2">
              <h5 class="titleServiciosNM">Configuración general</h5>
            </div>

            <div class="col-12">
              <div class="form-group">
                <label class='label_principal'>Imágenes de habitación</label>
                <div class="hotel_images">
                  <ul id='hotel_selectize_imgul'>

                    @foreach ( $hotelImages as $ik => $img)
                    <li data-index="{{$ik}}" class="hotel_image_item dyn_img_item">
                      <div class="imgHotel">
                        <img src="{{url("/storage/hotel") . "/" . $img["350"]}}" alt="foto">
                        <button class="button button_remove_img button_red button_small">
                          <i class="fa fa-times"></i>
                        </button>
                      </div>
                    </li>
                    @endforeach

                    <li class="hotel_image_item hotel_image_new">
                      <div class="imgHotel">
                        <i class="fa fa-plus"></i>
                        <input name='hotel_images[]' id='hotel_selectize_img' multiple type="file"
                          class="custom-file-input nuevaImagen" accept="image/*" max="5">
                        <input name='hotel_indexes_imgs' type="hidden" id='hotel_selectize_imgindx'>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group"><label for="" class='label_principal'>Nombre de hotel</label>
                <div class="input-group">
                  <input class="form-control" maxlength="150" placeholder="Nombre del hotel" name="hotel_name"
                    id="hotel_name" type="text" value="{{$data->hotel_name}}">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group"><label for="" class='label_principal'>Email de hotel</label>
                <div class="input-group">
                  <input disabled class="form-control" placeholder="Correo" maxlength="255" id="hotel_email" type="text"
                    value="{{$data->user_email}}">
                </div>
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <label for="" class='label_principal'>Contraseña</label>
                  <input name='hotel_password' class="form-control" placeholder="Contraseña" maxlength="255"
                    id="hotel_password" type="password" value="">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group"><label for="" class='label_principal'>Repita su contraseña</label>
                <div class="input-group">
                  <input name='hotel_rpassword' class="form-control" placeholder="Repita su contraseña" maxlength="255"
                    id="hotel_respassword" type="password" value="">
                </div>
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-group position-relative">
                <label class="label_principal required" for="hotel_city">Ciudad
                  <span class="required">*</span>
                </label>
                <input disabled value="{{$data->city_name ?? ""}}" size="60" autocomplete="new-password" maxlength="150"
                  class="form-control" id="hotel_city" type="text" />
                {{-- <input value="{{$data->city_id ?? ""}}" type="hidden" name="hotel_city" id="hotel_city_real_val">
                <div id='hotel_cities_results' class="list-group autocomplete_lists">
                </div> --}}
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label class="label_principal" for="hotel_rooms"># habitaciones
                </label>
                <input value="{{$data->hotel_rooms ?? ""}}" size="60" maxlength="150" class="form-control"
                  name="hotel_rooms" id="hotel_rooms" type="number" />
              </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="label_principal required" for="hotel_address">Dirección
                  <span class="required">*</span>
                </label>
                <input size="60" value="{{$data->hotel_address ?? ""}}" maxlength="150" class="form-control"
                  name="hotel_address" id="hotel_address" type="text" />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="label_principal required" for="hotel_phone">Teléfono
                  <span class="required">*</span>
                </label>
                <input value="{{$data->hotel_phone ?? ""}}" size="60" maxlength="150" class="form-control"
                  name="hotel_phone" id="hotel_phone" type="number" />
              </div>
            </div>

          </div>

          <div class="hotel_configuration_services mb-3">
            @component('components.dynamic-checkbox', array(
            'mainArray' => ['Piscina', 'Bolos','Bar', 'Parqueadero'],
            'otherArray' => !empty( $data->hotel_services) ? json_decode( $data->hotel_services, true) : null,
            'uid' => 'hotel_services'
            ))
            @endcomponent
          </div>

          <div class="form-group">
            <label for="" class="label_principal">Descripción de Hotel</label>
            <div id="hotel_description" data-name='hotel_description' class="hotel_config ckeditor_initial">
              {!! $data->hotel_description !!}
            </div>
          </div>
          <div class="form-group">
            <label for="" class="label_principal">Términos de Hotel</label>
            <div id="hotel_terms" data-name='hotel_terms' class="hotel_config ckeditor_initial">
              {!! $data->hotel_terms !!}
            </div>
          </div>

        </div>
        <button class="button mt-3 button_blue no_hover">Guardar</button>


      </form>
    </div>

  </div>
</section>
@endsection