@extends('layout-admin')
@section('content')
@section('body_id', $updating ? "update_hotel": "create_hotel")
@section('title', $updating ? "Actualizar Hotel" : "Crear Hotel")

@php

if ( $updating){
$hotelImages = json_decode( $hotel->hotel_images, true);
}
@endphp

<section id="index">
	<div class="container">
		<div class="box">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="formHome" id="formHabitaciones">
						@if (isset($_GET['new_reg']))
						<div class="alert alert-success">
							@if ($updating)
							Hotel Actualizado correctamente
							@else
							Hotel creado correctamente
							@endif
						</div>
						@elseif($errors->any())
						<div class="alert alert-danger">
							{{$errors->first()}}
						</div>
						@endif

						<div id='create_hotel_danger' class="alert alert-danger form_alert_toggle"></div>

						<form enctype="multipart/form-data" id="hotel-form"
							action="{{url($updating ? "hotel/editar-hotel/{$hotel->hotel_id}" :'/hotel/create')}}" method="post">
							@csrf

							<h5 class="titleServiciosNM">Crear un nuevo hotel</h5>

							<p class="note">Los campos con <span class="required">*</span> son obligatorios.</p>

							<div class="form-group">
								<label class='label_principal'>Imágenes de habitación</label>
								<div class="hotel_images">
									<ul id='hotel_selectize_imgul'>

										@if ($updating)
										@foreach ( $hotelImages as $ik => $img)
										<li data-index="{{$ik}}" class="hotel_image_item dyn_img_item">
											<div class="imgHotel">
												<img src="{{url("/storage/hotel") . "/" . $img["350"]}}" alt="foto">
												<button class="button button_remove_img button_red button_small">
													<i class="fa fa-times"></i>
												</button>
											</div>
										</li>
										@endforeach
										@endif

										<li class="hotel_image_item hotel_image_new">
											<div class="imgHotel">
												<i class="fa fa-plus"></i>
												<input name='hotel_images[]' id='hotel_selectize_img' multiple type="file"
													class="custom-file-input nuevaImagen" accept="image/*" max="5">
												<input name='hotel_indexes_imgs' type="hidden" id='hotel_selectize_imgindx'>
											</div>
										</li>
									</ul>
								</div>
							</div>


							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="label_principal required" for="hotel_name">Nombre
											<span class="required">*</span>
										</label>
										<input value="{{$hotel->hotel_name ?? ""}}" size="60" maxlength="150" class="form-control"
											name="hotel_name" id="hotel_name" type="text" />
									</div>
									<div class="form-group position-relative">
										<label class="label_principal required" for="hotel_city">Ciudad
											<span class="required">*</span>
										</label>
										<input value="{{$hotel->city_name ?? ""}}" size="60" autocomplete="new-password" maxlength="150"
											class="form-control" id="hotel_city" type="text" />
										<input value="{{$hotel->city_id ?? ""}}" type="hidden" name="hotel_city" id="hotel_city_real_val">
										<div id='hotel_cities_results' class="list-group autocomplete_lists">
										</div>
									</div>
									<div class="form-group">
										<label class="label_principal required" for="hotel_email">Email
											<span class="required">*</span>
										</label>
										<input value="{{$hotel->user_email ?? ""}}" size="60" maxlength="150" class="form-control"
											name="hotel_email" id="hotel_email" type="email" />
									</div>
									<div class="form-group">
										<label class="label_principal required" for="hotel_password">Contraseña
											<span class="required">*</span>
										</label>
										<input size="60" maxlength="150" class="form-control" name="hotel_password" id="hotel_password"
											type="password" />
									</div>
									<div class="form-group">
										<label class="label_principal required" for="hotel_rpassword">Repetir Contraseña
											<span class="required">*</span>
										</label>
										<input size="60" maxlength="150" class="form-control" name="hotel_rpassword" id="hotel_rpassword"
											type="password" />
									</div>

									<div class="form-group">
										<label class="label_principal">Activo</label>
										<div class="radio_custom">
											<div class="radio_wrap">
												@if ($updating)
												<input id="Hotel_active_1" value="1" {{$hotel->hotel_active === 1 ? "checked" : ""}}
													type="radio" name="hotel_active" />
												@else
												<input id="Hotel_active_1" value="1" checked type="radio" name="hotel_active" />
												@endif
												<div class="radio_circle"></div>
											</div>
											<label for="Hotel_active_1">SI</label>
										</div>
										<div class="radio_custom">
											<div class="radio_wrap">
												@if ($updating)
												<input id="Hotel_active_0" {{$hotel->hotel_active === 0 ? "checked" : ""}} value="0"
													type="radio" name="hotel_active" />
												@else
												<input id="Hotel_active_0" value="0" type="radio" name="hotel_active" />
												@endif
												<div class="radio_circle"></div>
											</div>
											<label for="Hotel_active_0">NO</label>
										</div>
									</div>
									<div class="buttons mt-3">
										<button type="submit" class="button button_blue no_hover" value="Perfil"
											id="boton_perfil">{{$updating ? 'Actualizar' : 'Crear'}}</button>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="label_principal required" for="hotel_address">Dirección
											<span class="required">*</span>
										</label>
										<input size="60" value="{{$hotel->hotel_address ?? ""}}" maxlength="150" class="form-control"
											name="hotel_address" id="hotel_address" type="text" />
									</div>
									<div class="form-group">
										<label class="label_principal required" for="hotel_phone">Teléfono
											<span class="required">*</span>
										</label>
										<input value="{{$hotel->hotel_phone ?? ""}}" size="60" maxlength="150" class="form-control"
											name="hotel_phone" id="hotel_phone" type="number" />
									</div>
									<div class="form-group">
										<label class="label_principal" for="hotel_rooms"># habitaciones
										</label>
										<input value="{{$hotel->hotel_rooms ?? ""}}" size="60" maxlength="150" class="form-control"
											name="hotel_rooms" id="hotel_rooms" type="number" />
									</div>
									<div class="form-group">
										<label class="label_principal" for="Hotel_description">Características</label>
										<div data-name='hotel_description' id="hotel_description" class="ckeditor_initial">

											{!! $hotel->hotel_description ?? "" !!}
										</div>
									</div>

								</div>
						</form>
					</div>
				</div><!-- form -->
			</div>
		</div>
	</div>
</section>

@endsection