@extends('layout-site')
@section('body_id',"home_body")

@section('content')

<section id="banner">
  <div class="formulario bradius">
    <div class="formulario-top bradius">SUBASTA TU RESERVA EN MENOS DE <span class="48horas">48 HORAS</span></div>
    <div class="formulario-datos">
      <form action="">
        <input type="hidden" value="{{csrf_token()}}" id="token_subasta">
        <div class="formulario-select">
          <div>
            <label for="destino" class="b7">Destino</label>
            <select name="destino" id="destino">
              <option value="none">Seleccione una ciudad</option>
              @foreach ($cities as $city)
              <option value="{{$city->city_id}}">{{$city->city_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="formulario-select">
          <div>
            <label for="fecha_ingreso" class="b7">Llegada</label>
            <input id="fecha_ingreso" type="text" class="data" placeholder="dd/mm/yy" readonly>
          </div>
          <div>
            <label for="fecha_salida" class="b7">Salida</label>
            <input id="fecha_salida" type="text" class="data2" placeholder="dd/mm/yy" readonly>
          </div>
        </div>
        <div class="formulario-select">
          <div>
            <label for="adultos" class="b7">Adultos</label>
            <select name="adultos" id="adultos">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </select>
          </div>
          <div>
            <label for="ninos" class="b7 exclama-label">
              <span class="exclama-texto">Niños mayores de 2 años</span>Niños
              <i class="icon-exclamation exclama"></i>
            </label>
            <select name="ninos" id="ninos">
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </select>
          </div>
          <div>
            <label for="infantes" class="b7 exclama-label">
              <span class="exclama-texto">Niños menores a 2 años</span>Infantes
              <i class="icon-exclamation exclama"></i>
            </label>
            <select name="infantes" id="infantes">
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </select>
          </div>
        </div>
        <div id="newkids" class="newkids">

        </div>
        <div class="presupuesto">
          <label class="b7">Presupuesto por noche</label>
        </div>
        <div id="rangos">
          <input type="text" id="input-with-keypress-0">
          <input type="text" id="input-with-keypress-1">
        </div>
        <div id="slider-snap">

        </div>

        @php
        $email = session()->get("user_email");
        $nombre = session()->get("user_name");
        @endphp
        <div class="formulario-select form-datos">
          <div><label for="email" class="b7">Nombre</label> <input id="subasta_nombre" {{$nombre ? "readonly" : ""}}
              type="text" value="{{ $nombre ?? ""}}"></div>
          <div><label for="email" class="b7">E-mail</label> <input id="subasta_email" {{ $email ? "readonly" : ""}}
              type="text" value="{{ $email ?? ""}}"></div>
        </div>


        <div class="subastar bradius">
          <a href="#" id="send-subasta" title="SUBASTAR"><span>SUBASTAR</span></a>
        </div>
        <p id='subastar_error' class="text-danger my-2 d-none">Error</p>
      </form>
    </div>
  </div>
  @foreach ($plansbanner as $kb => $pb)
  <a href="{{url("/plan/ver-plan") . "/" . $pb->plan_id}}">
    <div id="banner-promo-{{$kb}}" class="banner-promo" style="display:none;">
      <div class="banner-promo-titulo">
        <h4>PLAN EN</h4>
      </div>
      <h3 class="antonio">{{$pb->city_name}}</h3>
      <p class="porsolo">- por sólo -</p>
      <div class="label-naranja">
        <p class="antonio">{{formatPrice($pb->plan_price, "")}}</p>
        <p class="moneda">{{$pb->plan_currency}}</p>
      </div>
      <div class="banner-promo-estado">
        <p>INCLUYE</p>
      </div>
      <ul class="caracteristicas mt-3 pr-1 pl-1">
        @foreach (json_decode($pb->plan_special_service, true) as $img)
        <li>
          <div class="whiter-border white"><i class="white {{$img["icon"]}}"></i></div>
          <h5>{{$img["title"]}}</h5>
        </li>
        @endforeach

      </ul>
      <button class="button button_blue contrast">Ver más</button>
    </div>
  </a>
  @endforeach

  <div class="camera_wrap">
    @foreach ($plansbanner as $pb)
    @if (count(json_decode($pb->plan_images, true)) > 0)
    <div data-src="{{url("/storage/plan") . "/" . json_decode($pb->plan_images, true)[0]["xl"] }}">
    </div>
    @else
    <div data-src="{{asset("/images/subastas-turisticas-plan-background-min.jpg") }}">
    </div>
    @endif
    @endforeach



  </div>

  <!-- Modal -->
  <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="titleModal">Error</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul id="alert_errors">
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- end modal -->

  <!-- Modal -->
  <div class="modal fade" id="auctionModal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="titleModal">Subasta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="subasta_details_wrap"></div>
          <div id="subasta_error" class="alert alert-danger alert_error"></div>
          <div id="subasta_rows">

          </div>
        </div>
        <div class="modal-footer">
          <button id="btn_save_auction" type="button" class="button button_red" data-dismiss="modal">Guardar
            Subasta</button>
          <button id="btn_finish_auction" type="button" class="button button_red dimiss_modal"
            data-dismiss="modal">Finalizar subasta</button>
        </div>
      </div>
    </div>
  </div>



</section>

{{-- Final de Banner --}}

{{-- Subscripcion --}}
<section id="suscripcion">
  <div class="container">
    <div class="row">
      <div class="col-md-12 promociones">
        <div class="row">
          <div class="col-md-9 promociones-form">
            <h4 class="b8">SUSCRÍBETE Y RECIBE<br><span class="b9 naranja"> PROMOCIONES ESPECIALES</span></h4>
            <form action="" id="promo">
              <div class="row">
                <div class="col-lg-6 col-sm-12 my-1 col-md-12">
                  <label class="sr-only" for="inlineFormInputGroupUsername5">Nombre</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fa fa-user"></i></div>
                    </div>
                    <input type="text" class="form-control" id="inlineFormInputGroupUsername5" placeholder="Nombre">
                  </div>
                </div>
                <div class="col-lg-6 col-sm-12 my-1 col-md-12">
                  <label class="sr-only" for="inlineFormInputGroupUsername6">Email</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">@</div>
                    </div>
                    <input type="email" class="form-control" id="inlineFormInputGroupUsername6" placeholder="Email">
                  </div>
                </div>
                <div class="col-12 text-right mt-2">
                  <button type="submit" class="button button_red no_hover">REGISTRARSE</button>
                </div>

              </div>

            </form>
            <p class="mt-1">* Las mejores promociones en hoteles y pasajes para toda Colombia</p>
          </div>
          <div class="col-md-3 p-0">
            <div class="promociones-foto">
              <div class="wrapper">
                <img src="{{url('images/subastas-turisticas-promo.svg')}}" alt="">
                <h4 class="b9 text-white font-weight-bold">OFERTAS <br>EXCLUSIVAS</h4>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

</section>

{{-- Destacados --}}

<section id="destacados">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <i class="icon-destacados"></i>
        <h2>DESTACADOS</h2>

        <p>Las mejores promociones en hoteles para toda Colombia</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="destacador-arrow destacados-arrow-left">
          <i class="fas fa-angle-left"></i>
        </div>
        <div class="destacados-slider">
          @foreach ($hotsales as $hs)
          @component('components.card-hotsale', ['hotsale' => $hs])
          @endcomponent
          @endforeach
        </div>
        <div class="destacador-arrow destacados-arrow-right">
          <i class="fas fa-angle-right"></i>
        </div>
      </div>
      <div class="col-12 mb-5 mt-2 text-center">
        <a href="{{url('hotsales')}}" class="button button_blue no_hover">Todos los Destacados</a>
      </div>

    </div>
  </div>

</section>

{{-- Planes --}}

<section id="planes">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center text-white">
        <i class="icon-planes"></i>
        <h2>PLANES TURÍSTICOS</h2>
        <p>Los mejores planes turísticos con todo incluido.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center mt-3">
        <form id="filter_plans" action="">
          <select class="bradius" name="plan_city" id="plan_city">
            <option value="">Todas las ciudades</option>
            @foreach ($cityplan as $citid => $city)
            <option value="{{$citid}}">{{strtoupper($city)}}</option>
            @endforeach
          </select> <button type="submit" class="button button_red no_hover">BUSCAR</button>
        </form>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 planes-turisticos">
        <div class="row">
          @php $urlImages = url("/storage/plan") . "/"; @endphp
          @foreach ($plans as $plan)
          <div class=" col-xl-3 col-lg-4 col-md-6 col-sm-6 plan_square plan_68132">
            <a href="{{url("/plan/ver-plan") . "/" . $plan->plan_id}}">
              <div class="planes">
                <div class="planes-icono"><i class="icon-martillo"></i></div>
                <div class="planes-foto">
                  <img src="{{$urlImages . json_decode($plan->plan_images, true)[0]["350"]}}" alt="">
                </div>
                <div class="planes-ciudad">
                  <h3>{{ strtoupper($plan->city_name) }}</h3>
                  <div class="precio">
                    <p>{{formatPrice($plan->plan_price, "")}}</p>
                    <p class="precio_moneda">{{$plan->plan_currency}}</p>
                  </div>
                  <ul class="caracteristicas">
                    @foreach (json_decode($plan->plan_special_service, true) as $plansp)
                    <li>
                      <div><i class="{{$plansp["icon"]}}"></i></div>
                      <h5>{{$plansp["title"]}}</h5>
                    </li>
                    @endforeach

                  </ul>
                </div>
              </div>
            </a>
          </div>
          @endforeach

          <div class="col-md-12 mt-4 text-center">
            <a href="planes.html" class="button button_red no_hover bold">Todos los planes</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


@endsection