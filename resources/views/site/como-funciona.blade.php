@extends('layout-site')
@section('body_id', 'how_works_body')

@section('content')


<section id="galeriadest">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 m-0 p-0">
        <div class="camera_wrap" id="bannerimages">
          <div data-thumb="{{url('/images/howorks')}}/banner-1.jpg" data-src="{{url('/images/howorks')}}/banner-1.jpg">
            <div class="txtbanner">
              <div class="row" id="rowbanner">
                <div class="col-md-6">
                  <div class="divtitle">
                    <div class="titleBanner fadeIn">
                      <h3>Crea tu propio registro y conoce todos nuestros <span>descuentos</span></h3>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="formabout">
                    <div class="formularioreg">
                      <h3>Crear un registro nuevo</h3>
                      <div id='about_form1_success' class="alert alert-success d-none"><span>Correcto</span></div>
                      <div id='about_form1_error' class="alert alert-danger d-none"><span>Error</span></div>
                      <form id="about_form1" action="">

                        <div class="form-group">
                          <label for="nombres">Tu Nombre y apellido</label>
                          <input class='form-control' type="text" id="about_form1_nombres">
                        </div>
                        <input id="about_form1_type_sub" type="hidden" value="howworks">
                        <div class="form-group">
                          <label for="email">Tu E-Mail</label>
                          <input class='form-control' type="text" id="about_form1_email">
                        </div>
                        <div class="form-group">
                          <button id='about_form1_button' type="submit" class="button button_blue">Registrar</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div data-thumb="{{url('/images/howorks')}}/banner.jpg" data-src="{{url('/images/howorks')}}/banner.jpg">
            <div class="txtbanner">
              <div class="row" id="rowbanner">
                <div class="col-md-6">
                  <div class="divtitle">
                    <div class="titleBanner fadeIn">
                      <h3>Crea tu propio registro y conoce todos<br>nuestros <span>descuentos</span></h3>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="formabout">
                    <div class="formularioreg">
                      <h3>Crear un registro nuevo</h3>
                      <div id='about_form2_success' class="alert alert-success d-none"><span>Correcto</span></div>
                      <div id='about_form2_error' class="alert alert-danger d-none"><span>Error</span></div>
                      <form id="about_form2" action="">

                        <div class="form-group">
                          <label for="nombres">Tu Nombre y apellido</label>
                          <input class='form-control' type="text" id="about_form2_nombres">
                        </div>
                        <input id="about_form2_type_sub" type="hidden" value="howworks">
                        <div class="form-group">
                          <label for="email">Tu E-Mail</label>
                          <input class='form-control' type="text" id="about_form2_email">
                        </div>
                        <div class="form-group">
                          <button id='about_form2_button' type="submit"
                            class="button button_blue no_hover text-center">Registrar</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- INICIO CONTENIDO -->

<section class="secionabout" id="pasos">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h3>Tres simples <span>pasos</span></h3>
        <span class="spanmore">Asi es como haras parte de Nosotros</span>
        <ul class="ulpasos">
          <li>
            <div class="ico"><i class="icon-ulpasos"></i></div>LLena los
            espacios con tus datos personales y dale al boton empezar para continuar
          </li>
          <li>
            <div class="ico"><i class="icon-ulpasos"></i></div>Al registrar
            tu direccion Email podremos enviarte por email todo lo relacionado con las subastas
          </li>
          <li>
            <div class="ico"><i class="icon-ulpasos"></i></div>Al
            registrarse tendras la opcion de recibir asistencia las 24 horas sobre promociones y descuentos
          </li>
        </ul>
      </div>
      <div class="col-md-4">
        <div class="imgpasos">
          <div class="icopasos">
            <img src="{{url('/images/howorks')}}/imgpasos.png" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="secionabout" id="mejorexperiencia">
  <div class="container">
    <div class="row">
      <h3>La mejor experiencia con nuestro <span>Servicio</span></h3>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="infoexperiencia">
          <div class="imgexp">
            <img src="{{url('/images/howorks')}}/serv-1.jpg" alt="">
          </div>
          <div class="tituloexp">
            <p class="titulorojo">Hoteles al mejor Precio</p>
          </div>
          <div class="txtexp">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
              laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="infoexperiencia">
          <div class="imgexp">
            <img src="{{url('/images/howorks')}}/serv-2.jpg" alt="">
          </div>
          <div class="tituloexp">
            <p>La mejor opción en Subastas</p>
          </div>
          <div class="txtexp">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
              laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="infoexperiencia">
          <div class="imgexp">
            <img src="{{url('/images/howorks')}}/serv-3.jpg" alt="">
          </div>
          <div class="tituloexp">
            <p>Promociones Increibles</p>
          </div>
          <div class="txtexp">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
              laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="secionabout" id="comotrabajamos">
  <div class="container">
    <div class="row">
      <h3>¿Cómo <span>trabajamos?</span></h3>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="rowcomotrab">
          <div class="comotrab">
            <div class="ico">
              <img src="{{url('/images/howorks')}}/trabajo-1.png" alt="">
            </div>
            <div class="infocomo">
              <h4>Abre y cierra tu alojamiento sin clausula de permanencia</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut
                laoreet dolore magna.</p>
            </div>
          </div>
          <div class="comotrab">
            <div class="ico">
              <img src="{{url('/images/howorks')}}/trabajo-2.png" alt="">
            </div>
            <div class="infocomo">
              <h4>Control de Disponibilidad </h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut
                laoreet dolore magna.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="rowcomotrab">
          <div class="comotrab">
            <div class="ico">
              <img src="{{url('/images/howorks')}}/trabajo-3.png" alt="">
            </div>
            <div class="infocomo">
              <h4>Garantizamos la calidad en el servicio de los hoteles Aliados</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut
                laoreet dolore magna.</p>
            </div>
          </div>
          <div class="comotrab">
            <div class="ico">
              <img src="{{url('/images/howorks')}}/trabajo-4.png" alt="">
            </div>
            <div class="infocomo">
              <h4>Ofrecemos el mayor descuento en relación a la competencia</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut
                laoreet dolore magna.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="secionabout" id="alojamientos">
  <div class="container">
    <div class="row">
      <h3>Tipos de alojamiento</h3>
    </div>
    <div class="row">
      <div class="col-lg">
        <div class="tipo">
          <div class="imgtipo">
            <img src="{{url('/images/howorks')}}/alojamiento-1.jpg" alt="">
          </div>
          <div class="titletipo">
            <h3>Apartamento</h3>
          </div>
          <div class="txtitle">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut laoreet
              dolore magna.</p>
          </div>
        </div>
      </div>
      <div class="col-lg">
        <div class="tipo">
          <div class="imgtipo">
            <img src="{{url('/images/howorks')}}/alojamiento-2.jpg" alt="">
          </div>
          <div class="titletipo">
            <h3>Villas</h3>
          </div>
          <div class="txtitle">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut laoreet
              dolore magna.</p>
          </div>
        </div>
      </div>
      <div class="col-lg">
        <div class="tipo">
          <div class="imgtipo">
            <img src="{{url('/images/howorks')}}/alojamiento-3.jpg" alt="">
          </div>
          <div class="titletipo">
            <h3>Chalets de montaña</h3>
          </div>
          <div class="txtitle">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut laoreet
              dolore magna.</p>
          </div>
        </div>
      </div>
      <div class="col-lg">
        <div class="tipo">
          <div class="imgtipo">
            <img src="{{url('/images/howorks')}}/alojamiento-4.jpg" alt="">
          </div>
          <div class="titletipo">
            <h3>Hostales y pensiones</h3>
          </div>
          <div class="txtitle">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut laoreet
              dolore magna.</p>
          </div>
        </div>
      </div>
      <div class="col-lg">
        <div class="tipo">
          <div class="imgtipo">
            <img src="{{url('/images/howorks')}}/alojamiento-5.jpg" alt="">
          </div>
          <div class="titletipo">
            <h3>Bed and Breakfast</h3>
          </div>
          <div class="txtitle">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismtincidunt ut laoreet
              dolore magna.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="secionabout" id="comofunciona">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center">¿Cómo funciona <span>Subastas Turísticas?</span></h3>
      </div>
    </div>
    <div class="row rowcomo">
      <div class="col-md-6 divorder2">
        <div class="imgcomo">
          <img src="{{url('/images/howorks')}}/funciona-1.svg" alt="">
        </div>
      </div>
      <div class="col-md-6 divorder">
        <div class="infocomo">
          <h3>Ingresa tu destino</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
        </div>
      </div>
    </div>
    <div class="row rowcomo">
      <div class="col-md-6 divorder">
        <div class="infocomo">
          <h3>Escoje un Monto</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
        </div>
      </div>
      <div class="col-md-6 divorder2">
        <div class="imgcomo">
          <img src="{{url('/images/howorks')}}/funciona-2.svg" alt="">
        </div>
      </div>
    </div>
    <div class="row rowcomo">
      <div class="col-md-6 divorder2">
        <div class="imgcomo">
          <img src="{{url('/images/howorks')}}/funciona-3.svg" alt="">
        </div>
      </div>
      <div class="col-md-6 divorder">
        <div class="infocomo">
          <h3>Empieza la subasta</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
        </div>
      </div>
    </div>
    <div class="row rowcomo">
      <div class="col-md-6 divorder">
        <div class="infocomo">
          <h3>Escoje la mejor opción</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
        </div>
      </div>
      <div class="col-md-6 divorder2">
        <div class="imgcomo">
          <img src="{{url('/images/howorks')}}/funciona-4.svg" alt="">
        </div>
      </div>
    </div>
    <div class="row rowcomo">
      <div class="col-md-6 divorder2">
        <div class="imgcomo">
          <img src="{{url('/images/howorks')}}/funciona-5.svg" alt="">
        </div>
      </div>
      <div class="col-md-6 divorder">
        <div class="infocomo">
          <h3>Elige tu forma de pago</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="secionabout" id="pqrsf">
  <div class="container">
    <div class="row">
      <h3>Preguntas <span>frecuentes</span></h3>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="titulopqr">
          <h4>¿Dónde puedo ver cuánta comisión pagaré por las reservas?</h4>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="titulopqr">
          <h4>¿Qué pasará cuando me registre?</h4>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
            laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="regfooter">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <h3>Anímate y empieza a recibir todos los<br>beneficios de registrarte</h3>
      </div>
      <div class="col-md-12" id="formfooter">
        <div class="formabout">
          <div class="formularioreg">
            <h3>Crear un registro nuevo</h3>
            <div id='about_form3_success' class="alert alert-success d-none"><span>Correcto</span></div>
            <div id='about_form3_error' class="alert alert-danger d-none"><span>Error</span></div>
            <form id="about_form3" action="">

              <div class="form-group">
                <label for="nombres">Tu Nombre y apellido</label>
                <input class='form-control' type="text" id="about_form3_nombres">
              </div>
              <input id="about_form3_type_sub" type="hidden" value="howworks">

              <div class="form-group">
                <label for="email">Tu E-Mail</label>
                <input class='form-control' type="text" id="about_form3_email">
              </div>
              <div class="form-group">
                <button id='about_form3_button' type="submit"
                  class="button button_blue no_hover text-center">Registrar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection