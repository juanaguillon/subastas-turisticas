@extends('layout-site')
{{-- Sitio: Ver todos los Sitios --}}
@section('body_id','todos_hotsales')
@section('content')

<section id="banner_application">
  <div class="banner_application_wrap">
    <div class="banner_application">
      <div class="nombreHabitacion">
        <div class="nombrehabitacion_icono">
          <i class="icon-martillo"></i>
        </div>
        <h3>Nuestros HotSales</h3>
      </div>
      <img src="https://www.subastasturisticas.com/files/images/1528217311_5613121.jpg" alt="">
    </div>
  </div>
</section>
<section id="" class="py-5 section_gray">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="filtro_general">
          <form id="filter_plans" action="">
            <h3 class="front_title">Filtrar</h3>
            <div class="filtro_inputs">
              <div class="field">
                <label for="filtro_price" class="label_principal mb-0">Por Precio</label>
                <select class="select_principal" name="" id="filtro_price">
                  <option value="">Todos los precios</option>
                  <option value="less_100">Menor a $100.000</option>
                  <option value="100-200">$100.000 - $200.000</option>
                  <option value="200-350">$200.000 - $3500.000</option>
                  <option value="more_350">Mayor $350.000</option>
                </select>
              </div>
              <div class="field">
                <label for="filtro_city" class="label_principal mb-0">Por Ciudad</label>
                <select class="select_principal" name="filtro_city" id="filtro_city">
                  <option value="">Todas las ciudades</option>
                  <option value="11001">BOGOTÁ D.C.</option>
                  <option value="68132">CALIFORNIA</option>
                  <option value="3">CANCÚN</option>
                  <option value="13001">CARTAGENA</option>
                </select> </div>
              <!-- <div class="field">
                <label for="filtro_fecha" class="label_principal mb-0">Por Fecha</label>
                <input id="filtro_fecha" type="text" class="dia_llegada" placeholder="dd/mm/yy" readonly>
              </div> -->
            </div>
            <button class="button button_red no_hover mt-2">Filtrar</button>
          </form>
        </div>
        <div class="desctacados_items_wrap">
          @foreach ($hotsales as $hotsale)
          @component('components.card-hotsale', ['hotsale'=> $hotsale])
          @endcomponent
          @endforeach


        </div>
      </div>
    </div>

  </div>
</section>


@endsection