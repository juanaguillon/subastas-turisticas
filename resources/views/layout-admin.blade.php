<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Subastas Turísticas - @yield('title', "Inicio") </title>
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
  {{-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet"> --}}
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap" rel="stylesheet">
</head>

@php
function activeLinkInUrl($url){
if ( \Request::is($url)){
echo "active";
}
}
@endphp

<body id="@yield('body_id', " admin_body")">

  <header>
    <div class="jumbotron jumbotron-fluid jumbotronInt">
      <div class="container">
        <div class="admin_header">
          <div class="admin_logo_wrap">
            <a href="{{url("/")}}">
              <img src="{{url('/images/logo-subastas-turisticas.png')}}" alt="">
            </a>
          </div>

          {{-- <div class="admin_user_wrap">
          <div class="imgPerfil">
            <img src="" alt="">
          </div>
          <ul class="navbar-nav perfilMenu">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                Admin </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="#">Mensajes</a>
                <a class="dropdown-item" href="admin_config.html">Mi configuración</a>
                <a class="dropdown-item" href="cerrar_sesion.html">Cerrar sesión</a>
              </div>
            </li>
          </ul>
        </div> --}}
          <div>
            <span>{{getCurrentUserName()}}</span>

            <a href="{{url("user/logout")}}">Cerrar Sesión</a>
          </div>

        </div>
      </div>
    </div>
    <!-- Require the navigation -->


    <nav class="navbar navbar-expand-md bg-light navbar-light" id="main_admin_wrap">
      <div class="w-100 px-3">
        <button class="m-0 button button_white button_small" type="button" id="main_admin_button_nav">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="main_admin_nav_wrap" id="collapsibleNavbar">
          <ul class="navbar-nav" style="width:100%">

            @if (currentUserHasRol("admin"))
            <li class="nav-item navInt {{activeLinkInUrl('hotel/ver-hoteles')}}">
              <a class="nav-link" href="{{url('hotel/ver-hoteles')}}"><i class="fas fa-h-square"></i>Hoteles</a>
            </li>
            <li class="nav-item navInt {{activeLinkInUrl('subasta/mis-subastas')}}">
              <a class="nav-link" href="{{url("subasta/mis-subastas")}}"><i class="icon-martillo"></i>Subastas</a>
            </li>

            <li class="nav-item navInt {{activeLinkInUrl('hotsale/ver-hotsales')}}">
              <a class="nav-link" href="{{url('hotsale/ver-hotsales')}}"><i class="fas fa-medal"></i>Hot sale</a>
            </li>
            <li class="nav-item navInt {{activeLinkInUrl('plan/ver-planes')}}">
              <a class="nav-link" href="{{url('plan/ver-planes')}}"><i class="fas fa-umbrella-beach"></i></i>Planes</a>
            </li>
            <li class="nav-item navInt {{activeLinkInUrl('reserva/mis-reservas')}}">
              <a class="nav-link" href="{{url("reserva/mis-reservas")}}"><i class="fa fa-calendar"></i>Reservas</a>
            </li>
            <li class="nav-item navInt ">
              <a class="nav-link" href="admin_users.html"><i class="fa fa-users"></i>Usuarios</a>
            </li>

            @elseif(currentUserHasRol("hotel"))

            <li class="nav-item navInt {{activeLinkInUrl('hotel/configuracion')}}">
              <a class="nav-link" href="{{url("hotel/configuracion")}}"><i class="fa fa-home"></i>Home</a>
            </li>

            <li class="nav-item navInt {{activeLinkInUrl('room/mis-habitaciones')}}">
              <a class="nav-link" href="{{url("/room/mis-habitaciones")}}"><i class="fa fa-bed"></i>Habitaciones</a>
            </li>
            <li class="nav-item navInt {{activeLinkInUrl('subasta/mis-subastas')}}">
              <a class="nav-link" href="{{url("/subasta/mis-subastas")}}"><i class="icon-martillo"></i>Subastas</a>
            </li>

            <li class="nav-item navInt {{activeLinkInUrl('reserva/mis-reservas')}}">
              <a class="nav-link" href="{{url("/reserva/mis-reservas")}}"><i class="fa fa-calendar"></i>Reservas</a>
            </li>
            @elseif(currentUserHasRol("client"))

            <li class="nav-item navInt {{activeLinkInUrl('user/configuracion')}}">
              <a class="nav-link" href="{{url('user/configuracion')}}"><i class="fa fa-home"></i>Inicio</a>
            </li>
            <li class="nav-item navInt {{activeLinkInUrl('reserva/mis-reservas')}}">
              <a class="nav-link" href="{{url("/reserva/mis-reservas")}}"><i class="fa fa-calendar"></i>Reservas</a>
            </li>
            <li class="nav-item navInt {{activeLinkInUrl('subasta/mis-subastas')}}">
              <a class="nav-link" href="{{url("/subasta/mis-subastas")}}"><i class="icon-martillo"></i>Subastas</a>
            </li>

            @endif
          </ul>
        </div>
      </div>
    </nav>


  </header>

  @yield('content')

  <footer>
    <script src="{{asset('app.js')}}"></script>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <p id="derechosTxt">Todos los derechos reservados ® - Subastas Turisticas 2020</p>
        </div>
      </div>
    </div>
  </footer>
</body>