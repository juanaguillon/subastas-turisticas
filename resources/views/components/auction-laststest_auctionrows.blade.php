@foreach ($auctions as $row)
@php
$statusText = "";
switch ($row->auction_status) {
case 1:
$statusText = "Inicializado";
break;
case 2:
$statusText = "En Proceso";
break;
case 3:
$statusText = "Finalizada";
break;
case 4:
$statusText = "Aceptada";
break;
case 5:
$statusText = "Rechazada";
break;
default:
$statusText = "Sin Estado";
break;
}
@endphp
<tr class="odd">
  <td>{{$row->auctionrow_id}}</td>
  <td>{{$row->city_name}}</td>
  <td>{{$row->auction_days}}</td>
  <td>{{formatPrice($row->auction_maxval, "COP")}}</td>
  <td>{{formatPrice($row->auctionrow_price, "COP")}}</td>
  <td>{{formatDate($row->auctionrow_updated, "COP")}}</td>
  <td>{{$statusText}}</td>
  <td class="">
    <a class="view mr-2" title="Ver" href="{{url("/auction/gestionar-fila") . "/" . $row->auctionrow_id}}">
      <i class="fa fa-eye"></i>
    </a>
  </td>
</tr>
@endforeach