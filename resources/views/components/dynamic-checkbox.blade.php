<div class="row prop_list_wrap_{{$uid}}">

  @foreach ($mainArray as $mak => $mav)
  <div class="col-md-6 single_col_{{$uid}}">
    <div class="checkbox_custom">
      <div class="checkbox_wrap">

        @if (isset($otherArray) && !empty($otherArray) && in_array($mav, $otherArray))
        <input id="{{$uid}}_{{$mak}}" checked name="{{$uid}}[]" type="checkbox" class="form-check-input"
          value="{{$mav}}">
        @else
        <input id="{{$uid}}_{{$mak}}" name="{{$uid}}[]" type="checkbox" class="form-check-input" value="{{$mav}}">
        @endif

        <i class="fa fa-check"></i>
      </div>
      <label class="ml-2" for="{{$uid}}_{{$mak}}">{{$mav}}</label>
    </div>
  </div>
  @endforeach

  @if (isset($otherArray) && !empty($otherArray))
  @foreach ($otherArray as $oak => $oav)
  @if (in_array($oav, $mainArray))
  @continue
  @endif
  <div class="col-md-6 single_col_{{$uid}}">
    <div class="checkbox_custom">
      <div class="checkbox_wrap">
        <input id="{{$uid}}_add_{{$oak}}" checked name="{{$uid}}[]" type="checkbox" class="form-check-input"
          value="{{$oav}}">
        <i class="fa fa-check"></i>
      </div>
      <label class="ml-2" for="{{$uid}}_add_{{$oak}}">{{$oav}}</label>
    </div>
  </div>
  @endforeach
  @endif

</div>
<div class="row">
  <div class="col-md-6">
    <div class="checkbox_custom" id="checbkox_hab">
      <div class="checkbox_wrap">
        <input id="label_i{{$uid}}" type="checkbox" class="form-check-input">
        <i class="fa fa-check"></i>
      </div>
      <label class="ml-2" for="label_i{{$uid}}">Otro ítem</label>
    </div>
    <div class="input_{{$uid}}_wrap other_proper">
      <input placeholder="¿Qué ítem?" type="text" id="add_itext_{{$uid}}" class="form-control other_proper_text">
      <div class="mt-1">
        <button id="add_i_ibtn_{{$uid}}" class="button m-0 button_blue other_proper_button button_small">
          <i class="fa fa-plus"></i>
        </button>
        <button id="cancel_ibtn_{{$uid}}" class="button m-0 button_red other_proper_button button_small">
          <i class="fa fa-ban"></i>
        </button>
      </div>
    </div>
  </div>
</div>