<div class="destacado-item">
  <div class="destacado-label">
    <img src="{{url('/images/subastas-turisticas-label-naranja.svg')}}" alt="">
  </div>
  <a href="{{url("/hotsale/ver-hotsale") . "/" . $hotsale->hotsale_id}}">
    <div class="destacado-caja">
      <div class="destacado-imagen">
        <img src="{{url("/storage/hotsale") . "/" . json_decode($hotsale->hotsale_images, true)[0]["350"] }}" alt="">
      </div>
      <div class="destacado-texto">
        <h4>Plan todo <span class="b8">incluido</span></h4>
        <div class="linea2"></div>
        <h2 class="destacado-ciudad">{{$hotsale->city_name}}</h2>
        <div class="destacado-html-price">
          <span class="destacado-normal">{{formatPrice($hotsale->hotsale_price)}}</span>
          <span
            class="destacado-hotted">{{formatPrice(getDiscountPrice($hotsale->hotsale_price,$hotsale->hotsale_discount), $hotsale->hotsale_currency)}}</span>
        </div>
        <div class="destacado-precio">{{$hotsale->hotsale_discount}}%</div>
      </div>
    </div>
  </a>
</div>