{{-- Estos datos se mostrarán en el momento que se esté haciendo la subasta. Estos datos los verán el cliente que está haciendo la subasta en el "Site" o el hotel cuando quiera hacer una nueva oferta. --}}
@if ( isset($loopData) )
@foreach ($loopData as $data)
<tr id="r_{{$data->auctionrow_id}}" class="priceMin">
  <td>{{formatTime($data->auctionrow_time)}}</td>
  <td id='tdhotel_hotel_{{$data->hotel_id}}'>{{$data->hotel_name}}</td>
  <td id='tdhotel_room_{{$data->room_id}}'>{{$data->room_name}}</td>
  <td>{{formatPrice($data->auctionrow_price)}}</td>
  <td>
    <a href="#" data-toggle="tooltip" data-placement="left" data-html="true" title="" data-original-title="">
      <span class="add_services">0</span>
    </a>
  </td>
  <td class="ver_mas">
    <a href="{{url("/auctionrow/ver-fila-oferta/{$data->auctionrow_id}") }}" target="blank">
      <i class="fas fa-external-link-alt"></i>
    </a>
  </td>
</tr>
@endforeach
@elseif( $data )
<tr id="r_{{$data->auctionrow_id}}" class="priceMin">
  <td>{{formatTime($data->auctionrow_time)}}</td>
  <td id='tdhotel_hotel_{{$data->hotel_id}}'>{{$data->hotel_name}}</td>
  <td id='tdhotel_room_{{$data->room_id}}'>{{$data->room_name}}</td>
  <td>{{formatPrice($data->auctionrow_price)}}</td>
  <td>
    <a href="#" data-toggle="tooltip" data-placement="left" data-html="true" title="" data-original-title="">
      <span class="add_services">0</span>
    </a>
  </td>
  <td class="ver_mas">
    <a href="{{url("/auctionrow/ver-fila-oferta/{$data->auctionrow_id}") }}" target="blank">
      <i class="fas fa-external-link-alt"></i>
    </a>
  </td>
</tr>
@endif