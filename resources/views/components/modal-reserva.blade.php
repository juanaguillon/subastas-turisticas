@php
$nombreCliente = session()->get("user_name");
$emailCLiente = session()->get("user_email");
@endphp

<div class="modal" id="modal_reserve">
  <div class="modal_wrap">
    <div class="modal_close">
      <button class="button button_blue no_hover button_small">
        <i class="fa fa-times"></i>
      </button>
    </div>
    <div class="modal_content">
      <div class="logo_reserva">
        <img src="{{url("/images/logo-subastas-turisticas.png")}}" alt="">
      </div>
      <div class="type_person">
        <!-- <h3>¿Que tipo de usuario es usted?</h3> -->
        <ul class="type_person_list">
          <li data-tab_reserva="natural" class="change_reserv_tab button button_white no_hover tab active button_small">
            Soy una persona</li>
          <li data-tab_reserva="company" class="change_reserv_tab button button_white no_hover tab button_small">Soy una
            empresa</li>
        </ul>
      </div>

      <form id="reservation-create-form" method="post"
        data-action="https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu">
        <div class="form_wrap_reservation">
          <input type="hidden" id="reservation_token" value="{{csrf_token()}}">

          <input type="hidden" id="reservation_caseid" value='{{$plan_id}}'>
          <input type="hidden" id="realreserv_type" name="reservation_type" value="{{$plantype}}">
          <input type="hidden" id="realreserv_doctype" name="reservation_doctype" value="cc">
          <input type="hidden" id="realreserv_docnumber" name="reservation_docnumber">
          <input type="hidden" id="type_of_client" value="natural">

          <input type="hidden" name='description' id='reserv_description'>



          <input name="merchantId" id='merchant_id' type="hidden">
          <input name="amount" id='reservation_amount' type="hidden">
          <input name="accountId" id='account_id' type="hidden">
          <input name="referenceCode" id='reference_code' type="hidden">
          <input name="currency" id='reservation_currency' type="hidden">
          <input name="signature" id='reservation_sign' type="hidden">

          <input name="test" type="hidden" value="1">
          <input name="responseUrl" type="hidden" value="http://intuitionagencia.com/example/index.php">
          <input name="confirmationUrl" type="hidden" value="http://intuitionagencia.com/example/example.php">

          <div class="row">

            <div class="col-md-6 only_in_natural">
              <div class="form-group">
                <label for="modal_type_document">Tipo de Documento</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-credit-card"></i></div>
                  </div>
                  <select id="modal_type_document" class="custom-select">
                    <option value="cc">Cédula de Ciudadanía</option>
                    <option value="ci">Cedula de Identidad</option>
                    <option value="ce">Cedula extranjera</option>
                  </select>
                  <small id="modal_type_document_error" class="form-text text-danger"></small>
                </div>

              </div>
            </div>

            <div class="col-md-6 only_in_natural">
              <div class="form-group">
                <label for="modal_document">Número de Documento</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-address-card"></i></div>
                  </div>
                  <input class="form-control" id="modal_document" type="number" value="">
                  <small id="modal_document_error" class="form-text text-danger"></small>
                </div>

              </div>
            </div>
            <div class="col-md-6 only_in_company field_hidden">
              <div class="form-group">
                <label for="modal_nit">NIT.</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-address-card"></i></div>
                  </div>
                  <input class="form-control" id="modal_nit" type="number" value="">
                  <small id="modal_document_error" class="form-text text-danger"></small>
                </div>

              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="modal_name" class="only_in_natural">Nombres y apellidos</label>
                <label for="modal_name" class="only_in_company field_hidden">Nombre de empresa</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-user"></i></div>
                  </div>
                  @if ($nombreCliente)
                  <input class="form-control" value="{{$nombreCliente}}" readonly id="reservation_username"
                    name="buyerFullName" type="text" value="">
                  @else
                  <input class="form-control" value="" id="reservation_username" name="buyerFullName" type="text"
                    value="">
                  @endif

                  <small id="modal_name_error" class="form-text text-danger"></small>
                </div>

              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="modal_email">Email</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-envelope"></i></div>
                  </div>
                  @if ($emailCLiente)
                  <input class="form-control" readonly name="buyerEmail" id="reservation_useremail" type="email"
                    value="{{$emailCLiente}}">
                  @else
                  <input class="form-control" name="buyerEmail" id="reservation_useremail" type="email" value="">
                  @endif

                  <small id="modal_email_error" class="form-text text-danger"></small>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="modal_phone">Teléfono</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-phone"></i></div>
                  </div>
                  <input class="form-control" name="reservation_phone" id="reservation_phone" type="number" value="">
                  <small id="modal_phone_error" class="form-text text-danger"></small>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="modal_city_name">Ciudad</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-building"></i></div>
                  </div>
                  <input class="form-control" name="reservation_city" id="reservation_city" type="text" value="">
                  <small id="modal_city_name_error" class="form-text text-danger"></small>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="modal_address">Dirección</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
                  </div>
                  <input class="form-control" name="reservation_address" id="reservation_address" type="text" value="">
                </div>
              </div>
            </div>
          </div>

          <div class="text-center d-flex align-items-center flex-column justify-content-center">
            <span class="modal_terms">Al reservar como cliente usted <strong>ACEPTA</strong> de manera expresa, libre,
              voluntaria y autoriza a <strong>SUBASTAS TURÍSTICAS</strong> para realizar el tratamiento de sus datos
              personales, especialmente los relativos a su identificación personal, nombres y apellidos y cédula,
              ciudad, dirección de residencia, y email.</span>
            <small id="modal_address_error" class="form-text text-danger"></small>

            <button type="submit" id="reservar_plan" class="button button_blue no_hover">RESERVAR</button>
          </div>

        </div>
      </form>
    </div>
  </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">Ha ocurrido un error al reservar, intente nuevamente.</div>
@endif