<div class="row">
  <input type="hidden" id='auctionrow_init' value='{{$auction->auction_id}}'>
  {{-- auction_maxhotels verificará cuantos hoteles en total se estarán subastando. --}}
  {{-- Esto ayudará a obtener diferentes ID de hotel en cada auctionRow --}}
  <input type="hidden" id='auctionrow_maxhotels' value='{{$auction->auction_hotelscount}}'>

  {{-- auction_lasthotel Verificará cuál fue el último hotel que se procesó y obtener un hotel distinto para subastar --}}
  <input type="hidden" id="auctionrow_lastnum" value="0">
  {{-- auction_first Servirá para mostrar "Un hotel está subastando en este momento", este valor no lo usará el backend. --}}
  <input type="hidden" id="auction_first" value="1">
  <input type="hidden" id="auction_timestamp" value="150">

  <input type="hidden" id='auction_token' value='{{csrf_token()}}'>

  <div class="col-xs-12 col-sm-12 col-md-8">
    <table class="table tablagen table-striped">
      <tbody>
        <tr>
          <td class="titleTable">Destino</td>
          <td class="descTable">{{$city->city_name}}</td>
        </tr>
        <tr>
          <td class="titleTable">Fecha de inicio</td>
          <td class="descTable">{{$auction->auction_init}}</td>
        </tr>
        <tr>
          <td class="titleTable">Fecha de regreso</td>
          <td class="descTable">{{$auction->auction_end}}</td>
        </tr>
        <tr>
          <td class="titleTable">Máx. Coste (Por noche)</td>
          <td class="descTable">{{ formatPrice($auction->auction_maxval) }}</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-4">
    <table class="table tablagen table-striped">
      <tbody>
        <tr>
          <td class="titleTable">Días</td>
          <td class="descTable">{{$auction->auction_days}}</td>
        </tr>
        <tr>
          <td class="titleTable">Adultos</td>
          <td class="descTable">{{$auction->auction_adults}}</td>
        </tr>
        <tr>
          <td class="titleTable">Niños</td>
          <td class="descTable">{{$auction->auction_childrens != '' ? $auction->auction_childrens : '0' }}</td>
        </tr>
        <tr>
          <td class="titleTable">Infantes</td>
          <td class="descTable">{{$auction->auction_infants != '' ? $auction->auction_infants : '0' }}</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-12">
    <p class="text-info" id="subasta_text">Inicializando Subasta ...</p>
    <span id="subasta_timer" class="d-none">2'32''</span>
  </div>
</div>

<table id="bid_table" class="table tablagen odd" style="">
  <thead>
    <tr>
      <th>Tiempo</th>
      <th>Hotel</th>
      <th>Habitación</th>
      <th>Valor</th>
      <th>Obsequios</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody id="subasta_response_rows" class="d-none">

  </tbody>
</table>