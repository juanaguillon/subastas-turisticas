@extends('layout-admin')
@section('title', "Todos los Planes")
@section('body_id',"all_planes")
@section('content')

{{-- {{printcode($cities)}}
{{printcode($hotels)}}
{{printcode($status)}} --}}

<section id="index">
  <div id="content">
    <div class="container">

      <div class="row search-form">
        <div class="col-md-12 subastas_items">
          <h3 class="admin_title">Todos los Planes</h3>

          <div id="filtering_wrap_abs">
            <div class="form-group">
              <select class="select_principal show" id="subastas_filtro">
                <option value="Selecciones una filtro" disabled selected>Seleccione un filtro</option>
                <option value="subastas_city">Ciudad</option>
                <option value="subastas_hotel">Hotel</option>
                <option value="subastas_status">Estado</option>
              </select>
            </div>

            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_city" data-name="plan_city">
                <option value="none">Todos las ciudades</option>
                @foreach ($cities as $city)
                <option data-count="{{$city["count"]}}" value="{{$city['city_id']}}">
                  {{$city['city_name']}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_hotel" data-name="plan_hotel">
                <option value="none">Todos los hoteles</option>
                @foreach ($hotels as $hotel)
                <option data-count="{{$hotel["count"]}}" value="{{$hotel['hotel_id']}}">
                  {{$hotel['hotel_name'] }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_status" data-name="plan_active">
                <option value="none">Todos los estados</option>
                @foreach ($status as $stat)
                <option data-count="{{$stat["count"]}}" value="{{$stat['plan_active']}}">
                  {{$stat['plan_active'] == 1 ? 'Activo' : 'Inactivo'}}</option>
                @endforeach
              </select>
            </div>
            <button type="submit" class="button button_red no_hover" id="selects_filter_paginator">FILTRAR</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12" id="auction-grid">
          <div class="tablegen-loading">
            <div class="loading">
              <div class="lds-default">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
          <table class="table tablagen odd">
            <thead>
              <tr>
                <th class="order_column_th" data-oby="plan_name">Nombre</th>
                <th class="order_column_th" data-oby="city_name">Ciudad</th>
                <th class="order_column_th" data-oby="hotel_name">Hotel</th>
                <th class="order_column_th" data-oby="plan_price">Precio</th>
                <th class="order_column_th" data-oby="plan_created">Fecha</th>
                <th class="order_column_th" data-oby="plan_active">Estado</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody id="tbody_results">

            </tbody>

          </table>
          <input type="hidden" id="start_paginator" value="0">
          <input type="hidden" id="limit_paginator" value="10">
          <input type="hidden" id="url_paginator" value="{{url("/plan/paginador")}}">
          <input type="hidden" id="total_paginator" value="{{count($planes)}}">

          <div class="pagination_wrap">
            <select name="" class="select_principal select_paginator" id="quantity_screen_pag">
              <option value="1">1</option>
              <option value="5">5</option>
              <option value="10" selected>10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
            <div id="paginator_container">
            </div>
          </div>

          @if (currentUserHasRol("admin"))
          <div class="button_action_wrap mt-3">
            <a href="{{url("/plan/create")}}" class="button button_blue m-0">Crear Plan</a>
          </div>
          @endif

        </div>
      </div>


    </div>
  </div><!-- content -->
</section>
@endsection