@extends('layout-site')
@section("body_id","body_ver_plan")
@section('content')

@php

$planImages = json_decode($plan->plan_images, true);
$planImagesPath = url("/storage/plan") . "/";

$hotelImagesPath = url("/storage/hotel") . "/";
$hotelImages = json_decode($plan->hotel_images, true);

$roomImages = json_decode($plan->room_images, true);
$roomImagesPath = url("/storage/habitacion") . "/";
@endphp

@component('components.modal-reserva', array(
"plantype" => "plan",
'plan_id' => $plan->plan_id ,
))

@endcomponent

<section id="galeriadest">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 m-0 p-0">
        <div class="camera_wrap" id="bannerimages">
          @if ($planImages && count($planImages) >0)
          @foreach ($planImages as $img)
          <div data-thumb="{{$planImagesPath . $img["xl"]}}" data-src="{{$planImagesPath . $img["xl"]}}">
            <div class="nombreHabitacion fadeIn">
              <div class="nombrehabitacion_icono"><i class="icon-martillo"></i></div>
              <h3>{{$plan->city_name}}</h3>
            </div>
          </div>
          @endforeach
          @else
          <div data-thumb="{{url("/images/subastas-turisticas-hotsale-background-min.jpg")}}"
            data-src="{{url("/images/subastas-turisticas-hotsale-background-min.jpg")}}">
            <div class="nombreHabitacion fadeIn">
              <div class="nombrehabitacion_icono"><i class="icon-martillo"></i></div>
              <h3>{{$plan->city_name}}</h3>
            </div>
          </div>
          <div data-thumb="{{url("/images/subastas-turisticas-plan-background-min.jpg")}}"
            data-src="{{url("/images/subastas-turisticas-plan-background-min.jpg")}}">
            <div class="nombreHabitacion fadeIn">
              <div class="nombrehabitacion_icono"><i class="icon-martillo"></i></div>
              <h3>{{$plan->city_name}}</h3>
            </div>
          </div>
          @endif

        </div>
      </div>
    </div>
  </div>
</section>

<section id="destacados" class="plan_inner">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-8">
        <div class="titulohabitacion">
          <h3 class="bold">{{$plan->hotel_name}}</h3>
        </div>
        <div class="fotosProducto">
          @if (count($hotelImages) > 0)
          <div class="foto_producto_wrap">
            <button class="button_slide_product button_left"><i class="fa fa-angle-left"></i></button>
            <ul id="foto_producto_list">

              @foreach ($hotelImages as $ki => $img)
              <li id="foto0{{$ki}}">
                <a href="{{$hotelImagesPath . $img["765"]}}">
                  <img src="{{$hotelImagesPath . $img["765"]}}">
                </a>
              </li>
              @endforeach
            </ul>
            <button class="button_slide_product button_right"><i class="fa fa-angle-right"></i></button>
          </div>
          <div class="fotos_producto_wrap">
            <ul id="fotos_producto_list">
              @foreach ($hotelImages as $ki => $img)
              <div class="foto_wrap">
                <li>
                  <img src="{{$hotelImagesPath . $img["350"]}}">
                </li>
              </div>
              @endforeach
            </ul>
          </div>
          @else
          <div class="message_info">
            <p>Este hotel no tiene imágenes disponibles.</p>
          </div>
          @endif
        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <div class="titulohabitacion">
          <h3 class="bold">{{$plan->plan_name}}</h3>
        </div>
        <div class="infoPlanes">
          <div class="row no-gutters">
            <div class="col-12 column_reserv">
              <div class="infoprice">
                <div class="label"><label class="label">Destino:</label></div>
                <input type="text" class="form-control" id="where" name="name" value="{{$plan->city_name}}" disabled>
              </div>
            </div>
            @if ($plan->plan_description)


            <div class="col-12 column_reserv">
              <div class="infoprice">
                <div class="label"><label class="label">Descripción del plan:</label></div>
                <div class="descripcion_plan text-white">{!! $plan->plan_description !!}</div>
              </div>
            </div>
            @endif

            <div class="col-12 column_reserv">
              <div class="infoprice inf-iz-plan">
                <div class="pricetitle"><label class="label">Por solo:</label></div>
                <div class="price">{{formatPrice($plan->plan_price, "")}}<span size="1"
                    class="cop-plan">{{$plan->plan_currency}}</span>
                </div>
              </div>
            </div>
            <div class="col-12 column_reserv">
              <form id="ajax-contact-form2" class="form-horizontal" method="post">
                <button id="show_reserva_modal" type="submit" class="button button_red no_hover">RESERVAR</button>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="row mt-3">
      <div class="col-md-12 col-lg-8">
        <div class="servhabitacion">
          <h3 class="titleserv">Detalles del Plan</h3>

          <div class="liservice">
            <ul class="tabs">
              <li class="tab-link current" data-tab="tab-0"><span>Plan</span></li>
              <li class="tab-link" data-tab="tab-1"><span>Hotel</span></li>
              <li class="tab-link" data-tab="tab-2"><span>Habitación</span></li>
              <li class="tab-link" data-tab="tab-3"><span>Términos y condiciones</span></li>
            </ul>
            <div id="tab-0" class="tab-content current">

              @if ($hotDesc = $plan->plan_description)
              <div class="subseccion">
                <h4><i class="icono fas fa-info-circle"></i>Información del Plan</h4>
                {!!$hotDesc !!}
              </div>
              <div class="linea3"></div>
              @endif
              @if ($plan->plan_services && count(json_decode($plan->plan_services, true)) > 0 )
              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fa fa-bookmark"></i>Servicios Adicionales</h4>
                <div class="row no-gutters">
                  @foreach (json_decode($plan->plan_services, true) as $service)
                  <div class="col-md-6">
                    <span class="list_checkbox">
                      <i class="fas fa-check-square fa-lg"></i>
                      <span>{{$service}}</span>
                    </span>
                  </div>
                  @endforeach
                </div>
              </div>
              @endif
            </div>
            <div id="tab-1" class="tab-content">

              <div class="subseccion">
                <h4><i class="icono fa fa-building"></i>Hotel</h4>

                {{-- DESCRIPCION DE HOTEL --}}
                @if ($plan->hotel_description)
                {!!$plan->hotel_description!!}
                @endif

                <h4><i class="icono fa fa-phone"></i>Teléfonos</h4>
                <ul class="phones_views">
                  <li>{{$plan->hotel_phone}}</li>
                </ul>

              </div>
            </div>

            <div id="tab-2" class="tab-content">
              <div class="subseccion">
                @if ($plan->room_description)
                <h4><i class="icono fa fa-list-alt"></i>Descripción de la habitación</h4>
                {!! $plan->room_description !!}
                @endif

                <h6>Tamaño de la habitación:</h6>
                <span>{{$plan->room_meters}} m2</span>
                {{-- <p>Suite con vistas a la ciudad, TV de pantalla plana por cable, soporte para iPod, zona de estar con
                  sofá, y baño privado con ducha. Está insonorizada, e incluye acceso executive al salón.</p> --}}
              </div>
              <div class="linea3 mt-4 mb-4"></div>


              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fas fa-clone"></i>Imágenes de la habitación</h4>
                @if ($roomImages)
                <div class="slider_images_room_wrap">
                  <button class="button_slide_room button_left"><i class="fa fa-angle-left"></i></button>
                  <ul id="slider_images_room">
                    @foreach ($roomImages as $kimg => $img)
                    <li id="foto{{$kimg}}"><img data-lazy="{{$roomImagesPath . $img["765"]}}"></li>
                    @endforeach
                  </ul>
                  <button class="button_slide_room button_right"><i class="fa fa-angle-right"></i></button>
                </div>

                @else
                <p>Esta habitación no posee imágenes.</p>
                @endif


              </div>
              <div class="linea3 mt-4 mb-4"></div>
              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fa fa-bookmark"></i>Servicios Adicionales</h4>
                <div class="row no-gutters">
                  @if ($plan->room_services && count(json_decode($plan->room_services)) > 0 )
                  @foreach (json_decode($plan->room_services) as $service)
                  <div class="col-md-6">
                    <span class="list_checkbox">
                      <i class="fas fa-check-square fa-lg"></i>
                      <span>{{$service}}</span>
                    </span>
                  </div>
                  @endforeach
                  @else
                  <p>No se han encontrado servicios de la habitación.</p>
                  @endif
                </div>

              </div>
              <div class="linea3 mt-4 mb-4"></div>
              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fa fa-suitcase"></i>Equipamento de la habitación</h4>
                <div class="row">
                  @if ($plan->room_equipament && count(json_decode($plan->room_equipament)) > 0 )
                  @foreach (json_decode($plan->room_equipament) as $wquip)
                  <div class="col-md-6">
                    <span class="list_checkbox">
                      <i class="fas fa-check-square fa-lg"></i>
                      <span>{{$wquip}}</span>
                    </span>
                  </div>
                  @endforeach
                  @else
                  <p>No se han encontrado equipamiento de la habitación.</p>
                  @endif
                </div>
              </div>
              <div class="linea3 mt-4 mb-4"></div>
              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fa fa-flag-checkered"></i>Condiciones y restricciones</h4>
                @if ($plan->room_terms)
                {!! $plan->room_terms !!}
                @else
                <p>No se ha agregado condiciones y restricciones de la habitación.</p>
                @endif
              </div>
            </div>
            <div id="tab-3" class="tab-content">
              <div class="subseccion">
                <h4><i class="icono fa fa-gavel"></i>Términos y condiciones del Plan</h4>
                @if ($plan->plan_terms)
                {!! $plan->plan_terms !!}
                @else
                <p>No se ha agregado condiciones y restricciones de la habitación.</p>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <div class="col-md-12 m-0 p-0">
          <div class="allservice">
            <h4>Servicios del hotel:</h4>
            @if ($plan->hotel_services && count(json_decode($plan->hotel_services)) > 0)
            <ul>
              @foreach (json_decode($plan->hotel_services, true) as $service)
              <li><span class="hotel_service">{{$service}}</span></li>
              @endforeach
            </ul>
            @else
            <p>No se ha registrado servicios de hotel.</p>
            @endif

          </div>
        </div>

      </div>
      <div class="col-md-8 my-3">
        <div class="allservice">
          <h4>Condiciones y restricciones del hotel:</h4>

          @if ($terms = $plan->hotel_terms && $terms !== "" )
          {!! $plan->hotel_terms !!}
          @else
          <p>No se ha registrado condiciones de hotel.</p>
          @endif
          <p></p>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection