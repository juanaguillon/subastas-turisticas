@extends('layout-admin')
@section('title', $updating ? 'Actualizar Plan' : 'Crear Plan')
@section('content')

@php
if ( $updating){
$planImages = json_decode($plan->plan_images, true);
}
@endphp

<section class="nuevos" id="index">
  <div class="container">
    <form enctype="multipart/form-data" id="create-plan-form"
      action="{{$updating ? url("plan/editar-plan/{$plan->plan_id}") :url('plan/create')}}" method="post">
      @csrf
      <div class="box">
        @if (isset($_GET['new_reg']))
        <div class="alert alert-success">
          @if ($updating)
          Plan actualizado correctamente
          @else
          Plan creado correctamente
          @endif
        </div>
        @elseif($errors->any())
        <div class="alert alert-danger">
          {{$errors->first()}}
        </div>
        @endif

        <div id='create_plan_danger' class="alert alert-danger form_alert_toggle"></div>

        <h3 class="titleServiciosNM">{{$updating ?"Editar plan" :"Crear nuevo Plan"}}</h3>
        <div class="row">
          <div class="col-12">
            <h5 class="titleServiciosNM mb-2">Aspectos básicos</h5>
            <p class="titleHabitaciones">Los campos con <span style="color:red;">*</span> son obligatorios</p>
            <div class="form-group">
              <label class="label_principal">Imágenes de Plan</label>
              <div class="hotel_images">
                <ul id='hotel_selectize_imgul'>

                  @if ($updating)
                  @foreach ( $planImages as $ik => $img)
                  <li data-index="{{$ik}}" class="hotel_image_item dyn_img_item">
                    <div class="imgHotel">
                      <img src="{{url("/storage/plan") . "/" . $img["350"]}}" alt="foto">
                      <button class="button button_remove_img button_red button_small">
                        <i class="fa fa-times"></i>
                      </button>
                    </div>
                  </li>
                  @endforeach
                  @endif

                  <li class="hotel_image_item hotel_image_new">
                    <div class="imgHotel">
                      <i class="fa fa-plus"></i>
                      <input name='plan_images[]' id='hotel_selectize_img' multiple type="file"
                        class="custom-file-input nuevaImagen" accept="image/*" max="5">
                      <input name='plan_indexes_imgs' type="hidden" id='hotel_selectize_imgindx'>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

            <div class="form-row">
              <div class="col-md-6 mb-3">
                <label for='plan_name' class='label_principal required'>Nombre plan <span
                    class="required">*</span></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-form-1"></i></span>
                  <input maxlength="50" value="{{$plan->plan_name ?? ''}}" class="form-control"
                    placeholder="Nombre del plan" name='plan_name' id='plan_name' type="text" /> </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="plan_city" class='label_principal required'>Ciudad <span class="required">*</span></label>
                <div class="input-group position-relative">
                  <input class="form-control" value="{{$plan->city_name ?? ''}}" autocomplete='new-password'
                    placeholder="Digite la ciudad" type="text" id='plan_city' />
                  <input type="hidden" value="{{$plan->city_id ?? ''}}" name="plan_city" id="plan_city_real_val">
                  <div id='plan_cities_results' class="list-group autocomplete_lists">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-3">
                <label for="plan_hotel" class="required label_principal">Hotel <span class="required">*</span></label>
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="icon-admin-menu"></i>
                  </span>
                  <select {{!$updating ? "disabled" : "" }} class="select_principal" name="plan_hotel" id="plan_hotel">
                    @if ($updating)
                    <option selected value="{{$plan->hotel_id}}">{{$plan->hotel_name}}</option>
                    @endif
                  </select>
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="plan_room" class='label_principal'>Habitación</label>
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="icon-logo-3"></i>
                  </span>
                  <select {{!$updating ? "disabled" : "" }} class="select_principal" name="plan_room" id="plan_room">
                    @if ($updating)
                    <option selected value="{{$plan->room_id}}">{{$plan->room_name}}</option>
                    @endif
                  </select>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-3">
                <label for="plan_rooms_quantity" class='label_principal required'>Cantidad de Habitaciones <span
                    class="required">*</span></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-logo-3"></i></span>
                  <select class="select_principal" name="plan_rooms_quantity" id="plan_rooms_quantity">
                    <option value="none">Seleccione</option>
                    @for ($i = 1; $i <= 10 ; $i++) @if ($updating && $plan->plan_rooms_quantity == $i)
                      <option selected value="{{$i}}">{{$i}}</option>
                      @else
                      <option value="{{$i}}">{{$i}}</option>
                      @endif
                      @endfor
                  </select> </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="plan_price" class='label_principal required'>Precio del plan <span
                    class="required">*</span></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-icono-presupuestoD"></i></span>
                  <input value='{{$plan->plan_price ?? ''}}' class="form-control" placeholder="Precio por noche"
                    name="plan_price" id="plan_price" type="text" /> </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="plan_currency" class='label_principal required'>Seleccionar Moneda<span
                    class="required">*</span></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-money"></i></span>
                  <select class="select_principal" name="plan_currency" id="plan_currency">
                    <option {{$updating && $plan->plan_currency == 'COP' ? 'selected' : ''}} value="COP"
                      selected="selected">COP</option>
                    <option {{$updating && $plan->plan_currency == 'USD' ? 'selected' : ''}} value="USD">USD</option>
                  </select> </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-3">
                <label for="plan_mindate" class='label_principal required'>Fecha inicio <span class="required">*</span>
                </label>
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="icon-icono-diasD"></i>
                  </span>
                  <input value="{{$updating ? formatDate($plan->plan_mindate) : ""}}" class="form-control"
                    placeholder="# Dias" readonly="readonly" name="plan_mindate" id="plan_mindate" type="text" /> </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="plan_maxdate" class='label_principal required'>Fecha fin <span
                    class="required">*</span></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-icono-diasD"></i></span>
                  <input value="{{$updating ? formatDate($plan->plan_maxdate) : ""}}" class="form-control dia_llegada"
                    placeholder="# Dias" readonly="readonly" name="plan_maxdate" id="plan_maxdate" type="text" /> </div>
              </div>
            </div>

            <div class="form-row">

              <div class="col-md-6">
                <label class='label_principal' for="Plan_active">Activo</label>
                <div class="radio_custom">
                  <div class="radio_wrap">
                    @if ($updating)
                    <input id="plan_active_1" value="1" {{$plan->plan_active == 1 ? 'checked' : ''}} type="radio"
                      name="plan_active">
                    @else
                    <input id="plan_active_1" value="1" checked type="radio" name="plan_active">
                    @endif

                    <div class="radio_circle"></div>
                  </div>
                  <label for="plan_active_1">SI</label>
                </div>
                <div class="radio_custom">
                  <div class="radio_wrap">
                    @if ($updating)
                    <input id="plan_active_0" value="0" {{$plan->plan_active == 0 ? 'checked' : ''}} type="radio"
                      name="plan_active">
                    @else
                    <input id="plan_active_0" value="0" type="radio" name="plan_active">
                    @endif

                    <div class="radio_circle"></div>
                  </div>
                  <label for="plan_active_0">NO</label>
                </div>
              </div>

              <div class="col-md-6 banner_activo">
                <label class='label_principal' for="Plan_active">Banner activo</label>
                <div class="radio_custom">
                  <div class="radio_wrap">
                    @if ($updating)
                    <input id="plan_banner_active_1" value="1" {{$plan->plan_banner_active == 1 ? 'checked' : ''}}
                      type="radio" name="plan_banner_active">
                    @else
                    <input id="plan_banner_active_1" value="1" checked="" type="radio" name="plan_banner_active">
                    @endif
                    <div class="radio_circle"></div>
                  </div>
                  <label for="plan_banner_active_1">SI</label>
                </div>
                <div class="radio_custom">
                  <div class="radio_wrap">
                    @if ($updating)
                    <input id="plan_banner_active_0" value="0" {{$plan->plan_banner_active == 0 ? 'checked' : ''}}
                      type="radio" name="plan_banner_active">
                    @else
                    <input id="plan_banner_active_0" value="0" type="radio" name="plan_banner_active">
                    @endif

                    <div class="radio_circle"></div>
                  </div>
                  <label for="plan_banner_active_0">NO</label>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="box">
        <div class="row">
          <div class="col-12">
            <div class="form-row">
              <div class="col-md-12">
                <h5 class="titleServiciosNM mt-4">Plan - Info del header</h5>
                <label for="">Servicios especiales</label>
              </div>
              @if ($updating)
              @php
              $specialServices = json_decode($plan->plan_special_service, true);
              @endphp

              @foreach ($specialServices as $sk => $ss)
              <div class="col-md-4 mb-3">
                <div class="iconos-selec">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <input value="{{$ss['icon']}}" type="hidden" id='iconpicker_s{{$sk}}_icon'
                        name="plan_special_service[{{$sk}}][icon]">
                      <span class="input-group-text" id="iconpicker_s{{$sk}}_reflect">
                        <i class="{{$ss['icon']}}"></i>
                      </span>
                    </div>
                    <input class="form-control servicio_especial" maxlength="100" placeholder="Servicio Especial"
                      value='{{$ss['title']}}' name="plan_special_service[{{$sk}}][title]" id="plan_service{{$sk}}"
                      type="text" />
                  </div>
                </div>
                <div id="iconpicker_s{{$sk}}" data-iconset="fontawesome" data-search-text="Buscar..."></div>
              </div>
              @endforeach

              @else
              @for ($i = 0; $i < 3; $i++) <div class="col-md-4 mb-3">
                <div class="iconos-selec">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <input type="hidden" id='iconpicker_s{{$i}}_icon' name="plan_special_service[{{$i}}][icon]">
                      <span class="input-group-text" id="iconpicker_s{{$i}}_reflect">
                        -
                      </span>
                    </div>
                    <input class="form-control servicio_especial" maxlength="100" placeholder="Servicio Especial"
                      name="plan_special_service[{{$i}}][title]" id="plan_service{{$i}}" type="text" />
                  </div>
                </div>
                <div id="iconpicker_s{{$i}}" data-iconset="fontawesome" data-search-text="Buscar..."></div>
            </div>
            @endfor
            @endif


          </div>
        </div>

      </div>

      <div class="row">
        <div class="col-12">

          <div class="form-row mt-4">
            <div class="col-md-12 liservice">
              <h5 class="titleServiciosNM">Plan - Aspectos detallados</h5>
              <label for="Plan_title" class="label_principal">Promoción del plan</label>
              <div data-name='plan_promotion' class='ckeditor_initial' id="plan_prom">
                {!!$plan->plan_promotion ?? ''!!}
              </div>
              <div class="subseccion">
                <label for="Plan_title" class="label_principal">Descripción del plan</label>
                <div class='ckeditor_initial' data-name='plan_description' id="plan_description">
                  {!!$plan->plan_description ?? ''!!}
                </div>
              </div>

              <div class="aditionals_wrap mt-3">
                <label class="label_principal">Servicios Adicionales</label>
                <div class="row flex-column-reverse flex-md-row formHome formPerfil mb-2">
                  <div class="col-12" id="listservhb">
                    <div class="row prop_list_wrap_services">
                      @php
                      $services = ['Piscina','Bolos','Bar','Parqueadero'];
                      if ( $updating){
                      $savedServices = json_decode($plan->plan_services, true);
                      }
                      @endphp
                      @foreach ($services as $sk => $serv)
                      <div class="col-md-6 list_item_eqroom">
                        <div class="checkbox_custom">
                          <div class="checkbox_wrap">

                            @if ($updating)
                            <input {{in_array($serv, $savedServices) ? 'checked' : ''}} id="equipment_{{$sk}}"
                              name="plan_services[]" type="checkbox" class="form-check-input" value="{{$serv}}">
                            @else
                            <input id="equipment_{{$sk}}" name="plan_services[]" type="checkbox"
                              class="form-check-input" value="{{$serv}}">
                            @endif

                            <i class="fa fa-check"></i>
                          </div>
                          <label class="ml-2" for="equipment_{{$sk}}">{{$serv}}</label>
                        </div>
                      </div>
                      @endforeach
                      @if ($updating)

                      @foreach ($savedServices as $msk => $mservice)
                      @if (in_array($mservice, $services))
                      @continue
                      @endif
                      <div class="col-md-6 list_item_eqroom">
                        <div class="checkbox_custom">
                          <div class="checkbox_wrap">

                            <input checked id="equipment_add_{{$msk}}" name="plan_services[]" type="checkbox"
                              class="form-check-input" value="{{$mservice}}">
                            <i class="fa fa-check"></i>
                          </div>
                          <label class="ml-2" for="equipment_add_{{$msk}}">{{$mservice}}</label>
                        </div>
                      </div>
                      @endforeach
                          
                      @endif

                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="checkbox_custom" id="checbkox_hab">
                          <div class="checkbox_wrap">
                            <input id="service_add" type="checkbox" class="form-check-input">
                            <i class="fa fa-check"></i>
                          </div>
                          <label class="ml-2" for="service_add">Otro Servicio</label>
                        </div>
                        <div class="add_service_wrap other_proper">
                          <input placeholder="¿Qué Servicio?" type="text" id="add_service_text"
                            class="form-control other_proper_text">
                          <div class="mt-1">
                            <button id="add_service_button"
                              class="button m-0 button_blue other_proper_button button_small">
                              <i class="fa fa-plus"></i>
                            </button>
                            <button id="cancel_service_button"
                              class="button m-0 button_red other_proper_button button_small">
                              <i class="fa fa-ban"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <p><label class='label_principal' for="Plan_terms">Términos y condiciones</label></p>
              <div class="ckeditor_initial" id="plan_terms" data-name='plan_terms'>
                {!! $plan->plan_terms ?? '' !!}
              </div>
              <button type="submit" class="button button_blue mt-3">Guardar</button>
            </div>
          </div>

        </div>

      </div>

    </form>
  </div>
</section>

@endsection