@extends('layout-admin')
@section('title', $updating ? "Actualizar habitación" : "Crear Habitación")
@section('content')

<div id="index">
	<form enctype="multipart/form-data" id="room-form"
		action="{{url( $updating ? "room/editar-habitacion/{$room->room_id}" :'/room/create')}}" method="post">
		@csrf

		<div class="container mb-4 mt-4 oferta">
			<div class="box">
				@if (isset($_GET['new_reg']))
				<div class="alert alert-success">
					@if (isset($_GET['reg_updating']))
					Habitación actualizada correctamente
					@else
					Habitación creada correctamente
					@endif
				</div>
				@elseif($errors->any())
				<div class="alert alert-danger">
					{{$errors->first()}}
				</div>
				@endif

				<div id='create_room_danger' class="alert alert-danger form_alert_toggle"></div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						@if ($updating)
						<h5 class="titleServiciosNM">Actualizar Habitación</h5>
						@else
						<h5 class="titleServiciosNM">Crear nueva Habitación</h5>
						@endif
						<p class="titleHabitaciones">Los campos con <span style="color:red;">*</span> son obligatorios</p>
						<div></div>

						<div class="formHome" id="formHabitaciones">
							<!-- begin form -->

							<div class="form-group">
								<label>Imágenes de habitación</label>
								<div class="hotel_images">
									<ul id='hotel_selectize_imgul'>
										@if (isset($room))
										@if (count($room->room_images) > 0)
										@foreach ($room->room_images as $img)
										<li data-index="0" class="hotel_image_item dyn_img_item">
											<div class="imgHotel">
												<img src="{{url('/storage/habitacion') . '/' . $img['350']}}" alt="foto">
												<button class="button button_remove_img button_red button_small">
													<i class="fa fa-times"></i>
												</button>
											</div>
										</li>
										@endforeach
										@endif
										@endif

										<li class="hotel_image_item hotel_image_new">
											<div class="imgHotel">
												<i class="fa fa-plus"></i>
												<input name='room_images[]' id='hotel_selectize_img' multiple type="file"
													class="custom-file-input nuevaImagen" accept="image/*" max="5">
												<input name='room_indexes_imgs' type="hidden" id='hotel_selectize_imgindx'>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="active_wrap_room">
								<div class="active_cont_room">
									<label class="no_width">Activo</label>
									<div class="radio_custom">
										<div class="radio_wrap">
											@if (isset($room))
											<input id="room_active_1" value="1" {{ $room->room_active === 1 ? 'checked' : '' }} type="radio"
												name="room_active" />
											@else
											<input id="room_active_1" value="1" checked type="radio" name="room_active" />
											@endif
											<div class="radio_circle"></div>
										</div>
										<label for="room_active_1">SI</label>
									</div>
									<div class="radio_custom">
										<div class="radio_wrap">
											@if (isset($room))
											<input id="room_active_0" value="1" {{$room->room_active === 0 ? 'checked' : '' }} type="radio"
												name="room_active" />
											@else
											<input id="room_active_0" value="0" type="radio" name="room_active" />
											@endif

											<div class="radio_circle"></div>
										</div>
										<label for="room_active_0">NO</label>
									</div>
								</div>
							</div>
							<div class="form-row">

								<div class="col-md-6">
									<label for="room_name" class="required">Nombre <span class="required">*</span></label>
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-logo-3"></i></span>
										<input value="{{$room->room_name ?? ''}}" class="form-control" maxlength="150"
											placeholder="Nombre habitación" tabindex="1" name="room_name" id="room_name" type="text" />
									</div>
								</div>

								<div class="col-md-2">
									<label for="room_adults"># Adultos</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-form-1"></i></span>
										<select class="select_principal" tabindex="2" name="room_adults" id="room_adults">
											@for ($i = 1; $i <= 10; $i++) @if (isset($room) && $room->room_adults === $i)
												<option selected value="{{$i}}">{{$i}}</option>
												@else
												<option value="{{$i}}">{{$i}}</option>
												@endif
												@endfor
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<label for="room_childs"># Niños</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-form-1"></i></span>
										<select class="select_principal" tabindex="3" name="room_childs" id="room_childs">
											@for ($i = 0; $i <= 10; $i++) @if (isset($room) && $room->room_childs === $i)
												<option selected value="{{$i}}">{{$i}}</option>
												@else
												<option value="{{$i}}">{{$i}}</option>
												@endif
												@endfor
										</select>
									</div>
								</div>
								<div class="col-md-2 mb-4">
									<label for="room_babys"># Infantes</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-form-1"></i></span>
										<select class="select_principal" tabindex="4" name="room_babys" id="room_babys">
											@for ($i = 0; $i <= 10; $i++) @if (isset($room) && $room->room_babys === $i)
												<option selected value="{{$i}}">{{$i}}</option>
												@else
												<option value="{{$i}}">{{$i}}</option>
												@endif
												@endfor
										</select> </div>
								</div>
								<div class="col-md-12 mb-4">
									<div id="newkids" class="newkids"></div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-4 mb-4">
									<label for="room_minprice" class="required">Precio mínimo <span class="required">*</span></label>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="icon-icono-presupuestoD"></i>
										</span>
										<input value="{{$room->room_minprice ?? ''}}" class="form-control" placeholder="Precio habitación"
											tabindex="5" name="room_minprice" id="room_minprice" type="text" />
									</div>
								</div>


								<div class="col-md-4 mb-4">
									<div class="form-row mb-4">
										<label for="room_maxprice" class="required">Precio Máximo <span class="required">*</span></label>
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-icono-presupuestoD"></i></span>
											<input value="{{$room->room_maxprice ?? ''}}" class="form-control" placeholder="Precio habitación"
												tabindex="6" name="room_maxprice" id="room_maxprice" type="text" /> </div>
									</div>
								</div>
								<div class="col-md-4 mb-4">
									<label for="room_rooms"># Habitaciones</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-form-1"></i></span>
										<select class="select_principal" tabindex="8" name="room_rooms" id="room_rooms">
											@for ($i = 1; $i <= 10; $i++) @if (isset($room) && $room->room_rooms === $i)
												<option selected value="{{$i}}">{{$i}}</option>
												@else
												<option value="{{$i}}">{{$i}}</option>
												@endif
												@endfor

										</select>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container mt-4 mb-4">
			<div class="box">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<h5 class="titleServiciosNM">Información habitación</h5>
						<div class="liservice">

							<div class="form-row mt-4">
								<div class="col-md-4">
									<label for="room_meters" class="required">Tamaño habitación(m2)
										<span class="required">*</span>
									</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-logo-3"></i></span>
										<input value="{{$room->room_meters ?? ''}}" class="form-control" maxlength="150"
											placeholder="Tamaño habitación" type="number" tabindex="13" name="room_meters" id="room_meters" />
									</div>
								</div>
							</div>
							<label for="room_description">Descripción</label>
							<div class="ckeditor_initial" data-name='room_description' id='room_description'>
								{!! $room->room_description ?? '' !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="aditionals_wrap">
					<h5 class="titleServiciosNM">Equipamiento de la habitación</h5>
					@php
					$equipament = array(
					'Minibar',
					'Ducha',
					'Caja fuerte con capacidad para ordenador portátil',
					'Tv de pantalla plana',
					);
					$services = array(
					"Servicio adicional 1",
					"Servicio adicional 2"
					);
					$gifts = array(
					"Bolos",
					"Tour"
					);
					@endphp
					<div class="row flex-column-reverse flex-md-row formPerfil mt-4">
						<div class="col-12" id="listservhb">
							<div class="row prop_list_wrap_habequips">
								@foreach ($equipament as $ek => $equip)
								<div class="col-md-6 list_item_eqroom">
									<div class="checkbox_custom">
										<div class="checkbox_wrap">
											@if (isset($room) && in_array( $equip, $room->room_equipament))
											<input checked id="equipment_{{$ek}}" name="room_equipament[]" type="checkbox"
												class="form-check-input" value="{{$equip}}">
											@else
											<input id="equipment_{{$ek}}" name="room_equipament[]" type="checkbox" class="form-check-input"
												value="{{$equip}}">
											@endif

											<i class="fa fa-check"></i>
										</div>
										<label class="ml-2" for="equipment_{{$ek}}">{{$equip}}</label>
									</div>
								</div>
								@endforeach

								@if ($updating)
								@foreach ($room->room_equipament as $eqk => $eqp)
								@if (!in_array( $eqp, $equipament))
								<div class="col-md-6 list_item_eqroom">
									<div class="checkbox_custom">
										<div class="checkbox_wrap">
											<input checked id="equipment_add_{{$eqk}}" name="room_equipament[]" type="checkbox"
												class="form-check-input" value="{{$eqp}}">
											<i class="fa fa-check"></i>
										</div>
										<label class="ml-2" for="equipment_add_{{$eqk}}">{{$eqp}}</label>
									</div>
								</div>
								@endif
								@endforeach
								@endif

							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="checkbox_custom" id="checbkox_hab">
										<div class="checkbox_wrap">
											<input id="habequip_add" type="checkbox" class="form-check-input">
											<i class="fa fa-check"></i>
										</div>
										<label class="ml-2" for="habequip_add">Otro Equipamiento</label>
									</div>
									<div class="add_habequip_wrap other_proper">
										<input placeholder="¿Qué Equipamiento?" type="text" id="add_habequip_text"
											class="form-control other_proper_text">
										<div class="mt-1">
											<button id="add_habequip_button" class="button m-0 button_blue other_proper_button button_small">
												<i class="fa fa-plus"></i>
											</button>
											<button id="cancel_habequip_button"
												class="button m-0 button_red other_proper_button button_small">
												<i class="fa fa-ban"></i>
											</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>


				<div class="row mt-3">
					<div class="col-md-6">
						<h5 class="titleServiciosNM">Servicios adicionales</h5>
						<div class="formHome formPerfil">
							<div class="list_items_services_wrap">
								@foreach ($services as $sk => $servi)
								<div class="list_item_services">
									<div class="checkbox_custom">
										<div class="checkbox_wrap">

											@if (isset($room) && in_array( $servi, $room->room_services))
											<input id="serad-{{$sk}}" checked name="room_services[]" type="checkbox" class="form-check-input"
												value="{{$servi}}">
											@else
											<input id="serad-{{$sk}}" name="room_services[]" type="checkbox" class="form-check-input"
												value="{{$servi}}">
											@endif

											<i class="fa fa-check"></i>
										</div>
										<label class="ml-2" for="serad-{{$sk}}">{{$servi}}</label>
									</div>
								</div>
								@endforeach

								@if ($updating)
								@foreach ($room->room_services as $sk => $servi)
								@if (!in_array( $servi, $services))
								<div class="list_item_services">
									<div class="checkbox_custom">
										<div class="checkbox_wrap">
											<input checked id="serad-add-{{$sk}}" name="room_services[]" type="checkbox"
												class="form-check-input" value="{{$servi}}">
											<i class="fa fa-check"></i>
										</div>
										<label class="ml-2" for="serad-add-{{$sk}}">{{$servi}}</label>
									</div>
								</div>
								@endif
								@endforeach
								@endif

							</div>
							<div class="checkbox_custom" id="checbkox_ser">
								<div class="checkbox_wrap">
									<input id="serad_add" type="checkbox" class="form-check-input">
									<i class="fa fa-check"></i>
								</div>
								<label class="ml-2" for="serad_add">Otro Servicio</label>
							</div>
							<div class="add_service_wrap other_proper">
								<input placeholder="¿Qué Servicio?" type="text" id="add_service_text"
									class="form-control other_proper_text">
								<div class="mt-1">
									<button id="add_serv_button" class="button m-0 button_blue other_proper_button button_small">
										<i class="fa fa-plus"></i>
									</button>
									<button id="cancel_serv_button" class="button m-0 button_red other_proper_button button_small">
										<i class="fa fa-ban"></i>
									</button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<h5 class="titleServiciosNM">Obsequios</h5>
						<div class="obsequios_container prop_list_wrap_gifts">
							@foreach ($gifts as $gk => $gift)
							<div class="list_item_gifts">
								<div class="checkbox_custom">
									<div class="checkbox_wrap">

										@if (isset($room) && in_array( $gift, $room->room_gifts))
										<input id="gift-{{$gk}}" checked name="room_gifts[]" type="checkbox" class="form-check-input"
											value="{{$gift}}">
										<i class="fa fa-check"></i>
										@else
										<input id="gift-{{$gk}}" name="room_gifts[]" type="checkbox" class="form-check-input"
											value="{{$gift}}">
										<i class="fa fa-check"></i>
										@endif

									</div>
									<label class="ml-2" for="gift-{{$gk}}">{{$gift}}</label>
								</div>
							</div>
							@endforeach

							@if ($updating)
							@foreach ($room->room_gifts as $gk => $gift)
							@if (!in_array( $gift, $gifts))

							<div class="list_item_gifts">
								<div class="checkbox_custom">
									<div class="checkbox_wrap">
										<input id="gift-add-{{$sk}}" checked name="room_gifts[]" type="checkbox" class="form-check-input"
											value="Tour">
										<i class="fa fa-check"></i>
									</div>
									<label class="ml-2" for="gift-add-{{$sk}}">{{$gift}}</label>
								</div>
							</div>

							@endif
							@endforeach
							@endif



						</div>
						<div class="checkbox_custom" id="checbkox_ser">
							<div class="checkbox_wrap">
								<input id="gift_add" type="checkbox" class="form-check-input">
								<i class="fa fa-check"></i>
							</div>
							<label class="ml-2" for="gift_add">Otro Obsequio</label>
						</div>
						<div class="add_gift_wrap other_proper">
							<input placeholder="¿Qué obsequio?" type="text" id="add_gift_text" class="form-control other_proper_text">
							<div class="mt-1">
								<button id="add_gift_button" class="button m-0 button_blue other_proper_button button_small">
									<i class="fa fa-plus"></i>
								</button>
								<button id="cancel_gift_button" class="button m-0 button_red other_proper_button button_small">
									<i class="fa fa-ban"></i>
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="box">
				<h5 class="titleServiciosNM">Condiciones y restricciones de la habitación</h5>
				<div class="ckeditor_initial" data-name='room_terms' id='room_terms'>
					{!! $room->room_terms ?? '' !!}
				</div>
			</div>

			<button type="submit" class="button button_blue no_hover">{{$updating ? "Actualizar" : "Crear"}}</button>
		</div>

	</form>

</div>
@endsection