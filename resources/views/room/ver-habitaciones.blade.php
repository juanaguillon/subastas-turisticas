@extends('layout-admin')
@section('title', "Mis Habitaciones")
@section('body_id',"all_rooms")
@section('content')

<section id="index">
  <div id="content">
    <div class="container">

      <div class="row search-form">
        <div class="col-md-12 subastas_items">
          <h3 class="admin_title">Mis Habitaciones</h3>

          <div id="filtering_wrap_abs">
            <div class="form-group">
              <select class="select_principal show" id="subastas_filtro">
                <option value="Selecciones una filtro" disabled selected>Seleccione un filtro</option>
                <option value="subastas_status">Estado</option>
              </select>
            </div>

            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_status" data-name="room_active">
                <option value="none">Todos los estados</option>
                @foreach ($actives as $state)
                <option data-count="{{$state["count"]}}" value="{{$state['room_active']}}">
                  {{$state['room_active'] == 1 ? 'Activo' : 'Inactivo'}}</option>
                @endforeach
                {{-- <option data-count="{{$hot["count"]}}" value="{{$key}}">{{$hot["name"]}}</option> --}}
              </select>
            </div>
            <button type="submit" class="button button_red no_hover" id="selects_filter_paginator">FILTRAR</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12" id="auction-grid">
          <div class="tablegen-loading">
            <div class="loading">
              <div class="lds-default">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
          <table class="table tablagen odd">
            <thead>
              <tr>
                <th class="order_column_th" data-oby="room_name">Nombre</th>
                <th class="order_column_th" data-oby="room_adults"># Adultos</th>
                <th class="order_column_th" data-oby="room_minprice">Precio mínimo</th>
                <th class="order_column_th" data-oby="room_maxprice">Precio Máximo</th>
                <th class="order_column_th" data-oby="room_active">Activo</th>
                <th class="order_column_th" data-oby="room_created">Fecha</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody id="tbody_results">

            </tbody>

          </table>
          <input type="hidden" id="start_paginator" value="0">
          <input type="hidden" id="limit_paginator" value="5">
          <input type="hidden" id="url_paginator" value="{{url("/room/paginador")}}">
          <input type="hidden" id="total_paginator" value="{{$roomcount}}">

          <div class="pagination_wrap">
            <select name="" class="select_principal select_paginator" id="quantity_screen_pag">
              <option value="1">1</option>
              <option value="5">5</option>
              <option value="10" selected>10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
            <div id="paginator_container">
            </div>
          </div>


          @if (currentUserHasRol("hotel"))
          <div class="button_action_wrap mt-3">
            <a href="{{url('/room/create')}}" class="button button_blue m-0">Crear Habitación</a>
          </div>
          @endif
        </div>
      </div>


    </div>
  </div><!-- content -->
</section>
@endsection