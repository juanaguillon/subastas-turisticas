@extends('layout-admin')
@section('body_id', 'user_config')
@section('title','Mi Configuración')
@section('content')
<div id="index">

  <div class="container">
    <div class="row">
      <div class="col-12">

        <div class="box">

          @if($errors->any())
          <div class="alert alert-danger">
            {{$errors->first()}}
          </div>
          @elseif (isset($_GET['new_reg']))
          <div class="alert alert-success">
            Datos actualizados correctamente
          </div>
          @endif
          <div id="user_config_danger" class="alert alert-danger form_alert_toggle"></div>
          <h3 class="admin_title">Configuracion de sus datos</h3>
          <form id="user-form-config" action="{{url('user/configuracion')}}" method="post">
            @csrf
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="user_name" class="label_principal">Nombre</label>
                  <input class="form-control" value='{{$user->user_name}}' maxlength="255" name="user_name"
                    id="user_name" type="text" value="">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="user_email" class="label_principal">Email</label>
                  <input class="form-control" maxlength="255" placeholder="Email de usuario" name="user_email"
                    id="user_email" type="text" value="{{$user->user_email}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">

                  <label for="user_password" class="label_principal">Contraseña</label>
                  <input class="form-control" maxlength="60" name="user_password" id="user_password" type="password"
                    value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="user_rpassword" class="label_principal">Repetir Contraseña</label>

                  <input class="form-control" maxlength="60" id='user_rpassword' name="user_rpassword" type="password"
                    value="">
                </div>
              </div>
              <div class="col-12">
                <button class="button button_blue">
                  Guardar cambios
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection