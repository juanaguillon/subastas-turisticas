@extends('layout-admin')
@section('title', "Mis Subastas")
@section('body_id', "ver_subastas")
@section('content')
<section id="index">

  <div class="container">
    <div class="row search-form">
      <div class="col-md-12 subastas_items">
        <h3 class="admin_title">Historial de Subastas</h3>
        <div id="filtering_wrap_abs">
          <div class="form-group">
            <select class="select_principal show" id="subastas_filtro">
              <option value="Selecciones una filtro" disabled selected>Seleccione un filtro</option>
              <option value="subastas_ciudad">Ciudad</option>
              <option value="subastas_hotel">Hotel</option>
              <option value="subastas_estado">Estado</option>
              {{-- <option value="subastas_personas">Personas</option> --}}
            </select>
          </div>
          <div class="form-group">
            <select class="select_principal select_filtering" id="subastas_ciudad" data-name="city_id">
              <option value="none">Seleccione una ciudad</option>
              @foreach ($cities as $key => $city)
              <option data-count="{{$city["count"]}}" value="{{$key}}">{{$city["name"]}}</option>
              @endforeach
            </select> </div>
          <div class="form-group">
            <select class="select_principal select_filtering" id="subastas_hotel" data-name="auctionrow_hotel">
              <option value="none">Todos los hoteles</option>
              @foreach ($hotels as $key => $hot)
              <option data-count="{{$hot["count"]}}" value="{{$key}}">{{$hot["name"]}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <select class="select_principal select_filtering" id="subastas_estado" data-name="auction_status">
              <option value="none">Seleccione un estado</option>
              @foreach ($status as $key => $stat)
              <option data-count="{{$stat["count"]}}" value="{{$key}}">{{$stat["name"]}}</option>
              @endforeach
            </select>
          </div>

          <button type="submit" class="button button_red no_hover" id="selects_filter_paginator">FILTRAR</button>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12" id="auction-grid">
        <div class="tablegen-loading">
          <div class="loading">
            <div class="lds-default">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
        </div>
        <table class="table tablagen odd">
          <thead>
            <tr>
              <th class="order_column_th" data-oby="auctionrow_id">Referencia</th>
              <th class="order_column_th" data-oby="city_name">Destino</th>
              <th class="order_column_th" data-oby="auction_days">Días</th>
              <th class="order_column_th" data-oby="auction_maxprice">Máx. Precio</th>
              <th class="order_column_th" data-oby="auctionrow_price">Última oferta</th>
              <th class="order_column_th desc" data-oby="auctionrow_updated">Fecha</th>
              <th class="">Estado</th>
              <th class="" data-oby="">&nbsp;</th>
            </tr>
          </thead>
          <tbody id="tbody_results">

          </tbody>

        </table>
        <input type="hidden" id="start_paginator" value="0">
        <input type="hidden" id="limit_paginator" value="5">
        <input type="hidden" id="url_paginator" value="{{url("/auction/paginador")}}">
        <input type="hidden" id="total_paginator" value="{{count($auctions)}}">

        <div class="pagination_wrap">
          <select name="" class="select_principal select_paginator" id="quantity_screen_pag">
            <option value="1">1</option>
            <option value="5">5</option>
            <option value="10" selected>10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
          <div id="paginator_container">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection