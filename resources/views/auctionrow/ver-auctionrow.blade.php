{{-- Este se usará en el momento que se está crendo o modificando la subasta. --}}

@extends('layout-site')
@section("body_id","body_ver_auctionrow")
@section('content')

@php
$hotelImagesPath = url("/storage/hotel") . "/";

$roomImages = json_decode($auctionrow->room_images, true);
$roomImagesPath = url("/storage/habitacion") . "/";

$hotelImages = json_decode($auctionrow->hotel_images, true);

$roomGifts = json_decode($auctionrow->room_gifts);
@endphp


<section id="galeriadest">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 m-0 p-0">
        <div class="camera_wrap" id="bannerimages">
          <div data-thumb="{{url("/images/subastas-turisticas-hotsale-background-min.jpg")}}"
            data-src="{{url("/images/subastas-turisticas-hotsale-background-min.jpg")}}">
            <div class="nombreHabitacion fadeIn">
              <div class="nombrehabitacion_icono"><i class="icon-martillo"></i></div>
              <h3>{{$auctionrow->city_name}}</h3>
            </div>
          </div>
          <div data-thumb="{{url("/images/subastas-turisticas-plan-background-min.jpg")}}"
            data-src="{{url("/images/subastas-turisticas-plan-background-min.jpg")}}">
            <div class="nombreHabitacion fadeIn">
              <div class="nombrehabitacion_icono"><i class="icon-martillo"></i></div>
              <h3>{{$auctionrow->city_name}}</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section id="destacados" class="plan_inner">

  <div class="container">
    @component('components.modal-reserva', array(
    "plantype" => "auction",
    'plan_id' => $auctionrow->auctionrow_id ,
    ))
    @endcomponent
    <div class="row">
      <div class="col-md-12 col-lg-8">
        <div class="titulohabitacion">
          <h3 class="bold">{{$auctionrow->hotel_name}}</h3>
        </div>
        <div class="fotosProducto">
          @if (count($hotelImages) > 0)
          <div class="foto_producto_wrap">
            <button class="button_slide_product button_left"><i class="fa fa-angle-left"></i></button>
            <ul id="foto_producto_list">

              @foreach ($hotelImages as $ki => $img)
              <li id="foto0{{$ki}}">
                <a href="{{$hotelImagesPath . $img["765"]}}">
                  <img src="{{$hotelImagesPath . $img["765"]}}">
                </a>
              </li>
              @endforeach
            </ul>
            <button class="button_slide_product button_right"><i class="fa fa-angle-right"></i></button>
          </div>
          <div class="fotos_producto_wrap">
            <ul id="fotos_producto_list">
              @foreach ($hotelImages as $ki => $img)
              <div class="foto_wrap">
                <li>
                  <img src="{{$hotelImagesPath . $img["350"]}}">
                </li>
              </div>
              @endforeach
            </ul>
          </div>
          @else
          <div class="message_info">
            <p>Este hotel no tiene imágenes disponibles.</p>
          </div>
          @endif

        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <div class="titulohabitacion">
          <h3 class="bold">Oferta en {{$auctionrow->city_name}}</h3>
        </div>

        <div class="infoPlanes">
          <div class="row no-gutters">
            <div class="mb-2 col-md-12 column_reserv">
              <label class="label" for="where">Destino:</label>
              <input type="text" class="form-control" id="where" name="name" value="{{$auctionrow->city_name}}"
                disabled="">
            </div>
            <div class="mb-2 col-md-6 column_reserv">
              <label class="label" for="checkin">Check-in:</label>
              <input type="text" class="form-control" id="checkin" name="name" value="{{$auctionrow->auction_init}}"
                disabled="">
            </div>
            <div class="mb-2 col-md-6 column_reserv">
              <label class="label" for="checkout">Check-out:</label>
              <input type="text" class="form-control" id="checkout" name="name" value="{{$auctionrow->auction_end}}"
                disabled="">
            </div>
            <div class="mb-2 col-md-4 column_reserv">
              <label class="label" for="checkin">Adultos</label>
              <input type="text" class="form-control" id="where" name="name" value="{{$auctionrow->auction_adults}}"
                disabled="">
            </div>
            <div class="mb-2 col-md-4 column_reserv">
              <label class="label" for="checkout">Niños</label>
              <input type="text" class="form-control" id="where" name="name"
                value="{{$auctionrow->auction_childrens ?? 0}}" disabled="">
            </div>
            <div class="mb-2 col-md-4 column_reserv">
              <label class="label" for="checkout">Infantes</label>
              <input type="text" class="form-control" id="where" name="name"
                value="{{$auctionrow->auction_infants ?? 0}}" disabled="">
            </div>


            @if ($roomGifts && count($roomGifts) > 0 )
            <div class="col-12 column_reserv">
              <label class="">Obsequios</label>
              <ul class="aditions_auction">
                @foreach ($roomGifts as $gift)
                <li>{{$gift}}</li>
                @endforeach
              </ul>
            </div>
            @endif


            <div class="col-12 column_reserv col_gutter">
              <div class="infoprice inf-iz">
                <div class="pricetitle">
                  <span>Precio:</span>
                </div>
                <div class="price">{{formatPrice($auctionrow->auctionrow_price, '')}} <span size="1"
                    class="cop">COP</span>
                </div>
              </div>
            </div>
            <div class="col-12 column_reserv col_gutter mb-3">
              <div class="infoprice inf-der">
                <div class="pricetitle">
                  <span>Total booking:</span>
                </div>
                @php
                $calculate = $auctionrow->auctionrow_price * $auctionrow->auction_days
                @endphp
                <div class="price">{{formatPrice($calculate, '')}} <span size="1" class="cop">COP</span>
                </div>
              </div>
            </div>

          </div>
          <div class="column_reserv">
            <form id="ajax-contact-form2" class="form-horizontal" method="post">
              <button id="show_reserva_modal" type="submit" class="button button_red no_hover">RESERVAR</button>
            </form>
          </div>
        </div>

      </div>
    </div>
    <div class="row mt-3">
      <div class="col-md-12 col-lg-8">
        <div class="servhabitacion">
          <h3 class="titleserv">Detalles del Oferta</h3>

          <div class="liservice">
            <ul class="tabs">
              <li class="tab-link current" data-tab="tab-1"><span>Hotel</span></li>
              <li class="tab-link" data-tab="tab-2"><span>Habitación</span></li>
              <li class="tab-link" data-tab="tab-3"><span>Términos y condiciones</span></li>
            </ul>

            <div id="tab-1" class="tab-content current">
              <div class="subseccion">
                <h4><i class="icono fa fa-building"></i>Hotel</h4>

                {{-- DESCRIPCION DE HOTEL --}}
                @if ($auctionrow->hotel_description)
                {!!$auctionrow->hotel_description!!}
                @endif

                <h4><i class="icono fa fa-phone"></i>Teléfonos</h4>
                <ul class="phones_views">
                  <li>{{$auctionrow->hotel_phone}}</li>
                </ul>

              </div>
            </div>

            <div id="tab-2" class="tab-content">
              <div class="subseccion">
                @if ($auctionrow->room_description)
                <h4><i class="icono fa fa-list-alt"></i>Descripción de la habitación</h4>
                {!! $auctionrow->room_description !!}
                @endif

                <h6>Tamaño de la habitación:</h6>
                <span>{{$auctionrow->room_meters}} m2</span>
                {{-- <p>Suite con vistas a la ciudad, TV de pantalla plana por cable, soporte para iPod, zona de estar con
                  sofá, y baño privado con ducha. Está insonorizada, e incluye acceso executive al salón.</p> --}}
              </div>
              <div class="linea3 mt-4 mb-4"></div>


              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fas fa-clone"></i>Imágenes de la habitación</h4>
                @if ($roomImages)
                <div class="slider_images_room_wrap">
                  <button class="button_slide_room button_left"><i class="fa fa-angle-left"></i></button>
                  <ul id="slider_images_room">
                    @foreach ($roomImages as $kimg => $img)
                    <li id="foto{{$kimg}}"><img data-lazy="{{$roomImagesPath . $img["765"]}}"></li>
                    @endforeach
                  </ul>
                  <button class="button_slide_room button_right"><i class="fa fa-angle-right"></i></button>
                </div>

                @else
                <p>Esta habitación no posee imágenes.</p>
                @endif


              </div>
              <div class="linea3 mt-4 mb-4"></div>
              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fa fa-bookmark"></i>Servicios Adicionales</h4>
                <div class="row no-gutters">
                  @if ($auctionrow->room_services && count(json_decode($auctionrow->room_services)) > 0 )
                  @foreach (json_decode($auctionrow->room_services) as $service)
                  <div class="col-md-6">
                    <span class="list_checkbox">
                      <i class="fas fa-check-square fa-lg"></i>
                      <span>{{$service}}</span>
                    </span>
                  </div>
                  @endforeach
                  @else
                  <p>No se han encontrado servicios de la habitación.</p>
                  @endif
                </div>

              </div>
              <div class="linea3 mt-4 mb-4"></div>
              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fa fa-suitcase"></i>Equipamento de la habitación</h4>
                <div class="row">
                  @if ($auctionrow->room_equipament && count(json_decode($auctionrow->room_equipament)) > 0 )
                  @foreach (json_decode($auctionrow->room_equipament) as $wquip)
                  <div class="col-md-6">
                    <span class="list_checkbox">
                      <i class="fas fa-check-square fa-lg"></i>
                      <span>{{$wquip}}</span>
                    </span>
                  </div>
                  @endforeach
                  @else
                  <p>No se han encontrado equipamiento de la habitación.</p>
                  @endif
                </div>
              </div>
              <div class="linea3 mt-4 mb-4"></div>
              <div class="subseccion">
                <h4 class="mt-4"><i class="icono fa fa-flag-checkered"></i>Condiciones y restricciones</h4>
                @if ($auctionrow->room_terms)
                {!! $auctionrow->room_terms !!}
                @else
                <p>No se ha agregado condiciones y restricciones de la habitación.</p>
                @endif
              </div>
            </div>
            <div id="tab-3" class="tab-content">
              <div class="subseccion">
                <h4><i class="icono fa fa-gavel"></i>Términos y condiciones de Hotel</h4>
                @if ($auctionrow->hotel_terms)
                {!! $auctionrow->hotel_terms !!}
                @else
                <p>No se ha agregado condiciones y restricciones del hotel.</p>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <div class="col-md-12 m-0 p-0">
          <div class="allservice">
            <h4>Servicios del hotel:</h4>
            @if ($auctionrow->hotel_services && count(json_decode($auctionrow->hotel_services)) > 0)
            <ul>
              @foreach (json_decode($auctionrow->hotel_services, true) as $service)
              <li><span class="hotel_service">{{$service}}</span></li>
              @endforeach
            </ul>
            @else
            <p>No se ha registrado servicios de hotel.</p>
            @endif

          </div>
        </div>

      </div>
      <div class="col-md-8 my-3">
        <div class="allservice">
          <h4>Condiciones y restricciones del hotel:</h4>

          @if ($auctionrow->hotel_terms && $auctionrow->hotel_terms !== "" )
          {!! $auctionrow->hotel_terms !!}
          @else
          <p>No se ha registrado condiciones de hotel.</p>
          @endif
          <p></p>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection