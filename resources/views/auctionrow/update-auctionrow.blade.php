@extends('layout-admin')
@section('content')
<section id="index">
  <div class="container">
    <div class="box" id="box_update_auctionrow">
      <div class="row">
        <input type="hidden" id='auctionrow_init' value='{{$auction->auction_id}}'>
        {{-- auction_maxhotels verificará cuantos hoteles en total se estarán subastando. --}}
        {{-- Esto ayudará a obtener diferentes ID de hotel en cada auctionRow --}}
        <input type="hidden" id='auctionrow_maxhotels' value='{{$auction->auction_hotelscount}}'>

        {{-- auction_lasthotel Verificará cuál fue el último hotel que se procesó y obtener un hotel distinto para subastar --}}
        <input type="hidden" id="auctionrow_lastnum" value="0">
        <input type="hidden" id="auction_timestamp" value="">
        {{-- auction_first Servirá para mostrar "Un hotel está subastando en este momento", este valor no lo usará el backend. --}}
        <input type="hidden" id="auction_first" value="1">

        <input type="hidden" id='auction_token' value='{{csrf_token()}}'>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <table class="table tablagen table-striped">
            <tbody>
              <tr>
                <td class="titleTable">Destino</td>
                <td class="descTable">{{$hotel->hotel_name}}</td>
              </tr>
              <tr>
                <td class="titleTable">Fecha de inicio</td>
                <td class="descTable">{{$auction->auction_init}}</td>
              </tr>
              <tr>
                <td class="titleTable">Fecha de regreso</td>
                <td class="descTable">{{$auction->auction_end}}</td>
              </tr>
              <tr>
                <td class="titleTable">Nombre</td>
                <td class="descTable">{{$auction->auction_username}}</td>
              </tr>
              <tr>
                <td class="titleTable">E-mail</td>
                <td class="descTable">{{$auction->auction_useremail}}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <table class="table tablagen table-striped">
            <tbody>
              <tr>
                <td class="titleTable">Presupuesto maximo (Por noche)</td>
                <td class="descTable">{{formatPrice($auctionrow->auctionrow_price)}}</td>
              </tr>
              <tr>
                <td class="titleTable"># Personas</td>
                <td class="descTable">{{$persons}}</td>
              </tr>
              <tr>
                <td class="titleTable">Adultos</td>
                <td class="descTable">{{$auction->auction_adults}}</td>
              </tr>
              <tr>
                <td class="titleTable">Niños</td>
                <td class="descTable">{{$auction->auction_childrens ?? 0}}</td>
              </tr>
              <tr>
                <td class="titleTable">Infantes</td>
                <td class="descTable">{{$auction->auction_infants ?? 0}}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-12">
        </div>
      </div>
    </div>

    <div class="box">
      @if ($auction->auction_status === 3 )
      <h5 class="text-primary mb-2">Subasta finalizada</h5>
      @endif
      @if ($auction->auction_status === 1 )
      <h5 class="text-primary mb-2">Obteniendo subasta</h5>
      <input type="hidden" id='getting_auctions'>
      <div id="ofertate_wrap">
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="alert alert-danger d-none" id="subastar_auction_error"></div>
            <div class="alert alert-info d-none" id="subastar_auction_info"></div>
          </div>
          <div class="col-md-6"></div>
          <div class="col-md-6">
            <select id="subastar_room_id" class='select_principal'>
              <option value="none">Seleccione Habitación</option>
              @foreach ($rooms as $room)
              <option value="{{$room->room_id}}">{{$room->room_name}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-6">
            <div class="input-group mb-3">
              <input id='subastar_number' type="number" class="form-control" placeholder="Ofertar">
              <div class="input-group-append">
                <button class="btn btn-primary" id='subastar_auction' type="button">Subastar</button>
              </div>
            </div>
          </div>
        </div>

      </div>
      @endif
      <p class="text-info d-none" id="subasta_text">Se esta actualizando la subasta...</p>
      <span data-timeix='150' id="subasta_timer" class="d-none">02'30''</span>
      <table id="bid_table" class="table tablagen odd" style="">
        <thead>
          <tr>
            <th>Tiempo</th>
            <th>Hotel</th>
            <th>Habitación</th>
            <th>Valor</th>
            <th>Obsequios</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody id="subasta_response_rows" class="d-none">

        </tbody>
      </table>

    </div>
  </div>
</section>
@endsection