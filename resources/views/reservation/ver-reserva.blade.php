@extends('layout-admin')

@section('title', "Reserva 15")
@section('content')

<section id="index">


  <div class="container">


    <div class="box_card">

      <div class="box_head">
        <h3>Información de titular</h3>
      </div>
      <div class="box_content">
        <div class="row">
          <div class="col-md-12">
            <div class="information_group">
              <span class="information_label">Nombres y Apellidos:</span>
              <span class="information_value">{{formatName($reserva->reservation_clientname)}}</span>
            </div>

          </div>
          <div class="col-md-6">
            <div class="information_group">

              <span class="information_label">Teléfono:</span>
              <span class="information_value">{{$reserva->reservation_clientphone}}</span>
            </div>
          </div>
          <div class="col-md-6">
            <div class="information_group">

              <span class="information_label">Correo Electrónico:</span>
              <span class="information_value">{{strtolower($reserva->reservation_clientmail)}}</span>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="box_card h-100">
          <div class="box_head">
            <h3>Información de plan</h3>
          </div>

          <div class="box_content">
            <div class="row">
              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Código de reserva:</span>
                  <span class="information_value">{{$reserva->reservation_code}}</span>
                </div>
              </div>

              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Estado:</span>
                  <span class="information_value">{{$reserva_data["reserva_estadotext"]}}</span>
                </div>
              </div>

              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Nombre de reserva:</span>
                  <span class="information_value">{{$reserva_data["reserva_name"]}}</span>
                </div>
              </div>

              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Precio Total:</span>
                  <span
                    class="information_value">{{formatPrice($reserva->reservation_price, $reserva->reservation_currency)}}</span>
                </div>
              </div>

              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Tipo de reserva:</span>
                  @switch($reserva->reservation_type)
                  @case("hotsale")
                  <span class="information_value">Hotsale</span>
                  @break
                  @case("plan")
                  <span class="information_value">Plan</span>
                  @break
                  @case("auction")
                  <span class="information_value">Oferta</span>
                  @break
                  @default

                  @endswitch
                </div>

              </div>

              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Salida:</span>
                  <span class="information_value">{{formatDate($reserva_data["reserva_init"])}}</span>
                </div>
              </div>


              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Regreso:</span>
                  <span class="information_value">{{formatDate($reserva_data["reserva_end"])}}</span>
                </div>
              </div>

            </div>
          </div>

        </div>

      </div>

      <div class="col-md-6">
        <div class="box_card  h-100">
          <div class="box_head">
            <h3>Información de hotel</h3>

          </div>
          <div class="box_content">
            <div class="row">
              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Nombre de Hotel</span>
                  <span class="information_value">{{$reserva->hotel_name}}</span>
                </div>

              </div>
              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Dirección:</span>
                  <span class="information_value">{{$reserva->hotel_address}}</span>
                </div>

              </div>

              <div class="col-12">
                <div class="information_group">
                  <span class="information_label">Ciudad:</span>
                  <span class="information_value">{{$reserva->city_name}}</span>
                </div>


              </div>
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>
</section>
@endsection