@extends('layout-admin')
@section('title', "Mis reservas")
@section('body_id',"ver_reservas")
@section('content')

<section id="index">
  <div id="content">
    <div class="container">

      <div class="row search-form">
        <div class="col-md-12 subastas_items">
          <h3 class="admin_title">Historial de Reservas</h3>

          <div id="filtering_wrap_abs">
            <div class="form-group">
              <select class="select_principal show" id="subastas_filtro">
                <option value="Selecciones una filtro" disabled selected>Seleccione un filtro</option>
                <option value="subastas_destino">Destino</option>
                <option value="subastas_status">Estado</option>
              </select>
            </div>
            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_destino" data-name="city_id">
                <option data-count="{{count($reservas)}}" value="none">Seleccione un destino</option>
                @foreach ($cities as $ckey => $cval)
                <option data-count="{{$cval["count"]}}" value="{{$ckey}}">{{$cval["city_name"]}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_status" data-name="reservation_status">
                <option data-count="{{count($reservas)}}" value="none">Todos los estados</option>
                @foreach ($status as $skey => $sval)
                <option data-count="{{$sval["count"]}}" value="{{$skey}}">{{$sval["reservation_statustext"]}}</option>
                @endforeach
              </select>
            </div>

            <button type="submit" class="button button_red no_hover" id="selects_filter_paginator">FILTRAR</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12" id="auction-grid">
          <div class="tablegen-loading">
            <div class="loading">
              <div class="lds-default">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
          <table class="table tablagen odd">
            <thead>
              <tr>
                <th class="order_column_th" data-oby="city_name">Destino</th>
                <th class="order_column_th" data-oby="reservation_created">Fecha</th>
                <th class="order_column_th" data-oby="reservation_price">Precio</th>
                <th class="order_column_th" data-oby="reservation_status">Estado</th>
                <th class="order_column_th"></th>
              </tr>
            </thead>
            {{-- The results will be loaded with resources/javascript/pages/reservation/cliente-reservas.js --}}
            <tbody id="tbody_results">
            </tbody>

          </table>
          <input type="hidden" id="start_paginator" value="0">
          <input type="hidden" id="limit_paginator" value="5">
          <input type="hidden" id="url_paginator" value="{{url("/reserva/paginador")}}">
          <input type="hidden" id="total_paginator" value="{{count($reservas)}}">

          <div class="pagination_wrap">
            <select name="" class="select_principal select_paginator" id="quantity_screen_pag">
              <option value="1">1</option>
              <option value="5">5</option>
              <option value="10" selected>10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
            <div id="paginator_container">
            </div>
          </div>
        </div>
      </div>


    </div>
  </div><!-- content -->
</section>
@endsection