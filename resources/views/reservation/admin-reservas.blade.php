@extends('layout-admin')
@section('title', "Mis reservas")
@section('body_id',"admin_reservas")
@section('content')


<section id="index">
  <div id="content">
    <div class="container">

      <div class="row search-form">
        <div class="col-md-12 subastas_items">
          <h3 class="admin_title">Historial de Reservas</h3>

          <div id="filtering_wrap_abs">
            <div class="form-group">
              <select class="select_principal show" id="subastas_filtro">
                <option value="Selecciones una filtro" disabled selected>Seleccione un filtro</option>
                <option value="subastas_hotel">Hotel</option>
                <option value="subastas_typeresrva">Tipo de Reserva</option>
              </select>
            </div>

            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_hotel" data-name="hotel_id">
                <option value="none">Todos los hoteles</option>
                @foreach ($hotels as $ik => $hotel)
                <option data-count="{{$hotel["count"]}}" value="{{$ik}}">{{$hotel["hotel_name"]}}</option>
                @endforeach
                {{-- <option data-count="{{$hot["count"]}}" value="{{$key}}">{{$hot["name"]}}</option> --}}
              </select>
            </div>
            <div class="form-group">
              <select class="select_principal select_filtering" id="subastas_typeresrva" data-name="reservation_type">
                <option value="none">Todos los tipos</option>
                @foreach ($type as $ik => $typ)
                <option data-count="{{$typ["count"]}}" value="{{$ik}}">{{$typ["reservation_typetext"]}}</option>
                @endforeach
                {{-- <option data-count="{{$hot["count"]}}" value="{{$key}}">{{$hot["name"]}}</option> --}}
              </select>
            </div>

            <button type="submit" class="button button_red no_hover" id="selects_filter_paginator">FILTRAR</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12" id="auction-grid">
          <div class="tablegen-loading">
            <div class="loading">
              <div class="lds-default">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
          <table class="table tablagen odd">
            <thead>
              <tr>

                <th class="order_column_th" data-oby="reservation_id">Ref.</th>
                <th class="order_column_th" data-oby="hotel_name">Hotel</th>
                <th class="order_column_th" data-oby="reservation_type">Tipo de reserva</th>
                <th class="order_column_th" data-oby="reservation_created">Fecha</th>
                <th class="order_column_th" data-oby="reservation_price">Precio</th>
                <th class="order_column_th" data-oby="reservation_clientname">Titular</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbody_results">

            </tbody>

          </table>
          <input type="hidden" id="start_paginator" value="0">
          <input type="hidden" id="limit_paginator" value="5">
          <input type="hidden" id="url_paginator" value="{{url("/reserva/paginador")}}">
          <input type="hidden" id="total_paginator" value="{{count($reservas)}}">

          <div class="pagination_wrap">
            <select name="" class="select_principal select_paginator" id="quantity_screen_pag">
              <option value="1">1</option>
              <option value="5">5</option>
              <option value="10" selected>10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
            <div id="paginator_container">
            </div>
          </div>
        </div>
      </div>


    </div>
  </div><!-- content -->
</section>
@endsection