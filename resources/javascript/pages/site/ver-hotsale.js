import {
	initalizeTabs,
	initRoomTabSlick,
	initProductSlick,
	initializeModalReservation
} from "./ver-plan";
import $ from "jquery";
import { initCamera } from "../../plugins/camera";

if ($("#body_ver_hotsale").length) {
	initalizeTabs();
	initRoomTabSlick("#slider_images_room");
	initProductSlick("#foto_producto_list", "#fotos_producto_list");
	initializeModalReservation();
	initCamera(
		".camera_wrap",
		function() {},
		function() {}
	);
}
