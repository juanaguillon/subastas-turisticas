// var conn = new WebSocket("ws://localhost:8090");
// conn.onopen = function(e) {
// 	console.log("Connection established!");
// 	$("#message_send").click(e => {
// 		let messageText = $("#message_text").val();
// 		conn.send(messageText);
// 	});
// };

// conn.onmessage = function(e) {
// 	console.log(e.data);
// };

import $ from "jquery";
import "jquery-ui/ui/core";
import "jquery-ui/ui/widgets/datepicker";
import noUiSlider from "nouislider/distribute/nouislider";
import wNumb from "wnumb";
import "nouislider/distribute/nouislider.css";
import slick from "slick-slider/slick/slick";
import { datePickerConfig, renderRow } from "../base";
import { initCamera } from "../../plugins/camera";
/**
 * Se pueden ver estas variables en el archivo webpack.env
 */
const backurl = process.env.BACKEND_URL;
const socketPort = process.env.WEBSOCKET_PORT;
const socketUrl = process.env.WEBSOCKET_URL + ":" + socketPort;

function createDatePicker() {
	$.datepicker.regional["es"] = datePickerConfig;
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$(function() {
		$("#fecha_ingreso").datepicker({
			beforeShow: function(input, inst) {
				setTimeout(function() {
					inst.dpDiv.css({
						top: $("#fecha_ingreso").offset().top + 35,
						left: $("#fecha_ingreso").offset().left
					});
				}, 0);
			}
		});
		$("#fecha_salida").datepicker({
			beforeShow: function(input, inst) {
				setTimeout(function() {
					inst.dpDiv.css({
						top: $("#fecha_salida").offset().top + 35,
						left: $("#fecha_salida").offset().left
					});
				}, 0);
			}
		});
	});
}

function initalizeCamera() {
	var onLoad = function() {
		$(".banner-promo").hide();
	};
	var onEndTrans = function() {
		var ind = $(".camera_target .cameraSlide.cameracurrent").index();
		$("#banner-promo-" + ind).show();
	};
	initCamera(".camera_wrap", onLoad, onEndTrans);
}

/**
 * Funcionalidad para crear una subasta
 */
function onCreateSubasta() {
	//Intervalo de cada cuanto se va a llamar una nueva fila de subasta
	var intervalAuRow = null;
	// Conexión de websocket
	var connWebSock = null;
	// Contador que aparece encima de la tabla de filas de subasta
	var counter = null;

	const initalizeWebSocket = function() {
		connWebSock = new WebSocket(socketUrl);
		connWebSock.onopen = function(e) {
			console.log("Connection established!");
			let messages = {
				caseSub: "auctionSub",
				auctionID: $("#auctionrow_init").val()
			};
			connWebSock.send(JSON.stringify(messages));
		};
		connWebSock.onmessage = function(e) {
			console.log(e.data);
			if (e.data == 1) {
				$.get({
					url:
						backurl +
						"/auctionrow/get-last-by-auction-id/" +
						$("#auctionrow_init").val(),
					success: function(dataRes) {
						renderRow($, dataRes);
					}
				});
			}
		};

		// clearInterval(intervalAuRow);
		// alert("Cancel Interval");
	};

	const initializeSubasRows = function($) {
		const showError = function(text) {
			$("#subasta_error").addClass("show");
			$("#subasta_error").text(text);
		};

		const changeStatusText = function(text) {
			$("#subasta_text").text(text);
			$("#subasta_text").removeClass("d-none");
		};
		const hideStatusText = function() {
			$("#subasta_text").addClass("d-none");
		};

		/**
		 * Inicializar la subasta. Tenga en cuenta que existe la fase de crear la subasta, y la fase de llamar las filas de la subasta creada. La referencia de subasta actual sera auctionID
		 */
		const callAjaxSubasta = function() {
			var auctionID = $("#auctionrow_init").val();
			$.ajax({
				url: backurl + "/auctionrow/create",
				method: "POST",
				data: {
					_token: $("#auction_token").val(),
					auctionrow_init: auctionID,
					auctionrow_maxhotels: $("#auctionrow_maxhotels").val(),
					auctionrow_lastnum: $("#auctionrow_lastnum").val(),
					auctionrow_time: $("#auction_timestamp").val()
				},
				success: function(dataAuct) {
					hideStatusText();
					if (
						typeof dataAuct.error !== "undefined" &&
						dataAuct.error != false
					) {
						// En caso que haya un error, se debe agregar la subasta ( Auction) a estado cancelada.
						showError(dataAuct.message);
						clearInterval(intervalAuRow);
						clearInterval(counter);
						let url =
							backurl + "/auction/alter-auction/" + auctionID + "?status=5";
						$.ajax({
							url: url,
							success: function() {
								showError(dataAuct.message);
							}
						});
					} else {
						if (dataAuct.iterate === true) {
							// Agregar el nuevo arrayKey a los hoteles agregados a la subasta.
							// En teoría, agregar +1 a el valor actual de el key Array de los hoteles.
							$("#auctionrow_lastnum").val(dataAuct.hotelkey);
						} else {
							// En caso que no se envie en los dato la propiedad 'iterate' no se continuara le peticion de AJAX y cancelar el envio de nuevas filas
							clearInterval(intervalAuRow);
						}

						// Si se ha acabado de crear la nueva subasta. Pues en la primer "auctionrow" no se mostrara el mensaje "Un hotel esta subastando"
						if ($("#auction_first").val() === "1") {
							$("#auction_first").val("0");
							$("#subasta_response_rows").removeClass("d-none");
							$("#subasta_response_rows").append(dataAuct.roomrender);
						} else {
							changeStatusText("Un hotel está subastando en este momento...");
							renderRow($, dataAuct);
						}
					}
				}
			});
		};

		/**
		 * Cuando termine el conteo de la subasta
		 */
		const endIntervalTimer = function() {
			connWebSock.close();
			$.ajax({
				url: backurl + "/auction/alter-auction/" + $("#auctionrow_init").val(),
				success: function() {
					$("#subasta_timer").addClass("d-none");
					changeStatusText("Por favor escoja la oferta que desea tomar.");
				}
			});
		};

		callAjaxSubasta();
		// Inicializar contador para terminar subasta
		if ($("#subasta_timer").val() === "") {
			var count = 150;
			counter = setInterval(function() {
				count = count - 1;
				if (count == -1) {
					endIntervalTimer();
					clearInterval(counter);
					clearInterval(intervalAuRow);
					return;
				}
				var seconds = count % 60;
				var minutes = Math.floor(count / 60);
				minutes %= 60;

				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				$("#subasta_timer").text(minutes + "'" + seconds + '"');
				$("#auction_timestamp").val(count);
				$("#subasta_timer").removeClass("d-none");
			}, 1000);
		}
		intervalAuRow = setInterval(callAjaxSubasta, 11000);
	};

	const calculateDays = function(start, end) {
		var mdy1 = start.split("-");
		var mdy2 = end.split("-");
		var newDate1 = new Date(mdy1[2], mdy1[1], mdy1[0] - 1);
		var newDate2 = new Date(mdy2[2], mdy2[1], mdy2[0] - 1);
		return Math.round((newDate1 - newDate2) / (1000 * 60 * 60 * 24));
	};

	$("#send-subasta").click(function(e) {
		e.preventDefault();

		const showSubastaError = function(text) {
			$("#subastar_error").text(text);
			$("#subastar_error").removeClass("d-none");
		};

		let city = $("#destino").val();
		let fingreso = $("#fecha_ingreso").val();
		let fsalida = $("#fecha_salida").val();
		let personNombre = $("#subasta_nombre").val();
		let personEmail = $("#subasta_email").val();
		let ninos = $("#ninos").val();
		let infantes = $("#infantes").val();
		if (city === "" || city === "none") {
			return showSubastaError("Debe proporcionar una ciudad de destino.");
		} else if (fingreso === "") {
			return showSubastaError("Debe agregar una fecha de salida.");
		} else if (fsalida === "") {
			return showSubastaError("Debe agregar una fecha de llegada.");
		} else if (personNombre === "") {
			return showSubastaError("Debe proporcionar un nombre de titular");
		} else if (personEmail === "") {
			return showSubastaError(
				"Debe proporcionar un correo electrónico de titular."
			);
		}
		$("#subastar_error").addClass("d-none");

		let minPrice = $("#input-with-keypress-0")
			.val()
			.replace(".", "");
		minPrice = minPrice.replace("$", "");
		let maxPrice = $("#input-with-keypress-1")
			.val()
			.replace(".", "");
		maxPrice = maxPrice.replace("$", "");
		$.ajax({
			url: backurl + "/auction/create",
			data: {
				auction_city: $("#destino").val(),
				_token: $("#token_subasta").val(),
				auction_adults: $("#adultos").val(),
				auction_minval: minPrice,
				auction_maxval: maxPrice,
				auction_username: $("#subasta_nombre").val(),
				auction_useremail: $("#subasta_email").val(),
				auction_init: $("#fecha_ingreso").val(),
				auction_end: $("#fecha_salida").val(),
				auction_childrens: ninos,
				auction_infants: infantes,
				auction_days: calculateDays(
					$("#fecha_salida").val(),
					$("#fecha_ingreso").val()
				)
			},
			method: "POST",
			success: function(dataRes) {
				console.log(dataRes);
				if (typeof dataRes.error !== "undefined" && dataRes.error === false) {
					$("#subasta_details_wrap").html(dataRes.details_results);
					$("#auctionModal").addClass("show");
					$("body").css("overflow", "hidden");
					initializeSubasRows($);
					initalizeWebSocket();
				} else {
				}
			}
		});
	});
}

function createNounSlider() {
	var keypressSlider = document.getElementById("slider-snap");
	var input0 = document.getElementById("input-with-keypress-0");
	var input1 = document.getElementById("input-with-keypress-1");
	var inputs = [input0, input1];

	if (keypressSlider) {
		noUiSlider.create(keypressSlider, {
			start: [50000, 100000],
			tooltips: false,
			snap: true,
			connect: true,
			range: {
				min: 10000,
				"10%": 20000,
				"15%": 30000,
				"20%": 40000,
				"25%": 50000,
				"30%": 60000,
				"35%": 70000,
				"40%": 80000,
				"45%": 90000,
				"50%": 100000,
				"55%": 120000,
				"60%": 140000,
				"65%": 160000,
				"70%": 180000,
				"75%": 200000,
				"80%": 220000,
				"85%": 240000,
				"90%": 260000,
				"95%": 300000,
				max: 350000
			},
			/*limit: 600,*/
			format: wNumb({
				decimals: 3,
				thousand: ".",
				prefix: "$"
			})
		});
	}

	keypressSlider.noUiSlider.on("update", function(values, handle) {
		inputs[handle].value = values[handle];
	});

	function setSliderHandle(i, value) {
		var r = [null, null];
		r[i] = value;
		keypressSlider.noUiSlider.set(r);
	}

	// Listen to keydown events on the input field.
	inputs.forEach(function(input, handle) {
		input.addEventListener("change", function() {
			setSliderHandle(handle, this.value);
		});

		input.addEventListener("keydown", function(e) {
			var values = keypressSlider.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var steps = keypressSlider.noUiSlider.steps();

			// [down, up]
			var step = steps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {
				case 13:
					setSliderHandle(handle, this.value);
					break;

				case 38:
					// Get step to go increase slider value (up)
					position = step[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						setSliderHandle(handle, value + position);
					}

					break;

				case 40:
					position = step[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						setSliderHandle(handle, value - position);
					}

					break;
			}
		});
	});
}

function createSlickSliderDestacados() {
	$(".destacados-slider").slick({
		nextArrow: ".destacados-arrow-right",
		prevArrow: ".destacados-arrow-left",
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 2,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 2,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
}

/**
 * Abrir el modal para ingresar/register/olvidar contrasena
 */
function openModalToLogin() {
	$("#toggle_modal_login").click(e => {
		$("#loginModal").addClass("show");
	});
}

/**
 * Cerrar los modales
 */
function closeModal() {
	$(".modal .close, .modal .dimiss_modal").click(e => {
		$(".modal.show").removeClass("show");
		$("body").css("overflow", "visible");
	});
}

/**
 * Ejecutar cuando se click en los botones de el login modal.
 */
function changeBodyLoginModal() {
	$(".modal_login_change_body").click(function(e) {
		e.preventDefault();
		var dataBody = $(this).data("body");
		$(".login_modal_body ").removeClass("show");
		$("#" + dataBody).addClass("show");
		$("#loginModalLabel").text($(this).text());
		$("#modal_login_error").removeClass("show");
		$("#modal_login_success").removeClass("show");
	});
}

/**
 * Cuando se haga submit en el modal de inicio login/registrar/restaurar contrasena
 */
function submitFormsModal() {
	$("#modal_login_form,#modal_register_form,#modal_restpass_form").on(
		"submit",
		function(e) {
			e.preventDefault();
			var actualForm = $(this);
			var showSuccess = function(text) {
				$("#modal_login_error").removeClass("show");
				$("#modal_login_success").addClass("show");
				$("#modal_login_success").text(text);
				$("#loginModal .modal-body").scrollTop(0);
			};
			var showError = function(text) {
				$("#modal_login_success").removeClass("show");
				$("#modal_login_error").addClass("show");
				$("#modal_login_error").text(text);
				$("#loginModal .modal-body").scrollTop(0);
			};
			var validateEmail = function(email) {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(String(email).toLowerCase());
			};

			var sendAjax = function() {
				var urlActual = actualForm.data("url");
				var token = $("#_token").val();
				var data = actualForm.serialize() + "&_token=" + token;
				$.ajax({
					url: urlActual,
					method: "POST",
					data: data,
					success: function(datares) {
						console.log(datares);
						if (datares.success === true) {
							if (typeof datares.redirect !== "undefined") {
								window.location.href = datares.redirect;
							} else {
								showSuccess(datares.message);
								actualForm[0].reset();
							}
						} else {
							showError(datares.message);
						}
					}
				});
			};

			if (actualForm.hasClass("modal_login_form")) {
				let emailLogin = $("#email_login").val();
				let passLogin = $("#password_login").val();

				if (emailLogin === "" || !validateEmail(emailLogin)) {
					showError("Debe ingresar un email válido.");
				} else if (passLogin === "") {
					showError("Ingrese una contraseña");
				} else {
					sendAjax();
				}
			} else if (actualForm.hasClass("modal_register_form")) {
				let nombre = $("#name_register").val();
				let email = $("#email_register").val();
				let password = $("#password_register").val();
				let rpassword = $("#rpassword_register").val();
				if (nombre === "") {
					showError("Debe ingresar un nombre");
				} else if (email === "" || !validateEmail(email)) {
					showError("Debe ingresar un email válido.");
				} else if (password !== rpassword) {
					showError("Las contraseñas no coinciden");
				} else {
					sendAjax();
				}
			} else if (actualForm.hasClass("modal_restpass_form")) {
				let emailReset = $("#email_forgotpassword").val();
				if (emailReset === "" || !validateEmail(emailReset)) {
					showError("Debe ingresar un email válido.");
				} else {
					sendAjax();
				}
			}
		}
	);
}

if ($("#home_body").length) {
	createDatePicker();
	initalizeCamera();
	onCreateSubasta();
	createNounSlider();
	createSlickSliderDestacados();
}

openModalToLogin();
changeBodyLoginModal();
closeModal();
submitFormsModal();
