import { initCamera } from '../../plugins/camera';
const backendUrl = process.env.BACKEND_URL;

/**
 * Funcionalidad para crear un nuevo suscriptor. Esta funcion es llamada en el momento que algunos de los tres forms de "como functiona" se envia.
 * @param {string} name Nombre de el nuevo suscriptor
 * @param {string} email Email del nuevo suscriptor
 * @param {function} callback Callback cuando se ejecuta la funcion. Envia un objeto con error y mensaje.
 */
const sendFormSubscriber = (name, email, callback) => {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if (name === '') {
		callback({
			error: true,
			message: 'Debe ingresar un nombre.'
		});
	} else if (email === '') {
		callback({
			error: true,
			message: 'Debe ingresar un email.'
		});
	}else if (re.test(String(email).toLowerCase())){
			callback({
			error: true,
			message: 'El email ingresado no es válido.'
		});
	} else {
		$.ajax({
			url: backendUrl + '/user/crear-suscripcion',
			method: 'post',
			data: {
				user_email: email,
				user_name: name
			},
			success: function(data) {
				callback(data);
			}
		});
	}
};

/**
 * Cuando alguno de los formularios de "como funciona sea enviado"
 */
function whenSubmitForms() {
	$('#about_form1, #about_form2, #about_form3').submit(function(e) {
		e.preventDefault();
		const currentFormID = $(this).attr('id');
		let button = $('#' + currentFormID + '_button');
		button.addClass('button_load');

		let name = $('#' + currentFormID + '_nombres').val();
		let email = $('#' + currentFormID + '_email').val();
		sendFormSubscriber(name, email, function(res) {
			button.removeClass('button_load');
			let selectorError = '_error';
			let selectorSuccess = '_success';
			if (res.error) {
				$('#' + currentFormID + selectorSuccess).addClass('d-none');
				$('#' + currentFormID + selectorError).removeClass('d-none');
				$('#' + currentFormID + selectorError + ' span').html(res.message);
			} else {
				$('#' + currentFormID + selectorError).addClass('d-none');
				$('#' + currentFormID + selectorSuccess).removeClass('d-none');
				$('#' + currentFormID + selectorSuccess + ' span').html(res.message);
			}
		});
	});
}

function initializeCamera() {
	var onLoad = function() {
		$('.banner-promo').hide();
	};
	var onEndTrans = function() {
		var ind = $('.camera_target .cameraSlide.cameracurrent').index();
		$('#banner-promo-' + ind).show();
	};
	initCamera('.camera_wrap', onLoad, onEndTrans);
}

if ($('#how_works_body').length) {
	initializeCamera();
	whenSubmitForms();
}
