import $ from "jquery";
import "slick-slider/slick/slick";
import { initCamera } from "../../plugins/camera";

const backendURL = process.env.BACKEND_URL;

if ($("#body_ver_plan").length) {
	initalizeTabs();
	initProductSlick("#foto_producto_list", "#fotos_producto_list");
	initializeModalReservation();
	initRoomTabSlick("#slider_images_room");
	initCamera(
		".camera_wrap",
		function() {},
		function() {}
	);
}

/**
 * Inicializar el modal de reserva. Esta función también activara el comportamiento de tabs y los datos dinámicos dependientes del cliente rellenando el formulario
 */
function initializeModalReservation() {
	const validateEmail = email => {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	};

	$(".modal_close button").click(function() {
		$("#modal_reserve").removeClass("show");
	});
	$("#show_reserva_modal").click(function(e) {
		e.preventDefault();
		$("#modal_reserve").addClass("show");
	});

	$(".change_reserv_tab").click(function() {
		let currentTab = $(this).data("tab_reserva");
		$(".change_reserv_tab").removeClass("active");
		$(this).addClass("active");
		$(".only_in_natural,.only_in_company").addClass("field_hidden");
		$(".only_in_" + currentTab).removeClass("field_hidden");
		$("#type_of_client").val(currentTab);
	});

	var testing = false;
	$("#reservar_plan").click(function(e) {
		const showMessage = text => {
			$("#modal_address_error").text(text);
			setTimeout(() => {
				$("#modal_address_error").text("");
			}, 5000);
			return false;
		};

		let nombre = $("#reservation_username").val();
		let email = $("#reservation_useremail").val();
		let phone = $("#reservation_phone").val();
		let city = $("#reservation_city").val();
		let address = $("#reservation_address").val();
		let type_client = $("#type_of_client").val();

		if (type_client == "company") {
			$("#realreserv_doctype").val("nit");
			$("#realreserv_docnumber").val($("#modal_nit").val());
		} else if (type_client == "natural") {
			$("#realreserv_doctype").val($("#modal_type_document").val());
			$("#realreserv_docnumber").val($("#modal_document").val());
		}
		if (
			nombre === "" ||
			email === "" ||
			phone === "" ||
			city === "" ||
			address === ""
		) {
			return showMessage("Todos los campos son obligatorios.");
		} else if (!validateEmail(email)) {
			return showMessage("Debe ingresar un email válido.");
		} else {
			$.ajax({
				// async: false,
				url: backendURL + "/reserva/check-reservation",
				method: "POST",
				data: {
					_token: $("#reservation_token").val(),
					reservation_username: nombre,
					reservation_useremail: email,
					reservation_phone: phone,
					reservation_city: city,
					reservation_address: address,
					reservation_docnumber: $("#realreserv_docnumber").val(),
					reservation_doctype: $("#realreserv_doctype").val(),
					reservation_type: $("#realreserv_type").val(),
					reservation_caseid: $("#reservation_caseid").val()
				},
				success: function(resdata) {
					if (typeof resdata.error !== "undefined" && resdata.error === true) {
						return showMessage(resdata.message);
					} else {
						$("#merchant_id").val(resdata.control_mercant);
						$("#reservation_amount").val(resdata.control_price);
						$("#account_id").val(resdata.control_accountid);
						$("#reference_code").val(resdata.control_reference);
						$("#reservation_currency").val(resdata.control_curency);
						$("#reservation_sign").val(resdata.control_signature);
						$("#reserv_description").val(resdata.control_desc);

						testing = true;

						var formID = $("#reservation-create-form");
						formID.unbind("submit").attr("action", formID.data("action"));
						formID.submit();
					}
				}
			});
		}
		return testing;
	});
}

/**
 * Inicializar las tabs que están en la parte inferior izquierda, como Hotel, Habitación y términos.
 */
function initalizeTabs() {
	$(".tabs li").click(function() {
		let tabID = $(this).data("tab");
		$(".tab-content").removeClass("current");
		$("#" + tabID).addClass("current");
		$(".tab-link").removeClass("current");
		$(this).addClass("current");
		$("#slider_images_room").slick("setPosition", 0);
	});
}

/**
 * Inicializar slick con lista principal y con lista de navegación
 * @param {string} principal Lista donde se mostrará solo una imagen de el producto
 * @param {string} asNav Lista donde se puede seleccionar las diferentes imágenes para ver
 */
function initProductSlick(principal, asNav) {
	$(principal).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		asNavFor: asNav,
		prevArrow: $(".button_slide_product.button_left"),
		nextArrow: $(".button_slide_product.button_right")
	});

	$(asNav).slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: principal,
		dots: false,

		// centerMode: true,
		focusOnSelect: true
	});
}

/**
 * Inicializar el slick de imagenes de habitacion en la pestaña de "Habitación" en hotsale, plan, Subasta, etc.
 * @param {string} selector
 */
function initRoomTabSlick(selector) {
	$(selector).slick({
		slidesToShow: 1,
		lazyLoad: "ondemand",
		slidesToScroll: 1,
		prevArrow: $(".button_slide_room.button_left"),
		nextArrow: $(".button_slide_room.button_right")
	});
}

/**
 * Exportar estas funciones para ser usadas en hotsale, auction, etc.
 */
export {
	initalizeTabs,
	initProductSlick,
	initRoomTabSlick,
	initializeModalReservation
};
