import $ from "jquery";
import InlineEditor from "@ckeditor/ckeditor5-build-inline/build/ckeditor";
import { addDynamicImg } from "../components/add-dynamic-img";

const backurl = process.env.BACKEND_URL;
let editor;

if ($("#create_hotel,#update_hotel").length) {
	InlineEditor.create(document.querySelector("#hotel_description"))
		.then(newEditor => {
			editor = newEditor;
		})
		.catch(error => {
			console.error(error);
		});

	searchCitiesInInputCity({
		inputRealVal: "#hotel_city_real_val",
		inputSelector: "#hotel_city",
		wrapResults: "#hotel_cities_results"
	});
	whenSubmitForm();
	addDynamicImg("#hotel_selectize_imgindx", "#hotel_selectize_imgul", function(
		data
	) {
		console.log(data);
	});
}

function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

/**
 * Cuando se haga submit a el formulario
 */
function whenSubmitForm() {
	$("#hotel-form").submit(e => {
		const showError = text => {
			$("#create_hotel_danger").addClass("show");
			$("#create_hotel_danger").text(text);
			$("body, html").scrollTop($("#create_hotel_danger").offset().top - 20);
		};

		let name = $("#hotel_name").val();
		let city = $("#hotel_city").val();
		let email = $("#hotel_email").val();
		let pass = $("#hotel_password").val();
		let rpass = $("#hotel_rpassword").val();
		let address = $("#hotel_address").val();
		let phone = $("#hotel_phone").val();
		if (
			name === "" ||
			city === "" ||
			email === "" ||
			address === "" ||
			phone === ""
		) {
			showError("Todos los campos son requeridos");
			return false;
		} else if ($("#create_hotel").length) {
			if (pass === "" || rpass === "") {
				showError("Todos los campos son requeridos");
				return false;
			}
		} else if (!validateEmail(email)) {
			showError("Debe ingresar un email válido");
			return false;
		} else if (pass !== rpass) {
			showError("Las contraseñas no coinciden");
			return false;
		}
		const editorData = editor.getData();
		let nameVal = $("#hotel_description").data("name");
		$("<input />")
			.attr("type", "hidden")
			.attr("name", nameVal)
			.attr("value", editorData)
			.appendTo("#hotel-form");
	});
}

/**
 * Inicializar funcionalidad para buscar ciudades por nombre
 */
function searchCitiesInInputCity(options) {
	const initializeClickOnSubitem = () => {
		$(".cities_result").unbind();
		$(".cities_result").click(function(e) {
			e.preventDefault();
			let dataId = $(this).data("cityid");
			let cityName = $(this).text();
			$(options.inputRealVal).val(dataId);
			$(options.inputSelector).val(cityName);
			$(options.wrapResults).removeClass("show");
			options.clickOnItem(dataId);
		});
	};
	var timeOutActive;
	$(options.inputSelector).keyup(function() {
		/**
		 * El coportamiento con timeOutActive servira para hacer la peticion Ajax unicamente si se escribio la ultima tecla en el input 550 Milisegundos atras.
		 */
		clearTimeout(timeOutActive);
		var thisval = $(this).val();
		// Ocultar el div de resultados
		if (thisval === "") {
			$(options.wrapResults).removeClass("show");
			return;
		}
		timeOutActive = setTimeout(function() {
			/**
			 * Obtener las ciudades segun el actual valor de el input Text 'Ciudad'
			 */
			$.ajax({
				url: backurl + "/city/get-by-name/" + thisval,
				success: function(datares) {
					if (datares.length > 0) {
						var newHtml = "";
						for (let i = 0; i < datares.length; i++) {
							let elmn = datares[i];
							newHtml +=
								"<li class='cities_result list-group-item list-group-item-action' data-cityid='" +
								elmn.city_id +
								"'>";
							newHtml += elmn.city_name + ", " + elmn.city_state;
							newHtml += "</li>";
						}
						$(options.wrapResults).html(newHtml);
						$(options.wrapResults).addClass("show");
						initializeClickOnSubitem();
					} else {
						$(options.wrapResults).html(
							"<li class='list-group-item list-group-item-danger'>No se han encontrado resultados</li>"
						);
						$(options.wrapResults).addClass("show");
						// $("#hotel_cities_results").removeClass("show");
					}
				}
			});
		}, 550);
	});
}

export { searchCitiesInInputCity };
