import { showError } from "../base";
import { addCheckbox } from "../components/add-checkbox";

import InlineEditor from "@ckeditor/ckeditor5-build-inline/build/ckeditor";
let editorDesc;
let editorTerms;

if ($("#hotel_config").length) {
	initCKeditor();
	validateFiledsWrong();
	initalizeServicesCheckbox();
}

function initCKeditor() {
	InlineEditor.create(document.querySelector("#hotel_terms"))
		.then(newEditor => {
			editorTerms = newEditor;
		})
		.catch(error => {
			console.error(error);
		});
	InlineEditor.create(document.querySelector("#hotel_description"))
		.then(newEditor => {
			editorDesc = newEditor;
		})
		.catch(error => {
			console.error(error);
		});
}

/**
 * Validad si el envio de formulario existe algun campo erroneo
 */
function validateFiledsWrong() {
	$("#update_hotel_config").on("submit", function() {
		let nombreHotel = $("#hotel_name").val();
		let password = $("#hotel_password").val();
		let rpassword = $("#hotel_respassword").val();
		let qrooms = $("#hotel_rooms").val();
		let address = $("#hotel_address").val();
		let phone = $("#hotel_phone").val();

		if (nombreHotel === "") {
			return showError("Debe ingresar un nombre de hotel válido");
		} else if (password !== rpassword) {
			return showError("Las contraseñas no coinciden");
		} else if (qrooms === "") {
			return showError("Debe ingresar una cantidad de habitaciones válida");
		} else if (address === "") {
			return showError("Debe ingresar una dirección válida.");
		} else if (phone === "") {
			return showError("Debe ingresar un número de teléfono válido.");
		}

		const editorDataTerms = editorTerms.getData();
		const editorDataDesc = editorDesc.getData();
		let nameValTerms = $("#hotel_terms").data("name");
		let nameValDesc = $("#hotel_description").data("name");
		$("<input />")
			.attr("type", "hidden")
			.attr("name", nameValTerms)
			.attr("value", editorDataTerms)
			.appendTo("#update_hotel_config");
		$("<input />")
			.attr("type", "hidden")
			.attr("name", nameValDesc)
			.attr("value", editorDataDesc)
			.appendTo("#update_hotel_config");
	});
}

function initalizeServicesCheckbox() {
	addCheckbox("#label_ihotel_services", {
		wrapItems: ".prop_list_wrap_hotel_services",
		wrapText: ".input_hotel_services_wrap",
		inputText: "#add_itext_hotel_services",
		buttonAdd: "#add_i_ibtn_hotel_services",
		buttonCancel: "#cancel_ibtn_hotel_services",
		classNewItem: "col-md-6 single_col_hotel_services",
		attrName: "hotel_services"
	});
}
