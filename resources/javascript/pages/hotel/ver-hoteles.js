import { paginator } from "../components/paginator";
import { fileringSelects } from "../components/fitering-selects";

const siteURL = process.env.SITE_URL;

if ($("#all_hotels").length) {
	initializePaginator();
	initializeSelectFiltering();
}

function initializePaginator() {
	paginator({
		order: [
			"hotel_name",
			"city_name",
			"hotel_phone",
			"hotel_date",
			"hotel_status"
		],
		eachTr: function(singleTr) {
			let stringURL = siteURL + "/hotel/editar-hotel/" + singleTr["hotel_id"];
			return `<td class="" style='width: 5%;'>
								<a target='_blank' class="view mr-2" title="Editar" href="${stringURL}">
									<i class="fas fa-pencil-alt"></i>
								</a>
							</td>`;
		}
	});
}

function initializeSelectFiltering() {
	fileringSelects("#subastas_filtro", ".select_filtering");
}
