import $ from "jquery";

import "jquery-ui/ui/core";
import "jquery-ui/ui/widgets/datepicker";

import { addCheckbox } from "../components/add-checkbox";

import InlineEditor from "@ckeditor/ckeditor5-build-inline/build/ckeditor";
import { searchCitiesInInputCity } from "../hotel/create-hotel";
import { datePickerConfig } from "../base";
const backurl = process.env.BACKEND_URL;

let descriptionEditor;
let termsEditor;

if ($("#hotsale-form").length) {
	InlineEditor.create(document.querySelector("#hotsale_description"))
		.then(editor => {
			descriptionEditor = editor;
		})
		.catch(error => {
			console.error(error);
		});
	InlineEditor.create(document.querySelector("#hotsale_terms"))
		.then(editor => {
			termsEditor = editor;
		})
		.catch(error => {
			console.error(error);
		});

	searchCitiesInInputCity({
		inputRealVal: "#hotsale_city_real_val",
		inputSelector: "#hotsale_city",
		wrapResults: "#hotsale_cities_results",
		clickOnItem: cityID => {
			initHotelsByCityID(cityID, "#hotsale_hotel_name", "#hotsale_room");
		}
	});
	initializeDatePicker();
	onSubmitFormCreateHotsale();
	initializeChecbox();
}

/**
 * Cuando se haga submit en el formulario de hotsale
 */
function onSubmitFormCreateHotsale() {
	const showError = text => {
		$("#create_hotsale_danger").addClass("show");
		$("#create_hotsale_danger").text(text);
		$("body, html").scrollTop($("#create_hotsale_danger").offset().top - 20);
	};

	$("#hotsale-form").on("submit", e => {
		let hotsale_name = $("#hotsale_name").val();
		let hotsale_city = $("#hotsale_city").val();
		let hotsale_hotel_name = $("#hotsale_hotel_name").val();
		let hotsale_room = $("#hotsale_room").val();
		let hotsale_price = $("#hotsale_price").val();
		let hotsale_currency = $("#hotsale_currency").val();
		let hotsale_discount = $("#hotsale_discount").val();
		let hotsale_mindate = $("#hotsale_mindate").val();
		let hotsale_maxdate = $("#hotsale_maxdate").val();

		if (
			hotsale_name === "" ||
			hotsale_city === "" ||
			hotsale_hotel_name === "" ||
			hotsale_hotel_name === "none" ||
			hotsale_room === "" ||
			hotsale_room === "none" ||
			hotsale_price === "" ||
			hotsale_currency === "" ||
			hotsale_discount === "" ||
			hotsale_mindate === "" ||
			hotsale_maxdate === ""
		) {
			showError("Hay campos obligatorios que deben ser completados");
			return false;
		}

		const descData = descriptionEditor.getData();
		const termsData = termsEditor.getData();
		let descName = $("#hotsale_description").data("name");
		$("<input />")
			.attr("type", "hidden")
			.attr("name", descName)
			.attr("value", descData)
			.appendTo("#hotsale-form");
		let termName = $("#hotsale_terms").data("name");
		$("<input />")
			.attr("type", "hidden")
			.attr("name", termName)
			.attr("value", termsData)
			.appendTo("#hotsale-form");
	});
}

function initializeDatePicker() {
	$.datepicker.regional["es"] = datePickerConfig;
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$(function() {
		$("#hotsale_mindate").datepicker({
			beforeShow: function(input, inst) {
				setTimeout(function() {
					inst.dpDiv.css({
						top: $("#hotsale_mindate").offset().top + 35,
						left: $("#hotsale_mindate").offset().left
					});
				}, 0);
			}
		});
		$("#hotsale_maxdate").datepicker({
			beforeShow: function(input, inst) {
				setTimeout(function() {
					inst.dpDiv.css({
						top: $("#hotsale_maxdate").offset().top + 35,
						left: $("#hotsale_maxdate").offset().left
					});
				}, 0);
			}
		});
	});
}

/**
 * Inicializar la obtencion de todas las habitaciones por el cambio de los hoteles mostrados.
 * @param {string} selectInit Selector de elemento que espera el evento 'change'
 * @param {string} roomsWrap Selector de el select que se mostraran todas las habitaciones.
 */
const initRoomsByHotelID = (selectInit, roomsWrap) => {
	$(selectInit).unbind("change");
	$(selectInit).change(function() {
		let currentVal = $(this).val();
		if (currentVal == "none") return;
		console.log(currentVal);
		$.ajax({
			url: backurl + "/room/get-by-hotel-id/" + currentVal,
			method: "GET",
			success: datares => {
				let newHtml = '<option value="none">Seleccione</option>';
				datares.forEach(elm => {
					newHtml +=
						'<option value="' +
						elm.room_id +
						'">' +
						elm.room_name +
						"</option>";
				});
				$(roomsWrap).html(newHtml);
				$(roomsWrap).removeAttr("disabled");
			}
		});
	});
};

function initializeChecbox() {
	addCheckbox("#service_add", {
		wrapItems: ".prop_list_wrap_services",
		wrapText: ".add_service_wrap",
		inputText: "#add_service_text",
		buttonAdd: "#add_service_button",
		buttonCancel: "#cancel_service_button",
		classNewItem: "col-md-6 list_item_eqroom",
		attrName: "hotsale_services"
	});
}

/**
 * Inicializar la obtecion de todos los hoteles segun la ciudad seleccionada
 * @param {string|int} cityID Ciudad de los hoteles que se quieren obtener
 * @param {string} hotelsSelect Elemento select que se desea agregar los hoteles obtenidos
 * @param {string} roomsSelect Elemento select que se desea agregar las habitaciones
 */
const initHotelsByCityID = (cityID, hotelsSelect, roomsSelect) => {
	$.ajax({
		url: backurl + "/hotel/get-by-city-id/" + cityID,
		success: function(datares) {
			if (datares.length > 0) {
				var newHtml = "";
				newHtml += "<option value='none'>Seleccione</option>";
				for (let i = 0; i < datares.length; i++) {
					const element = datares[i];
					newHtml += '<option value="' + element.hotel_id + '">';
					newHtml += element.hotel_name;
					newHtml += "</option>";
				}
				$(hotelsSelect).html(newHtml);
				$(hotelsSelect).removeAttr("disabled");
				initRoomsByHotelID(hotelsSelect, roomsSelect);
			} else {
				$(hotelsSelect).attr("disabled", "disabled");
			}
		}
	});
};

export { initRoomsByHotelID, initHotelsByCityID };
