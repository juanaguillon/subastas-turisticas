/**
 * Este archivo contendra multiples configuraciones y funciones que seran usadas en multiples partes de la aplicacion, en administrador, el el sitio o en ambas.
 * Aqui puede agregar funciones para agregar funciones reutilizables.
 */
import $ from "jquery";

export const datePickerConfig = {
	closeText: "Cerrar",
	prevText: "< Ant",
	nextText: "Sig >",
	currentText: "Hoy",
	monthNames: [
		"Enero",
		"Febrero",
		"Marzo",
		"Abril",
		"Mayo",
		"Junio",
		"Julio",
		"Agosto",
		"Septiembre",
		"Octubre",
		"Noviembre",
		"Diciembre"
	],
	monthNamesShort: [
		"Ene",
		"Feb",
		"Mar",
		"Abr",
		"May",
		"Jun",
		"Jul",
		"Ago",
		"Sep",
		"Oct",
		"Nov",
		"Dic"
	],
	dayNames: [
		"Domingo",
		"Lunes",
		"Martes",
		"Miércoles",
		"Jueves",
		"Viernes",
		"Sábado"
	],
	dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Juv", "Vie", "Sáb"],
	dayNamesMin: ["Do", "Lu", "Ma", "Mié", "Ju", "Vi", "Sáb"],
	weekHeader: "Sm",
	dateFormat: "dd-mm-yy",
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: "",
	minDate: 0
};

/**
 * Renderizar una nueva fila de oferta
 */
export const renderRow = (jQuery, data) => {
	jQuery("#subasta_text").removeClass("d-none");
	setTimeout(() => {
		jQuery("#subasta_text").addClass("d-none");
		jQuery("#subasta_response_rows").removeClass("d-none");
		if (
			jQuery("#subasta_response_rows tr").length ==
			jQuery("#auctionrow_maxhotels").val()
		) {
			jQuery("#subasta_response_rows tr:first").remove();
			jQuery("#subasta_response_rows").append(data.roomrender);
		} else {
			jQuery("#subasta_response_rows").append(data.roomrender);
		}
	}, 3500);
};

/**
 * Mostrar un error. Debe tener en cuenta que para mostrar este error, debe estar el elemento .form_alert_toggle en el DOM, que sera el contenedor donde se agregara la alerta error.
 * @param {string} text Texto informativo del error
 */
export const showError = function(text) {
	let elementErr = $(".form_alert_toggle");
	elementErr.text(text);
	elementErr.addClass("show");

	$("html, body").animate(
		{
			scrollTop: $(".form_alert_toggle").offset().top - 10
		},
		400
	);
	return false;
};
