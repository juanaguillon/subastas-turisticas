import { paginator } from "../components/paginator";
import { fileringSelects } from "../components/fitering-selects";

const siteURL = process.env.SITE_URL;

if ($("#ver_reservas").length) {
	initializePaginator();
	initializeSelectFiltering();
}

function initializePaginator() {
	paginator({
		order: [
			"city_name",
			"reservation_date",
			"reservation_price",
			"reservation_statustext"
		],
		eachTr: function(singleTr) {
			console.log(singleTr);
			let pdfURL =
				siteURL + "/reserva/ver-reserva-pdf/" + singleTr["reservation_id"];
			let linkURL =
				siteURL + "/reserva/ver-reserva/" + singleTr["reservation_id"];
			return `<td class="table_actions" style="width:7%">
                  <button class="button button_small button_white mb-0 show_action_list no_line">
                    <i class="fa fa-ellipsis-h"></i>
                    <div class="actions_list_wrap">
                      <ul class="actions_list">
                        <li>
                          <a href="${pdfURL}.pdf" target="blank">
                            <i class="fa fa-download mr-2"></i>
                            <span>Descargar PDF</span>
                          </a>
                        </li>
                        <li>
                          <a target="_blank" href="${linkURL}">
                            <i class="fa fa-search mr-2"></i>
                            <span>Visualizar</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </button>
                </td>`;
		}
	});
}

function initializeSelectFiltering() {
	fileringSelects("#subastas_filtro", ".select_filtering");
}
