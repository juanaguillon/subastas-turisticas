/**
 * El codigo actual se importara unicamente cuando este en el sitio.
 */
export * from "./site/home";
export * from "./site/ver-plan";
export * from "./site/ver-hotsale";
export * from "./site/ver-auctionrow";
export * from "./site/como-funciona";

$(window).scroll(function() {
	let st = $(window).scrollTop();
	if (st > 155) {
		$("header#header_site").addClass("fixed");
	} else {
		$("header#header_site").removeClass("fixed");
	}
});

$("#div_search button").click(e => {
	e.stopPropagation();
	$("#search_in_mobile").toggleClass("active");
});

$("#search_in_mobile").click(e => e.stopPropagation());

$("body").click(() => {
	$("#search_in_mobile").removeClass("active");
});
