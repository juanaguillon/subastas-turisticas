/**
 * El codigo actual se importara unicamente cuando este en el administrador.
 */
import $ from "jquery";

import { showError } from "./base";
import { addDynamicImg } from "./components/add-dynamic-img";


export * from "./user/configuracion-usuario";

export * from "./hotel/create-hotel";
export * from "./hotel/ver-hoteles";
export * from "./hotel/configuracion";

export * from "./hotsale/create-hotsale";
export * from "./hotsale/ver-hotsales";

export * from "./plan/create-plan";
export * from "./plan/ver-planes";

export * from "./room/create-room";
export * from "./room/ver-habitaciones";

export * from "./auctionrow/update-auctionrow";

export * from "./auction/cliente-auctions";

export * from "./reservation/cliente-reservas";
export * from "./reservation/admin-reservas";
export * from "./reservation/hotel-reservas";


/** Permitir selecciones de imagenes en hoteles, habitaciones, planes etc. */
addDynamicImg("#hotel_selectize_img", "#hotel_selectize_imgul", function(
	error
) {
	showError(error.message);
});

/** Permitir arrastrar y soltar en las imagenes de hoteles, habitaciones, etc. */
$("#hotel_selectize_imgul").sortable({
	items: "li:not(.hotel_image_new)",
	placeholder: "hotel_division",
	axis: "x",
	stop: function(e, ui) {
		var stackIndexs = [];
		$("#hotel_selectize_imgul li.dyn_img_item").each(function(e) {
			stackIndexs.push($(this).data("index"));
		});
		$("#hotel_selectize_imgindx").val(stackIndexs.join(","));
	}
});

/**
 * En algunas pantallas, como "Mis Reservas" al costado derecho de cada fila, se ve el botón para expandir acciones de reserva ( Descargar PDF, Ver Reserva)
 * El siguiente codigo hará funcionar este listado.
 */
function initAuctionListClicked() {
	$(".show_action_list").click(function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(".actions_list_wrap").removeClass("show");
		$(this)
			.children(".actions_list_wrap")
			.addClass("show");
	});
	$(".actions_list_wrap").click(function(e) {
		e.stopPropagation();
	});
}

/**
 * Que se ocultará cuando clickee el body.
 * Esto puede ser útil para ocultar elementos que se están mostrando
 */
function whenClickOnBody() {
	$("body").click(function() {
		$(".actions_list_wrap").removeClass("show");
	});
}

window.onload = function() {
	initAuctionListClicked();
	whenClickOnBody();
};
