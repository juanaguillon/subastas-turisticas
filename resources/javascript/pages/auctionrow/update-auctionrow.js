import $ from "jquery";
import { renderRow } from "../base";

const backendurl = process.env.BACKEND_URL;
const socketPort = process.env.WEBSOCKET_PORT;
const socketUrl = process.env.WEBSOCKET_URL + ":" + socketPort;

// Obtener el auctionID para obtener el/los ultimo(s) auctionRows
let auctionID = $("#auctionrow_init").val();
const callAjaxSubasta = function() {
  $.ajax({
    url: backendurl + "/auctionrow/get-last-by-auction-id/" + auctionID,
    success: function(datares) {
      renderRow($, datares);
    }
  });
};

$("#subastar_auction").click(function(e) {
  const showError = function(text) {
    $("#subastar_auction_error").text(text);
    $("#subastar_auction_error").removeClass("d-none");
    $("#subastar_auction_info").addClass("d-none");
  };
  const showInfo = function(text) {
    $("#subastar_auction_info").text(text);
    $("#subastar_auction_info").removeClass("d-none");
    $("#subastar_auction_error").addClass("d-none");
  };
  e.preventDefault();

  let roomID = $("#subastar_room_id").val();
  let price = $("#subastar_number").val();
  if (roomID === "" || roomID === "none") {
    showError("Debe seleccionar una habitación");
    return false;
  } else if (price === "" || price < 1000) {
    showError("Debe agregar un precio válido a la nueva oferta.");
    return false;
  }

  $.ajax({
    url: backendurl + "/auctionrow/agregar-fila",
    method: "POST",
    data: {
      _token: $("#auction_token").val(),
      auctionrow_room: roomID,
      auctionrow_price: price,
      auctionrow_auction: auctionID,
      auctionrow_time: $("#subasta_timer").data("timeix")
    },
    success: darares => {
      if (darares.error === true) {
        showError(datares.message);
      } else {
        showInfo("Se ha actualizado correctamente su oferta");
        $.get({
          url: backendurl + "/auctionrow/get-last-by-auction-id/" + auctionID,
          success: function(datares2) {
            renderRow($, datares2);
          }
        });
        let connWebSock2 = new WebSocket(socketUrl);
        connWebSock2.onmessage = function(e) {
          console.log(e.data);
        };
        connWebSock2.onopen = function(e) {
          console.log("Connection established!");
          let messages = {
            caseSub: "auctionSet",
            auctionID: auctionID
          };
          connWebSock2.send(JSON.stringify(messages));
        };
      }
    }
  });
});
if ($("#box_update_auctionrow").length) {
  /**
   * Verificar si la actual subasta esta activa.
   */
  if ($("#getting_auctions").length) {
    $.ajax({
      url: backendurl + "/auctionrow/get-last-by-auction-id/" + auctionID,
      success: function(datares) {
        var count = datares.data.auctionrow_time;
        var counter = setInterval(function() {
          count = count - 1;
          if (count == -1) {
            $("#subasta_timer").addClass("d-none");
            clearInterval(counter);
            clearInterval(intervalAuRow);
            return;
          }
          var seconds = count % 60;
          var minutes = Math.floor(count / 60);
          minutes %= 60;

          minutes = minutes < 10 ? "0" + minutes : minutes;
          seconds = seconds < 10 ? "0" + seconds : seconds;

          $("#subasta_timer").text(minutes + "'" + seconds + '"');
          $("#subasta_timer")[0].setAttribute("data-timeix", count);
          $("#subasta_timer").removeClass("d-none");
        }, 1000);
        var intervalAuRow = setInterval(callAjaxSubasta, 11000);
      }
    });
  }

  $.ajax({
    url: backendurl + "/auctionrow/get-lastests/" + auctionID,
    success: function(datares) {
      $("#subasta_response_rows").html(datares.render);
      $("#subasta_response_rows").removeClass("d-none");
    }
  });
}
