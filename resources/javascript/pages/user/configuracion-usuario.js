import { showError } from "../base";

if ($("#user_config").length) {
	onSubmitForm();
}

/**
 * Cuando el formulario de configuracion de usuario haga submit.
 */
function onSubmitForm() {
	let userForm = $("#user-form-config");

	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	userForm.on("submit", function() {
		let name = $("#user_name").val();
		let email = $("#user_email").val();
		let password = $("#user_password").val();
		let rpassword = $("#user_rpassword").val();

		if (name === "") {
			return showError("Debe proporcionar un nombre de usuario.");
		} else if (email === "") {
			return showError("Debe proporcionar un email de usuario.");
		} else if (!validateEmail(email)) {
			return showError("Debe proporcionar un email de válido.");
		} else if (password !== rpassword) {
			return showError("Las contraseñas no coinciden");
		}
	});
}
