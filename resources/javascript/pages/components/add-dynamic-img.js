/**
 * En el administrador, multiples veces debemos administrar imagenes dinamicamente.
 * Este componente nos permitira inicializar de una manera sencilla
 */
import $ from "jquery";

export const addDynamicImg = function(
  selectorInput,
  selectorWrap,
  errorCallback
) {
  function handleFileSelect(event, output) {
    //Check File API support
    if (window.File && window.FileList && window.FileReader) {
      var files = event.target.files; //FileList object
      var orderIndex = [];

      if (files.length > 5) {
        errorCallback({
          error: true,
          code: "images_limit",
          message: "No puede ingresar mas de cinco imagenes."
        });
        return;
      }

      for (var i = 0; i < files.length; i++) {
        orderIndex.push(i);
        var file = files[i];
        if (!file.type.match("image")) continue;
        var picReader = new FileReader();
        picReader.index = i;
        picReader.onload = function(e) {
          var picFile = e.target;
          let newHtml = `
          <li data-index="${this.index}" class="hotel_image_item dyn_img_item">
            <div class="imgHotel" >
              <img src="${picFile.result}" alt="foto" >
              <button class="button button_remove_img button_red button_small">
                <i class="fa fa-times"></i>
              </button> 
            </div>
          </li>`;
          output.prepend(newHtml);

          // Permitir que los botones de eliminar imagen funciones correctamente.
          $(".button_remove_img").unbind("click");
          $(".button_remove_img").click(function(e) {
            e.preventDefault();
            $(this)
              .closest(".dyn_img_item")
              .remove();

            var stackIndexs = [];
            $(selectorWrap + " li.dyn_img_item").each(function(e) {
              stackIndexs.push($(this).data("index"));
            });
            $("#hotel_selectize_imgindx").val(stackIndexs.join(","));
          });
        };
        //Read the image
        picReader.readAsDataURL(file);
      }
      $("#hotel_selectize_imgindx").val(orderIndex.reverse().join(","));
    } else {
      alert("Su navegador no permite agregar imágenes.");
    }
  }
  $(selectorInput).change(function(e) {
    let output = $(selectorWrap);
    output.children(".dyn_img_item").remove();
    handleFileSelect(e, output);
  });
};
