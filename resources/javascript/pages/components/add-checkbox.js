/**
 * En multiples ocasiones, debemos agregar un servicio adicional, un obsquio, equipamiento, etc, en partes de el administrador
 * Este componente permitira de manera facil integrar ese comportamiento
 */

import $ from "jquery";


/**
 * Cuando se de enter en el input:text o en el boton de agregar
 */
const onAddCheckboxEvent = function(e, id, _options) {
  let wrapItems = $(_options.wrapItems);
  let classNewItem = _options.classNewItem;
  let attrName = _options.attrName;
  let wrapText = $(_options.wrapText);
  let inputText = $(_options.inputText);

  e.preventDefault();
  let alertDiv = wrapText.children(".add_checkbox_alert");
  if (inputText.val() === "") {
    alertDiv.removeClass("d-none");
    alertDiv.children("span").text("No puede dejar el campo vacio");
    return;
  } else {
    alertDiv.addClass("d-none");
    let newVal = inputText.val();
    let idx = wrapItems.children().length;
    let htmlNewItem = `
        <div class='${classNewItem}'>
          <div class="checkbox_custom">
            <div class="checkbox_wrap">
              <input id="${id +
                idx}" checked='checked' name="${attrName}[]" type="checkbox" class="form-check-input" value="${newVal}">
              <i class="fa fa-check"></i>
            </div>
            <label class="ml-2" for="${id + idx}">${newVal}</label>
          </div>
        </div>
      `;
    inputText.val("");
    wrapItems.append(htmlNewItem);
  }
};

/**
 * @param {string} inputSelector Input:checkbox Selector en donde se ejecutara el comportamiento
 * @param {object} options Otras opciones disponibles para personalizar el addCheckbox
 * @param {object} options.wrapItems Selector de contenedor donde ira los nuevos checkbox.
 * @param {object} options.wrapText Selector de contenedor con el input Text, agregar y cancelar operacion.
 * @param {object} options.inputText Selector de input:text a procesar.
 * @param {object} options.buttonAdd Selector boton agregar
 * @param {object} options.buttonCancel Selector boton cancelar
 * @param {string} options.classNewItem Clase CSS para el contenedor de el nuevo item a agregar
 * @param {string} options.attrName Atributo 'name' del nuevo checkbox para guardar.
 */
export const addCheckbox = function(checkboxSelector, options) {
  let checkbox = $(checkboxSelector);
  let wrapText = $(options.wrapText);
  let inputText = $(options.inputText);
  let buttonAdd = $(options.buttonAdd);
  let buttonCancel = $(options.buttonCancel);

  // Se usara esta clase cuando este no este checkeado.
  let classOnUnchecked = "d-none";
  wrapText.addClass(classOnUnchecked);
  wrapText.prepend(
    '<div class="add_checkbox_alert d-none"><span class="text-danger"></span></div>'
  );
  checkbox.on("change", function(e) {
    if ($(this).is(":checked")) {
      wrapText.removeClass(classOnUnchecked);
    } else {
      wrapText.addClass(classOnUnchecked);
    }
  });

  buttonAdd.click(function(e) {
    onAddCheckboxEvent(e, checkboxSelector, options);
  });
  inputText.keydown(function(e) {
    if (e.keyCode == 13) {
      onAddCheckboxEvent(e, checkboxSelector, options);
    }
  });

  buttonCancel.click(function(e) {
    e.preventDefault();
    wrapText.addClass(classOnUnchecked);
    inputText.val("");
    checkbox[0].checked = false;
  });
};
