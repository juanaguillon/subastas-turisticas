const $ = require("jquery");
window.jquery = $;
window.jQuery = $;
window.$ = jQuery;

const pagination = require("paginationjs");
const backendUrl = process.env.BACKEND_URL;

Array.prototype.remove = function() {
	var what,
		a = arguments,
		L = a.length,
		ax;
	while (L && this.length) {
		what = a[--L];
		while ((ax = this.indexOf(what)) !== -1) {
			this.splice(ax, 1);
		}
	}
	return this;
};

function initMoreActions() {
	$(".show_action_list").unbind("click");
	$(".show_action_list").click(function(e) {
		e.stopPropagation();
		$(".actions_list_wrap").removeClass("show");
		$(this)
			.children(".actions_list_wrap")
			.toggleClass("show");
	});
}

function initPaginator(options) {
	var dataPostSend = {
		dataSource: $("#url_paginator").val(),
		locator: "items",
		totalNumber: $("#total_paginator").val(),
		ulClassName: "ul_paginator",
		activeClassName: "active",
		pageSize: $("#limit_paginator").val(),
		ajax: {
			beforeSend: function() {
				$(".tablegen-loading").addClass("show");
			}
		},
		alias: {
			pageNumber: "start",
			pageSize: "count"
		},

		callback: function(data, pagination) {
			$(".tablegen-loading").removeClass("show");

			let html = setTrsPaginator(data, options);
			$("#tbody_results").html(html);
			var newStarter =
				parseInt(pagination.pageSize) * (pagination.pageNumber - 1);
			$("#start_paginator").val(newStarter);
			initMoreActions();
			// options.callback(data, pagination);
		}
	};

	dataPostSend.dataSource = $("#url_paginator").val();
	$("#paginator_container").pagination(dataPostSend);
}

/**
 * Inicializar el paginador con trs
 * @param {Array} eachData
 * @param {Object} options
 * @param {Object} options.except Evitar usar algunos keys, esto sera bueno para no mostrar información que el servidor envía
 * @param {Object} options.include Mostrar únicamente las keys que se han seleccionado
 * @param {Object} options.order Que keys y que orden mostrar. Si se agrega este, except ni include se usarán
 * @param {Function} options.eachTr Puede usar este callback para cambiar el final
 */
const setTrsPaginator = function(eachData, options = {}) {
	let keys = Object.keys(eachData[0]);

	if (typeof options.order !== "undefined") {
		const keys__ = [];
		for (let i = 0; i < options.order.length; i++) {
			const keyORder = options.order[i];
			if (keys.includes(keyORder)) {
				keys__.push(keyORder);
			}
		}
		keys = keys__;
	} else {
		if (typeof options.except !== "undefined") {
			let deletedKeys = options.except.split(",");
			deletedKeys.forEach(i => {
				keys.remove(i.trim());
			});
		}

		if (typeof options.include !== "undefined") {
			let includeKeys = options.include.split(",");
			let incKeys_ = [];
			includeKeys.forEach(inckey => {
				inckey = inckey.trim();
				if (keys.includes(inckey)) {
					incKeys_.push(inckey);
				}
			});
			keys = incKeys_;
		}
	}

	let totalHTML = "";
	for (let i = 0; i < eachData.length; i++) {
		const tr = eachData[i];
		totalHTML += `<tr>`;
		keys.forEach(e => {
			totalHTML += `<td>${tr[e]}</td>`;
		});

		if (typeof options.eachTr !== "undefined") {
			totalHTML += options.eachTr(tr);
		}

		totalHTML += `</tr>`;
	}
	return totalHTML;
};

// var a = [
// 	{
// 		primer: "Primeritem",
// 		primer2: "Primeritem2",
// 		primer3: "Primeritem3",
// 		primer4: "Primeritem4"
// 	},
// 	{
// 		primer: "XPrimeritem1",
// 		primer2: "XPrimeritem2",
// 		primer3: "XPrimeritem3",
// 		primer4: "XPrimeritem4"
// 	},
// 	{
// 		primer: "POPrimeritem1",
// 		primer2: "POPrimeritem2",
// 		primer3: "POPrimeritem3",
// 		primer4: "POPrimeritem4"
// 	}
// ];
// setTrsPaginator(a, {
// 	include: "primer, primer4"
// });

/**
 * Inicializar el paginador
 * @param {Object} options
 * @param {Object} options.except Evitar usar algunos keys, esto sera bueno para no mostrar información que el servidor envía
 * @param {Object} options.include Mostrar únicamente las keys que se han seleccionado
 * @param {Object} options.order Que keys y que orden mostrar. Si se agrega este, except ni include se usarán
 * @param {Function} options.eachTr Puede usar este callback para cambiar el final
 * @param {Function} options.callback Que se va a ejecutar cuando se complete el llamado de páginas.
 */
export const paginator = function(options = {}) {
	initPaginator(options);
	// $("#paginator_container").pagination(dataPostSend);
	// initMoreActions();
	$(".order_column_th").click(function(e) {
		e.preventDefault();

		let orderBY = $(this).data("oby");
		let prevURL = new URL($("#url_paginator").val());
		let siteURL = process.env.SITE_URL + prevURL.pathname

		let newParams = new URLSearchParams(prevURL.search.slice(1));
		newParams.set("orderby", orderBY);
		if ($(this).hasClass("asc")) {
			$(".order_column_th").removeClass("asc desc");
			$(this).addClass("desc");
			$(this).removeClass("asc");
			newParams.set("order", "DESC");
		} else {
			$(".order_column_th").removeClass("asc desc");
			$(this).addClass("asc");
			$(this).removeClass("desc");
			newParams.set("order", "ASC");
		}

		$("#url_paginator").val(siteURL + "?" + newParams.toString());

		initPaginator(options);
	});

	$("#quantity_screen_pag").change(function() {
		$("#limit_paginator").val($(this).val());
		initPaginator(options);
	});
	$("#selects_filter_paginator").click(function() {
		initPaginator(options);
	});
};
