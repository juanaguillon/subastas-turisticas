import $ from "jquery";

export const fileringSelects = function(selector, childsSelectsSelector) {
	let selectEl = $(selector);

	/**
	 * Tenga en cuenta, que el click de "FILTRAR", está en el archivo "Paginator.js" Ya que el filtro y el paginador trabajan en conjunto
	 */
	selectEl.change(function(e) {
		let otherSelectToShow = $(this).val();
		$(".select_filtering").removeClass("show");
		$("#" + otherSelectToShow).addClass("show");
	});

	$(childsSelectsSelector).change(function(e) {

		let prevURL = new URL($("#url_paginator").val());
		let siteURL = process.env.SITE_URL + prevURL.pathname;
		
		
		let newURL = new URLSearchParams();
		newURL.set($(this).data("name"), $(this).val());
		let newURLString = siteURL + "?" + newURL.toString();

		let counter = $(this)
			.children("option:selected")
			.data("count");
		$("#url_paginator").val(newURLString);
		$("#total_paginator").val(counter);
	});
};
