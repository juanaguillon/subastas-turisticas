import { paginator } from "../components/paginator";
import { fileringSelects } from "../components/fitering-selects";

const siteURL = process.env.SITE_URL;

if ($("#all_rooms").length) {
	initializePaginator();
	initializeSelectFiltering();
}

function initializePaginator() {
	paginator({
		order: [
			"room_name",
			"room_adults",
			"room_minprice",
			"room_maxprice",
			"room_active",
			"room_created"
		],
		eachTr: function(singleTr) {
			let stringURL =
        siteURL + "/room/editar-habitacion/" + singleTr["room_id"];
        console.log(singleTr)
			return `<td class="" style='width: 5%;'>
								<a target='_blank' class="view mr-2" title="Editar" href="${stringURL}">
									<i class="fas fa-pencil-alt"></i>
								</a>
							</td>`;
		}
	});
}

function initializeSelectFiltering() {
	fileringSelects("#subastas_filtro", ".select_filtering");
}
