import $ from "jquery";
import * as InlineEditor from "@ckeditor/ckeditor5-build-inline/build/ckeditor";
import { addCheckbox } from "../components/add-checkbox";
import { addDynamicImg } from "../components/add-dynamic-img";
import "jquery-ui/ui/widgets/sortable";

let roomDescriptionEditor;
let roomTermsEditor;

if ($("#room-form").length) {
  InlineEditor.create(document.querySelector("#room_description"))
    .then(newEditor => {
      roomDescriptionEditor = newEditor;
    })
    .catch(error => {
      console.error(error);
    });

  InlineEditor.create(document.querySelector("#room_terms"))
    .then(newEditor => {
      roomTermsEditor = newEditor;
    })
    .catch(error => {
      console.error(error);
    });

  onSubmitFormRoom();
  initializeMoreCheckbox();
}

const showError = text => {
  $("#create_room_danger").addClass("show");
  $("#create_room_danger").text(text);
  $("body, html").scrollTop($("#create_room_danger").offset().top - 20);
};

/**
 * Cuando se haga submit en el formulario de habitacion
 */
function onSubmitFormRoom() {
  $("#room-form").submit(function(e) {
    let room_name = $("#room_name").val();
    let room_minprice = $("#room_minprice").val();
    let room_maxprice = $("#room_maxprice").val();
    let room_meters = $("#room_meters").val();

    if (
      room_name === "" ||
      room_minprice === "" ||
      room_maxprice === "" ||
      room_meters === ""
    ) {
      showError("Hay campos obligatorios que no se han completado.");
      return false;
    } else if (room_minprice < 1000 || room_maxprice < 1000) {
      showError("Ingrese precios válidos.");
      return false;
    }

    let roomDescription = roomDescriptionEditor.getData();
    let nameValDesc = $("#room_description").data("name");
    $("<input />")
      .attr("type", "hidden")
      .attr("name", nameValDesc)
      .attr("value", roomDescription)
      .appendTo("#room-form");

    let roomTerms = roomTermsEditor.getData();
    let nameValTerms = $("#room_terms").data("name");
    $("<input />")
      .attr("type", "hidden")
      .attr("name", nameValTerms)
      .attr("value", roomTerms)
      .appendTo("#room-form");
  });
}

/**
 * Inicializar los nuevos checkbox como equipamiento de habitacion, serivicios y obsqueios.
 */
function initializeMoreCheckbox() {
  addCheckbox("#habequip_add", {
    wrapItems: ".prop_list_wrap_habequips",
    wrapText: ".add_habequip_wrap",
    inputText: "#add_habequip_text",
    buttonAdd: "#add_habequip_button",
    buttonCancel: "#cancel_habequip_button",
    classNewItem: "col-md-6 list_item_eqroom",
    attrName: "room_equipament"
  });
  addCheckbox("#serad_add", {
    wrapItems: ".list_items_services_wrap",
    wrapText: ".add_service_wrap",
    inputText: "#add_service_text",
    buttonAdd: "#add_serv_button",
    buttonCancel: "#cancel_serv_button",
    classNewItem: "list_item_services",
    attrName: "room_services"
  });
  addCheckbox("#gift_add", {
    wrapItems: ".prop_list_wrap_gifts",
    wrapText: ".add_gift_wrap",
    inputText: "#add_gift_text",
    buttonAdd: "#add_gift_button",
    buttonCancel: "#cancel_gift_button",
    classNewItem: "list_item_gifts",
    attrName: "room_gifts"
  });
}
