import { paginator } from "../components/paginator";
import { fileringSelects } from "../components/fitering-selects";

const siteURL = process.env.SITE_URL;

if ($("#all_planes").length) {
	initializePaginator();
	initializeSelectFiltering();
}

function initializePaginator() {
	paginator({
		order: [
			"plan_name",
			"city_name",
			"hotel_name",
			"plan_pricing",
			"plan_date",
			"plan_activetext"
		],
		eachTr: function(singleTr) {
			let stringURL = siteURL + "/plan/editar-plan/" + singleTr["plan_id"];
			console.log(singleTr);
			return `<td class="" style='width: 5%;'>
								<a target='_blank' class="view mr-2" title="Editar" href="${stringURL}">
									<i class="fas fa-pencil-alt"></i>
								</a>
							</td>`;
		}
	});
}

function initializeSelectFiltering() {
	fileringSelects("#subastas_filtro", ".select_filtering");
}
