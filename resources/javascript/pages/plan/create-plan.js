import $ from "jquery";
import * as iconpicker from "fontawesome-iconpicker/dist/js/fontawesome-iconpicker";
import * as InlineEditor from "@ckeditor/ckeditor5-build-inline/build/ckeditor";

import { searchCitiesInInputCity } from "../hotel/create-hotel";
import { initHotelsByCityID } from "../hotsale/create-hotsale";
import { datePickerConfig } from "../base";
import { addCheckbox } from "../components/add-checkbox";

var promEditor, descEditor, termsEditor;

var promEl = document.querySelector("#plan_prom"),
	descEl = document.querySelector("#plan_description"),
	termsEl = document.querySelector("#plan_terms");

if ($("#create-plan-form").length) {
	InlineEditor.create(promEl)
		.then(editor => {
			promEditor = editor;
		})
		.catch(error => {
			console.error(error);
		});
	InlineEditor.create(descEl)
		.then(editor => {
			descEditor = editor;
		})
		.catch(error => {
			console.error(error);
		});
	InlineEditor.create(termsEl)
		.then(editor => {
			termsEditor = editor;
		})
		.catch(error => {
			console.error(error);
		});

	initializeCityBehiavor();
	initializeDatePicker();
	initializeFAIcons();
	initializeCustomCheckbox();
	onSubmitForm();
}

function initializeDatePicker() {
	$.datepicker.regional["es"] = datePickerConfig;
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#plan_mindate").datepicker({
		beforeShow: function(input, inst) {
			setTimeout(function() {
				inst.dpDiv.css({
					top: $("#plan_mindate").offset().top + 35,
					left: $("#plan_mindate").offset().left
				});
			}, 0);
		}
	});
	$("#plan_maxdate").datepicker({
		beforeShow: function(input, inst) {
			setTimeout(function() {
				inst.dpDiv.css({
					top: $("#plan_maxdate").offset().top + 35,
					left: $("#plan_maxdate").offset().left
				});
			}, 0);
		}
	});
}

function initializeFAIcons() {
	var changeIcon = (e, selector) => {
		$(selector + "_reflect").html('<i class="' + e + '"></i>');
		$(selector + "_icon").val(e);
	};

	let objIconPicker = {
		placement: "inline",
		templates: {
			search:
				'<input type="search" class="form-control iconpicker-search" placeholder="Buscar..." />'
		}
	};

	$("#iconpicker_s0").iconpicker(objIconPicker);
	$("#iconpicker_s0").on("iconpickerSelected", function(e) {
		changeIcon(e.iconpickerValue, "#iconpicker_s0");
	});

	$("#iconpicker_s1").iconpicker(objIconPicker);
	$("#iconpicker_s1").on("iconpickerSelected", function(e) {
		changeIcon(e.iconpickerValue, "#iconpicker_s1");
	});
	$("#iconpicker_s2").iconpicker(objIconPicker);
	$("#iconpicker_s2").on("iconpickerSelected", function(e) {
		changeIcon(e.iconpickerValue, "#iconpicker_s2");
	});
}

function onSubmitForm() {
	const showError = text => {
		$("#create_plan_danger").addClass("show");
		$("#create_plan_danger").text(text);
		$("body, html").scrollTop($("#create_plan_danger").offset().top - 20);
		return false;
	};

	$("#create-plan-form").submit(function(e) {
		let plan_name = $("#plan_name").val();
		let plan_city_real_val = $("#plan_city_real_val").val();
		let plan_hotel = $("#plan_hotel").val();
		let plan_room = $("#plan_room").val();
		let plan_rooms_quantity = $("#plan_rooms_quantity").val();
		let plan_price = $("#plan_price").val();
		let plan_mindate = $("#plan_mindate").val();
		let plan_maxdate = $("#plan_maxdate").val();

		if (
			plan_name === "" ||
			plan_city_real_val === "" ||
			plan_hotel === "" ||
			plan_hotel === "none" ||
			plan_room === "" ||
			plan_room === "none" ||
			plan_rooms_quantity === "" ||
			plan_rooms_quantity === "none" ||
			plan_price === "" ||
			plan_mindate === "" ||
			plan_maxdate === ""
		) {
			return showError("Hay campos obligatorios sin completar.");
		}

		let prom = promEl.getAttribute("data-name");
		let description = descEl.getAttribute("data-name");
		let terms = termsEl.getAttribute("data-name");

		$("<input />")
			.attr("type", "hidden")
			.attr("name", prom)
			.attr("value", promEditor.getData())
			.appendTo("#create-plan-form");
		$("<input />")
			.attr("type", "hidden")
			.attr("name", description)
			.attr("value", descEditor.getData())
			.appendTo("#create-plan-form");
		$("<input />")
			.attr("type", "hidden")
			.attr("name", terms)
			.attr("value", termsEditor.getData())
			.appendTo("#create-plan-form");
	});
}

/**
 * Inicializar funcionalidad de el campo Ciudad.
 */
function initializeCityBehiavor() {
	searchCitiesInInputCity({
		inputRealVal: "#plan_city_real_val",
		inputSelector: "#plan_city",
		wrapResults: "#plan_cities_results",
		clickOnItem: cityID => {
			initHotelsByCityID(cityID, "#plan_hotel", "#plan_room");
		}
	});
}

function initializeCustomCheckbox() {
	addCheckbox("#service_add", {
		wrapItems: ".prop_list_wrap_services",
		wrapText: ".add_service_wrap",
		inputText: "#add_service_text",
		buttonAdd: "#add_service_button",
		buttonCancel: "#cancel_service_button",
		classNewItem: "col-md-6 list_item_eqroom",
		attrName: "plan_services"
	});
}
