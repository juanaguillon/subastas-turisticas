import { paginator } from "../components/paginator";
import { fileringSelects } from "../components/fitering-selects";

const siteURL = process.env.SITE_URL;

if ($("#ver_subastas").length) {
	initializePaginator();
	initializeSelectFiltering();
}

function initializePaginator() {
	paginator({
		order: [
			"auctionrow_id",
			"city_name",
			"auction_days",
			"auction_maxprice",
			"auction_price",
			"auction_date",
			"auction_statustext"
		],
		eachTr: function(singleTr) {
			let stringURL =
				siteURL + "/oferta/gestionar-oferta/" + singleTr["auctionrow_id"];
			return `<td class="">
								<a class="view mr-2" title="Ver" href="${stringURL}">
									<i class="fa fa-eye"></i>
								</a>
							</td>`;
		}
	});
}

function initializeSelectFiltering() {
	fileringSelects("#subastas_filtro", ".select_filtering");
}
