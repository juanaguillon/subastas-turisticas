<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(array(
  'prefix' => 'user'
), function () {
  // user/register
  Route::post('register', 'UserController@createUser')->middleware("check_rol:noauth");
  // user/login
  Route::post('login', 'UserController@loginUser')->middleware("check_rol:noauth");
  // user/logout
  Route::get('logout', 'UserController@logoutUser');
  // user/configuracion
  Route::get('configuracion', 'UserController@view__editUserConfig')->middleware('check_rol:client');
  Route::post('configuracion', 'UserController@editUserConfig')->middleware('check_rol:client');

  Route::post('crear-suscripcion', 'UserController@createSubscriber')->middleware('check_rol:noauth');
});

/** Rutas de hotel */
Route::group(array(
  'prefix' => 'hotel',
  // 'middleware' => 'auth:admin'
), function () {
  // hotel/get-by-city-id
  Route::get('get-by-city-id/{cityid}', 'HotelController@ajax__getByCityID')->middleware("check_rol:hotel");
  Route::get('configuracion', 'HotelController@view__hotelConfiguration')->middleware("check_rol:hotel");
  Route::post('configuracion', 'HotelController@editHotelConfiguration')->middleware("check_rol:hotel");

  // hotel/create
  Route::post('create', 'HotelController@createHotel')->middleware("check_rol:admin");
  Route::view('create', 'hotel.create-hotel', ["updating" => false])->middleware("check_rol:admin");

  // hotel/ver-hoteles
  Route::get("ver-hoteles", "HotelController@view__showHotels")->middleware("check_rol:admin");
  // hotel/paginador
  Route::get("paginador", "HotelController@ajax__getPaginator")->middleware("check_rol:admin");

  // hotel/editar-hotel/{hotelid}
  Route::get("editar-hotel/{hotelid}", "HotelController@view__editHotel")->middleware(["check_rol:admin", "check_rol:hotel"]);
  Route::post("editar-hotel/{hotelid}", "HotelController@editHotel")->middleware(["check_rol:admin", "check_rol:hotel"]);
});

/** Rutas de Hotsale */
Route::group(array(
  'prefix' => 'hotsale'
), function () {
  // hotsale/create
  Route::view('create', 'hotsale.create-hotsale', ["updating" => false])->middleware("check_rol:admin");
  Route::post('create', 'HotsaleController@createHotsale')->middleware("check_rol:admin");


  Route::get("ver-hotsale/{id}", "HotsaleController@view__showHotsaleByID");

  Route::get("ver-hotsales", "HotsaleController@view__showAllHotsales")->middleware("check_rol:admin");
  Route::get("paginador", "HotsaleController@ajax__getPaginator")->middleware("check_rol:admin");

  Route::get("editar-hotsale/{hotsaleid}", "HotsaleController@view__editHotsale")->middleware("check_rol:admin");
  Route::post("editar-hotsale/{hotsaleid}", "HotsaleController@editHotsale")->middleware("check_rol:admin");
});

/** Rutas de Plan */
Route::group(array(
  'prefix' => 'plan'
), function () {
  // plan/create
  Route::view('create', 'plan.create-plan', ['updating' => false])->middleware("check_rol:admin");
  Route::post('create', 'PlanController@createPlan')->middleware("check_rol:admin");

  Route::get("ver-planes", "PlanController@view__showAllPlans");
  Route::get("paginador", "PlanController@ajax__getPaginator")->middleware("check_rol:admin");

  Route::get("ver-plan/{planid}", "PlanController@view__showPlan");

  Route::get('editar-plan/{planid}', 'PlanController@view__editPlan');
  Route::post('editar-plan/{planid}', 'PlanController@editPlan');
});


/** Rutas de ciudad */
Route::group(array(
  'prefix' => 'city'
), function () {
  // city/get-by-name/{name}
  Route::get("get-with-some-room", "CityController@getAllCitiesWithRooms");
  Route::get('get-by-name/{name}', 'CityController@ajax__getCityByName');
});


/** Rutas de Room (Habitacion) */
Route::group(array(
  'prefix' => 'room'
), function () {
  // room/create
  Route::view('create', 'room.create-room', ['updating' => false])->middleware("check_rol:hotel");
  Route::post('create', 'RoomController@createRoom')->middleware("check_rol:hotel");
  Route::post('editar-habitacion/{roomid}', 'RoomController@updateRoom')->middleware("check_rol:hotel");

  // room/editar-habitacion
  Route::get('editar-habitacion/{roomid}', 'RoomController@view__updateRoom')->middleware("check_rol:hotel");

  // room/get-by-hotel-id
  Route::get('get-by-hotel-id/{hotelID}', 'RoomController@ajax__getRoomsByHotelId');
  // Route::get('get-room-for-auction', 'RoomController@filterRoomsForAuction');

  // room/mis-habitaciones
  Route::get('mis-habitaciones', 'RoomController@view__showMyRooms');
  // room/paginador
  Route::get('paginador', 'RoomController@ajax__getPaginator');
});


Route::group(array(
  'prefix' => 'subasta'
), function () {

  //  subasta/mis-subastas/
  Route::get("mis-subastas", "AuctionController@view__showMyAuctions");
});

Route::group(array(
  "prefix" => "auction"
), function () {
  // auction/create
  Route::post("create", "AuctionController@createAuction");
  Route::get("alter-auction/{auctionid}", "AuctionController@alterAuction");

  // acution/paginador
  Route::get("paginador", "AuctionController@ajax__getPaginator");
});

Route::group(array(
  "prefix" => "oferta"
), function () {

  // oferta/gestionar-oferta/{aurowid}
  Route::get("gestionar-oferta/{aurowid}", "AuctionRowController@manageAuctionRow");
});

Route::group(array(
  'prefix' => 'auctionrow'
), function () {
  // auctionrow/create
  Route::post('create', 'AuctionRowController@createAuctionRow');

  // auctionrow/agregar-fila/
  Route::post('agregar-fila', 'AuctionRowController@agregarAuctionRow');

  // auctionrow/get-by-auction-id/
  Route::get("get-last-by-auction-id/{auctionid}", "AuctionRowController@getLastAuctionRowByAuctionID");
  // auctionrow/get-lastests/
  Route::get("get-lastests/{auctionid}", "AuctionRowController@getAllAuctionsRowByAuctionID");

  // auctionrow/ver-fila-oferta/
  Route::get("ver-fila-oferta/{id}", "AuctionRowController@view__showAuctionRowByID");
});

Route::group(array(
  "prefix" => "reserva",
), function () {
  // reserva/check-reservations
  Route::post("/check-reservation", "ReservationController@redirectToPayU");

  // reserva/mis-reservas
  Route::get("/mis-reservas", "ReservationController@view__reservationsByCurrentUser")->middleware('check_rol:any');
  // reserva/ver-reserva/
  Route::get("/ver-reserva/{id}", "ReservationController@view__showReservationByID");
  // reserva/ver-reserva-pdf/{id}
  Route::get("/ver-reserva-pdf/{id}.pdf", "ReservationController@showReservationInPDF");

  // reserva/paginador
  Route::get("paginador", "ReservationController@ajax__getPaginator");
});

/** Rutas de Generales */
Route::get('/', 'SiteController@indexView');
Route::view('/como-funciona', 'site.como-funciona');
Route::get("/hotsales", "SiteController@view__todosHotsales");
